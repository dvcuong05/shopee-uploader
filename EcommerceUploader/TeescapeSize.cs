﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class TeescapeSize : UserControl
    {
        private string label = "";
        private string value = "";
        private bool isDemo = false;
        private bool isChecked = false;
        private string parentId = "";
        private bool isInitial = true;
        private string parentLable = "";
        private string[] defaultValues = new string[]{"0","1","2","3","4","5","6","7","8"}; 
        
        public delegate void IsCheckedHandler(Object sender, string parentId, string label, string value, Boolean isChecked, string parentLabel, Boolean isDemo);
        public event IsCheckedHandler Checked;

        public TeescapeSize(string parentLable, string parentId, string label, string value, Nullable<bool> isChecked, Nullable<bool> isDemo)
        {
            this.parentId = parentId;
            this.label = label;
            this.value = value;
            this.isDemo = isDemo != null ? isDemo.Value : false;
            this.parentLable = parentLable;
            if (Array.IndexOf(this.defaultValues, this.value) > -1)
            {
                this.isChecked = true;
            }
            if (isChecked != null)
            {
                this.isChecked = isChecked.Value;
                this.isInitial = isChecked.Value;
            }
            else
            {
                this.isInitial = false;
            }

            InitializeComponent();
        }

        private void TeescapeSize_Load(object sender, EventArgs e)
        {
            this.sizeLabel.Text = this.label;
            this.check.Checked = this.isChecked;
            if (!this.isDemo)
            {
                this.sizeLabel.BackColor = Color.Transparent;
            }
            else
            {
                this.sizeLabel.BackColor = Color.Red;
            }
        }

        private void check_CheckedChanged(object sender, EventArgs e)
        {
            if (this.isInitial)
            {
                this.isInitial = false;
                return;
            }
            this.Checked(sender, this.parentId, this.label, this.value, this.check.Checked, this.parentLable, this.isDemo);
        }

        private void sizeLabel_Click(object sender, EventArgs e)
        {
            if(this.sizeLabel.BackColor == Color.Red)
            {
                this.sizeLabel.BackColor = Color.Transparent;
                this.isDemo = false;
            }
            else
            {
                this.sizeLabel.BackColor = Color.Red;
                this.isDemo = true;
            }
            this.Checked(sender, this.parentId, this.label, this.value, this.check.Checked, this.parentLable, this.isDemo);
        }
    }
}
