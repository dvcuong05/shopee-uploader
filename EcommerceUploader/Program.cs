﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Linq;
using System.Net;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Main.Controls;

namespace Main
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                

                if (checkUpdate())
                {
                    try
                    {
                        var updaterModulePath = Path.Combine(Application.StartupPath, "updater.exe");
                        Process process = Process.Start(updaterModulePath, "/checknow");
                        process.Close();
                    }
                    catch (Exception e)
                    {
                        Application.Run(new Main());
                        Application.Exit();
                    }
                }
                else
                {
                    Application.Run(new Main());
                    Application.Exit();
                }
                CefSharp.Cef.Shutdown();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Program.cs UI Exception");
                Application.Exit();
            }

        }

        private static Boolean checkUpdate()
        {
            try
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead("http://nccsolution.com/toolversion/shopee/version.txt");
                StreamReader reader = new StreamReader(stream);
                String lastVersion = reader.ReadToEnd();

                Version currentVersion = Assembly.GetExecutingAssembly().GetName().Version;
                var lastVer = new Version(lastVersion);

                var result = currentVersion.CompareTo(lastVer);
                if (result > 0)
                    return false;
                else if (result < 0)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not check new version because exception:" + e.Message);
                return false;
            }

        }

        
    }
}
