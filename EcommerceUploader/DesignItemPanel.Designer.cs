﻿namespace Main
{
    partial class DesignItemPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignItemPanel));
            this.teeLink = new System.Windows.Forms.Label();
            this.keyword3 = new System.Windows.Forms.TextBox();
            this.keyword2 = new System.Windows.Forms.TextBox();
            this.teeName = new System.Windows.Forms.Label();
            this.keyword1 = new System.Windows.Forms.TextBox();
            this.urlOutput = new System.Windows.Forms.Label();
            this.keyword = new System.Windows.Forms.Label();
            this.keyword6 = new System.Windows.Forms.TextBox();
            this.keyword7 = new System.Windows.Forms.TextBox();
            this.keyword8 = new System.Windows.Forms.TextBox();
            this.keyword9 = new System.Windows.Forms.TextBox();
            this.keyword4 = new System.Windows.Forms.TextBox();
            this.keyword10 = new System.Windows.Forms.TextBox();
            this.keyword5 = new System.Windows.Forms.TextBox();
            this.uploadItemBtn = new System.Windows.Forms.Button();
            this.editItemNameBtn = new System.Windows.Forms.Button();
            this.editItemUrlBtn = new System.Windows.Forms.Button();
            this.copyBtn = new System.Windows.Forms.Button();
            this.pasteBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.teeLinkTxt = new System.Windows.Forms.TextBox();
            this.teeNameTxt = new System.Windows.Forms.TextBox();
            this.outputTxt = new System.Windows.Forms.TextBox();
            this.clearBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // teeLink
            // 
            this.teeLink.AutoSize = true;
            this.teeLink.Location = new System.Drawing.Point(5, 14);
            this.teeLink.Name = "teeLink";
            this.teeLink.Size = new System.Drawing.Size(80, 13);
            this.teeLink.TabIndex = 0;
            this.teeLink.Text = "Link Teespring ";
            this.teeLink.Click += new System.EventHandler(this.label1_Click);
            // 
            // keyword3
            // 
            this.keyword3.Location = new System.Drawing.Point(259, 89);
            this.keyword3.Name = "keyword3";
            this.keyword3.Size = new System.Drawing.Size(83, 20);
            this.keyword3.TabIndex = 20;
            // 
            // keyword2
            // 
            this.keyword2.Location = new System.Drawing.Point(176, 89);
            this.keyword2.Name = "keyword2";
            this.keyword2.Size = new System.Drawing.Size(82, 20);
            this.keyword2.TabIndex = 19;
            // 
            // teeName
            // 
            this.teeName.AutoSize = true;
            this.teeName.Location = new System.Drawing.Point(5, 38);
            this.teeName.Name = "teeName";
            this.teeName.Size = new System.Drawing.Size(88, 13);
            this.teeName.TabIndex = 2;
            this.teeName.Text = "Name Teespring ";
            this.teeName.Click += new System.EventHandler(this.label2_Click);
            // 
            // keyword1
            // 
            this.keyword1.Location = new System.Drawing.Point(92, 89);
            this.keyword1.Name = "keyword1";
            this.keyword1.Size = new System.Drawing.Size(82, 20);
            this.keyword1.TabIndex = 18;
            // 
            // urlOutput
            // 
            this.urlOutput.AutoSize = true;
            this.urlOutput.Location = new System.Drawing.Point(5, 62);
            this.urlOutput.Name = "urlOutput";
            this.urlOutput.Size = new System.Drawing.Size(65, 13);
            this.urlOutput.TabIndex = 4;
            this.urlOutput.Text = "URL output:";
            this.urlOutput.Click += new System.EventHandler(this.label3_Click);
            // 
            // keyword
            // 
            this.keyword.AutoSize = true;
            this.keyword.Location = new System.Drawing.Point(5, 89);
            this.keyword.Name = "keyword";
            this.keyword.Size = new System.Drawing.Size(51, 13);
            this.keyword.TabIndex = 6;
            this.keyword.Text = "Keyword:";
            // 
            // keyword6
            // 
            this.keyword6.Location = new System.Drawing.Point(92, 115);
            this.keyword6.Name = "keyword6";
            this.keyword6.Size = new System.Drawing.Size(82, 20);
            this.keyword6.TabIndex = 23;
            // 
            // keyword7
            // 
            this.keyword7.Location = new System.Drawing.Point(176, 115);
            this.keyword7.Name = "keyword7";
            this.keyword7.Size = new System.Drawing.Size(82, 20);
            this.keyword7.TabIndex = 24;
            // 
            // keyword8
            // 
            this.keyword8.Location = new System.Drawing.Point(260, 115);
            this.keyword8.Name = "keyword8";
            this.keyword8.Size = new System.Drawing.Size(82, 20);
            this.keyword8.TabIndex = 25;
            // 
            // keyword9
            // 
            this.keyword9.Location = new System.Drawing.Point(343, 115);
            this.keyword9.Name = "keyword9";
            this.keyword9.Size = new System.Drawing.Size(82, 20);
            this.keyword9.TabIndex = 26;
            // 
            // keyword4
            // 
            this.keyword4.Location = new System.Drawing.Point(343, 89);
            this.keyword4.Name = "keyword4";
            this.keyword4.Size = new System.Drawing.Size(82, 20);
            this.keyword4.TabIndex = 21;
            // 
            // keyword10
            // 
            this.keyword10.Location = new System.Drawing.Point(426, 115);
            this.keyword10.Name = "keyword10";
            this.keyword10.Size = new System.Drawing.Size(82, 20);
            this.keyword10.TabIndex = 27;
            // 
            // keyword5
            // 
            this.keyword5.Location = new System.Drawing.Point(426, 89);
            this.keyword5.Name = "keyword5";
            this.keyword5.Size = new System.Drawing.Size(82, 20);
            this.keyword5.TabIndex = 22;
            // 
            // uploadItemBtn
            // 
            this.uploadItemBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.uploadItemBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uploadItemBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uploadItemBtn.ForeColor = System.Drawing.SystemColors.Control;
            this.uploadItemBtn.Location = new System.Drawing.Point(344, 9);
            this.uploadItemBtn.Name = "uploadItemBtn";
            this.uploadItemBtn.Size = new System.Drawing.Size(57, 23);
            this.uploadItemBtn.TabIndex = 14;
            this.uploadItemBtn.Text = "Upload";
            this.uploadItemBtn.UseVisualStyleBackColor = false;
            this.uploadItemBtn.Click += new System.EventHandler(this.uploadItemBtn_Click);
            // 
            // editItemNameBtn
            // 
            this.editItemNameBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.editItemNameBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editItemNameBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editItemNameBtn.ForeColor = System.Drawing.SystemColors.Control;
            this.editItemNameBtn.Location = new System.Drawing.Point(344, 33);
            this.editItemNameBtn.Name = "editItemNameBtn";
            this.editItemNameBtn.Size = new System.Drawing.Size(57, 23);
            this.editItemNameBtn.TabIndex = 15;
            this.editItemNameBtn.Text = "Edit";
            this.editItemNameBtn.UseVisualStyleBackColor = false;
            this.editItemNameBtn.Click += new System.EventHandler(this.editItemNameBtn_Click);
            // 
            // editItemUrlBtn
            // 
            this.editItemUrlBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.editItemUrlBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editItemUrlBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editItemUrlBtn.ForeColor = System.Drawing.SystemColors.Control;
            this.editItemUrlBtn.Location = new System.Drawing.Point(344, 57);
            this.editItemUrlBtn.Name = "editItemUrlBtn";
            this.editItemUrlBtn.Size = new System.Drawing.Size(57, 23);
            this.editItemUrlBtn.TabIndex = 16;
            this.editItemUrlBtn.Text = "Edit";
            this.editItemUrlBtn.UseVisualStyleBackColor = false;
            this.editItemUrlBtn.Click += new System.EventHandler(this.editItemUrlBtn_Click);
            // 
            // copyBtn
            // 
            this.copyBtn.BackColor = System.Drawing.Color.LavenderBlush;
            this.copyBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("copyBtn.BackgroundImage")));
            this.copyBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.copyBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.copyBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyBtn.ForeColor = System.Drawing.SystemColors.Control;
            this.copyBtn.Location = new System.Drawing.Point(54, 87);
            this.copyBtn.Name = "copyBtn";
            this.copyBtn.Size = new System.Drawing.Size(33, 23);
            this.copyBtn.TabIndex = 17;
            this.copyBtn.UseVisualStyleBackColor = false;
            this.copyBtn.Click += new System.EventHandler(this.copyBtn_Click);
            // 
            // pasteBtn
            // 
            this.pasteBtn.BackColor = System.Drawing.Color.LavenderBlush;
            this.pasteBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pasteBtn.BackgroundImage")));
            this.pasteBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pasteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.pasteBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pasteBtn.ForeColor = System.Drawing.SystemColors.Control;
            this.pasteBtn.Location = new System.Drawing.Point(54, 113);
            this.pasteBtn.Name = "pasteBtn";
            this.pasteBtn.Size = new System.Drawing.Size(33, 23);
            this.pasteBtn.TabIndex = 18;
            this.pasteBtn.UseVisualStyleBackColor = false;
            this.pasteBtn.Click += new System.EventHandler(this.pasteBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DimGray;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(405, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 70);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // teeLinkTxt
            // 
            this.teeLinkTxt.Location = new System.Drawing.Point(95, 10);
            this.teeLinkTxt.Name = "teeLinkTxt";
            this.teeLinkTxt.Size = new System.Drawing.Size(247, 20);
            this.teeLinkTxt.TabIndex = 15;
            this.teeLinkTxt.TextChanged += new System.EventHandler(this.teeLinkTxt_TextChanged);
            // 
            // teeNameTxt
            // 
            this.teeNameTxt.Location = new System.Drawing.Point(95, 34);
            this.teeNameTxt.Name = "teeNameTxt";
            this.teeNameTxt.Size = new System.Drawing.Size(247, 20);
            this.teeNameTxt.TabIndex = 16;
            // 
            // outputTxt
            // 
            this.outputTxt.Location = new System.Drawing.Point(95, 58);
            this.outputTxt.Name = "outputTxt";
            this.outputTxt.Size = new System.Drawing.Size(247, 20);
            this.outputTxt.TabIndex = 17;
            // 
            // clearBtn
            // 
            this.clearBtn.BackColor = System.Drawing.Color.LavenderBlush;
            this.clearBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clearBtn.BackgroundImage")));
            this.clearBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.clearBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.clearBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearBtn.ForeColor = System.Drawing.SystemColors.Control;
            this.clearBtn.Location = new System.Drawing.Point(13, 113);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(33, 23);
            this.clearBtn.TabIndex = 28;
            this.clearBtn.UseVisualStyleBackColor = false;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // DesignItemPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.outputTxt);
            this.Controls.Add(this.teeLinkTxt);
            this.Controls.Add(this.teeNameTxt);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pasteBtn);
            this.Controls.Add(this.copyBtn);
            this.Controls.Add(this.editItemUrlBtn);
            this.Controls.Add(this.editItemNameBtn);
            this.Controls.Add(this.uploadItemBtn);
            this.Controls.Add(this.keyword10);
            this.Controls.Add(this.keyword5);
            this.Controls.Add(this.keyword9);
            this.Controls.Add(this.keyword4);
            this.Controls.Add(this.keyword6);
            this.Controls.Add(this.keyword7);
            this.Controls.Add(this.keyword8);
            this.Controls.Add(this.keyword);
            this.Controls.Add(this.keyword1);
            this.Controls.Add(this.urlOutput);
            this.Controls.Add(this.keyword2);
            this.Controls.Add(this.teeName);
            this.Controls.Add(this.keyword3);
            this.Controls.Add(this.teeLink);
            this.Enabled = false;
            this.Margin = new System.Windows.Forms.Padding(8, 5, 5, 5);
            this.Name = "DesignItemPanel";
            this.Size = new System.Drawing.Size(511, 140);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label teeLink;
        private System.Windows.Forms.TextBox keyword3;
        private System.Windows.Forms.TextBox keyword2;
        private System.Windows.Forms.Label teeName;
        private System.Windows.Forms.TextBox keyword1;
        private System.Windows.Forms.Label urlOutput;
        private System.Windows.Forms.Label keyword;
        private System.Windows.Forms.TextBox keyword6;
        private System.Windows.Forms.TextBox keyword7;
        private System.Windows.Forms.TextBox keyword8;
        private System.Windows.Forms.TextBox keyword9;
        private System.Windows.Forms.TextBox keyword4;
        private System.Windows.Forms.TextBox keyword10;
        private System.Windows.Forms.TextBox keyword5;
        private System.Windows.Forms.Button uploadItemBtn;
        private System.Windows.Forms.Button editItemNameBtn;
        private System.Windows.Forms.Button editItemUrlBtn;
        private System.Windows.Forms.Button copyBtn;
        private System.Windows.Forms.Button pasteBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox teeLinkTxt;
        private System.Windows.Forms.TextBox teeNameTxt;
        private System.Windows.Forms.TextBox outputTxt;
        private System.Windows.Forms.Button clearBtn;
    }
}
