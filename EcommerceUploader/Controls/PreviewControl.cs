﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Drawing.Imaging;
using NCCLibs;
using Main.classes;

namespace Main.Controls
{
    public partial class PreviewControl : UserControl
    {
        private int startX, startY;
        private bool isDragging;

        private Color penColor = Color.Red;
        private Rectangle designRect = new Rectangle();
        private Rectangle tagRect = new Rectangle();
        private Image designImage;
        private Image bgImage;
        private Image bgImageResized;
        private Image effectImage;

        private Image tagImage;
        private Image OriginTagImage;
        private Bitmap previewImage;
        //public delegate void UpdateHandler(string detail);
        //public event UpdateHandler UpdateProccess;

        public delegate void UpdateStyle(ImageItem bg, string colorName);
        public event UpdateStyle UpdateData;

        public delegate void OpacityChangeHandler(string parentId, classes.StyleColor styleColor);
        public event OpacityChangeHandler OpacityChanged;
        private enum PosSizableRect
        {
            UpMiddle,
            LeftMiddle,
            LeftBottom,
            LeftUp,
            RightUp,
            RightMiddle,
            RightBottom,
            BottomMiddle,
            None

        };
        private int sizeNodeRect = 5;
        private PosSizableRect nodeSelected = PosSizableRect.None;
        private bool isMoving = false;
        private float StartAngle;
        private float CurrentAngle;
        private float TotalAngle = 0;
        private ImageItem background;
        private ImageItem design;
        private ImageItem tag;
        private StyleColor selectedColor;
        private TeescapeColor selectedTeescapeColor;
        private string styleId;
        List<StyleColor> styleColors;
        public PreviewControl(string styleId, List<StyleColor> styleColors, ImageItem mockupItem, ImageItem design, ImageItem tag)
        {
            this.styleColors = (mockupItem != null && mockupItem.Colors != null && mockupItem.Colors.Count > 0) ? ToolUtils.convertStyleColorToStyleColor(mockupItem.Colors) : styleColors;
            this.styleId = styleId;
            if (this.styleColors != null && this.styleColors.Count > 0)
            {
                selectedColor = this.styleColors[0];
            }
            this.background = mockupItem;
            this.design = design;
            this.tag = tag;
            InitializeComponent();
        }

        public void releaseResouce()
        {
            designImage.Dispose();
            bgImage.Dispose();
            bgImageResized.Dispose();
            effectImage.Dispose();
            tagImage.Dispose();
            OriginTagImage.Dispose();
            previewImage.Dispose();
        }

        public void hideRect()
        {
            penColor = Color.Transparent;
            pictureBoxPreview.Refresh();
        }

        public void showRect()
        {
            penColor = Color.Red;
            pictureBoxPreview.Refresh();
        }

        private void PreviewControl_Load(object sender, EventArgs e)
        {
            previewBox.Height = 500;
            previewBox.Width = 500;
            previewBox.Left = 0;
            previewBox.Top = 0;
            //drawing
            initPreview();
            DrawColors();
        }

        private async void DrawColors()
        {
            await Task.Run(() =>
            {
                Invoke(new Action(() =>
                {
                    foreach (StyleColor c in this.styleColors)
                    {
                        addColorRadio(styleId, "", c);
                    }
                }));
            });
        }

        private async void initPreview()
        {
            if (background != null)
            {
                // background hold opacity of effect image 
                // and tag angle
                // and design rect
                opacityBar.Value = selectedColor.Opacity;
                TotalAngle = background.Angle;
                tagRect = background.AdditionalRect;
                designRect = background.Rect;
            }

            await Task.Run(() =>
            {
                if (!string.IsNullOrEmpty(design.FullPath))
                {
                    designImage = loadImage(@design.FullPath, true, 300);
                }

                //load tag
                // tag list from branch
                if (tag == null)
                {
                    if (!string.IsNullOrEmpty(ToolUtils.appConfig.TagPath))
                    {
                        List<ImageItem> tags = DirectoryUtil.GetImages(ToolUtils.appConfig.TagPath, ToolUtils.ACCEPT_IMAGE_TYPE, false);
                        if (tags != null && tags.Count > 0)
                        {
                            tag = tags[0];
                        }
                    }
                }
                if (tag != null)
                {
                    OriginTagImage = loadImage(tag.FullPath, true, 150);
                    if (TotalAngle != 0)
                    {
                        tagImage = ImageUtil.RotateBitmap((Bitmap)OriginTagImage, TotalAngle);
                    }
                    else
                    {
                        tagImage = (Image)OriginTagImage.Clone();
                    }
                    if (tagRect == null || (tagRect.X == 0 && tagRect.Y == 0))
                    {
                        //tagRect = new Rectangle(10, 10, tagImage.Width, tagImage.Height);
                        tagRect = new Rectangle(10, 10, tagImage.Width, tagImage.Width);
                    }
                }
                if (designRect == null)
                {
                    designRect = new Rectangle(131, 126, 200, 219);
                }

                CreatePreviewImage();

            });

        }

        public void saveData()
        {
            // save possiton
            opacityBar.Value = selectedColor.Opacity;
            background.Rect = designRect;
            background.AdditionalRect = tagRect;
            background.Angle = TotalAngle;
            background.Opacity = opacityBar.Value;
            background.Colors = ToolUtils.convertStyleColorToColorOpacity(this.styleColors);
            UpdateData(background, selectedColor.Name);
        }

        private Bitmap createPreviewImage(Image baseImg)
        {
            int[] s = getPreviewSize(baseImg, 500);
            return new Bitmap(s[0], s[1], PixelFormat.Format32bppArgb);
        }

        private int[] getPreviewSize(Image baseImg, int size)
        {
            int containerW, containerH;
            if (baseImg.Width > baseImg.Height)
            {
                containerW = size;
                containerH = Convert.ToInt32(baseImg.Height * (containerW / (float)baseImg.Width));
            }
            else
            {
                containerH = size;
                containerW = Convert.ToInt32(baseImg.Width * (containerH / (float)baseImg.Height));
            }
            return new int[] { containerW, containerH };
        }

        private void previewBox_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(penColor, 2))
            {
                e.Graphics.DrawRectangle(pen, designRect);

                e.Graphics.DrawRectangle(pen, tagRect);
                foreach (PosSizableRect pos in Enum.GetValues(typeof(PosSizableRect)))
                {
                    e.Graphics.DrawRectangle(new Pen(Color.Red), GetRect(pos));
                }

            }
        }

        private void previewBox_MouseDown(object sender, MouseEventArgs e)
        {
            startX = e.X;
            startY = e.Y;
            isDragging = true;

            TagBox_MouseDown(e);
            TagBoxRotate_MouseDown(e);
        }

        private void previewBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isDragging)
            {
                return;
            }

            //The x-value of our rectangle should be the minimum between the start x-value and the current x-position
            int x = Math.Min(startX, e.X);
            //The y-value of our rectangle should also be the minimum between the start y-value and current y-value
            int y = Math.Min(startY, e.Y);

            //The width of our rectangle should be the maximum between the start x-position and current x-position minus
            //the minimum of start x-position and current x-position
            int width = Math.Max(startX, e.X) - Math.Min(startX, e.X);

            //For the hight value, it's basically the same thing as above, but now with the y-values:
            int height = Math.Max(startY, e.Y) - Math.Min(startY, e.Y);
            if (designRadio.Checked)
            {
                designRect = new Rectangle(x, y, width, height);
            }
            if (tagRadio.Checked)
            {
                TagBox_MouseMove(e);
            }
            if (RotateRadio.Checked)
            {
                TagBoxRotate_MouseMove(e);
            }
            // Refresh the form and draw the rectangle
            previewBox.Invalidate();
        }

        private void previewBox_MouseUp(object sender, MouseEventArgs e)
        {
            isDragging = false;
            isMoving = false;
            TotalAngle = CurrentAngle;
            background.Angle = TotalAngle;
            drawToPreview();
            ChangeCursor(new Point(0, 0));
        }

        private void opacity_MouseUp(object sender, MouseEventArgs e)
        {
            if(selectedTeescapeColor != null)
            {
                selectedTeescapeColor.setOpacity(opacityBar.Value);
            }
            CreatePreviewImage();
        }

        private async void CreatePreviewImage()
        {
            await Task.Run(() =>
            {
                Invoke(new Action(() =>
                {

                    if (previewBox.Image != null)
                    {
                        previewBox.Image.Dispose();
                    }

                    if (previewImage != null)
                    {
                        previewImage.Dispose();
                    }

                    if (selectedColor == null)
                    {
                        MessageBox.Show("There has no color setted yet!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    // load layer 1
                    if (bgImage == null)
                    {
                        string p1 = background.AdditionalImageFullPath;
                        FileStream fs1 = new FileStream(p1, FileMode.Open, FileAccess.Read);
                        bgImage = Image.FromStream(fs1);
                        fs1.Close();
                        fs1 = null;
                    }
                    // get privew size base on layer 1;
                    previewImage = createPreviewImage(bgImage);
                    // draw new image
                    previewImage.SetResolution(bgImage.HorizontalResolution, bgImage.VerticalResolution);
                    var graphics = Graphics.FromImage(previewImage);

                    graphics.CompositingMode = CompositingMode.SourceOver;
                    // fill solid color:
                    string colorCode = selectedColor.ColorCode;
                    System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml(colorCode);
                    graphics.Clear(col);
                    // draw layer 1: mockup 1;
                    if (bgImageResized == null)
                    {
                        bgImageResized = ImageUtil.Resize(bgImage, previewImage.Width, previewImage.Height);
                    }
                    graphics.DrawImage(bgImageResized, 0, 0);
                    // layer1Resized.Dispose();
                    // layer1Resized = null;
                    if (effectImage == null)
                    {
                        string p2 = background.FullPath;
                        FileStream fs2 = new FileStream(p2, FileMode.Open, FileAccess.Read);
                        Image layer2 = Image.FromStream(fs2);
                        effectImage = ImageUtil.Resize(layer2, previewImage.Width, previewImage.Height);
                        layer2.Dispose();
                        layer2 = null;
                        fs2.Dispose();
                        fs2 = null;
                    }
                    float opacityvalue = (float)opacityBar.Value / 100;
                    Image opacityLayer2 = ImageUtil.ChangeOpacity(effectImage, opacityvalue);
                    graphics.DrawImage(opacityLayer2, 0, 0);
                    opacityLayer2.Dispose();
                    opacityLayer2 = null;
                    previewBox.Image = previewImage;
                    previewBox.Invalidate();
                    drawToPreview();

                }));
            });
        }

        private void drawToPreview()
        {
            try
            {

                var target = new Bitmap(previewImage.Width, previewImage.Height, PixelFormat.Format32bppArgb);
                target.SetResolution(previewImage.HorizontalResolution, previewImage.VerticalResolution);
                var graphics = Graphics.FromImage(target);
                graphics.CompositingMode = CompositingMode.SourceOver;

                graphics.DrawImage(previewImage, 0, 0, previewImage.Width, previewImage.Height);
                if (designImage != null)
                {
                    ImageUtil.AddSticker(graphics, new ImageItem()
                    {
                        Rect = designRect
                    }, designImage, target, 500, false);
                }
                if (tagImage != null)
                {
                    ImageUtil.AddSticker(graphics, new ImageItem()
                    {
                        Rect = tagRect
                    }, tagImage, target, 500, false);
                }
                graphics.Dispose();
                previewBox.Image = target;
                previewBox.Invalidate();
            }
            catch (Exception ee)
            {
                MessageBox.Show("Cannot merge images. Please check your images input again?.Eg: brand tag, mockup images,..", "Teescape Uploader", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private Image loadImage(string path, bool isResize, int size)
        {
            FileStream fs = new FileStream(@path, FileMode.Open, FileAccess.Read);
            Image img = Image.FromStream(fs);
            if (isResize)
            {
                int[] s = getPreviewSize(img, size);
                Image resizedImg = ImageUtil.Resize(img, s[0], s[1]);
                img.Dispose();
                img = null;
                fs.Dispose();
                fs = null;
                return resizedImg;
            }
            fs.Dispose();
            fs = null;
            return img;
        }


        private Rectangle GetRect(PosSizableRect p)
        {
            switch (p)
            {
                case PosSizableRect.LeftUp:
                    return CreateRectSizableNode(tagRect.X, tagRect.Y);

                case PosSizableRect.LeftMiddle:
                    return CreateRectSizableNode(tagRect.X, tagRect.Y + +tagRect.Height / 2);

                case PosSizableRect.LeftBottom:
                    return CreateRectSizableNode(tagRect.X, tagRect.Y + tagRect.Height);

                case PosSizableRect.BottomMiddle:
                    return CreateRectSizableNode(tagRect.X + tagRect.Width / 2, tagRect.Y + tagRect.Height);

                case PosSizableRect.RightUp:
                    return CreateRectSizableNode(tagRect.X + tagRect.Width, tagRect.Y);

                case PosSizableRect.RightBottom:
                    return CreateRectSizableNode(tagRect.X + tagRect.Width, tagRect.Y + tagRect.Height);

                case PosSizableRect.RightMiddle:
                    return CreateRectSizableNode(tagRect.X + tagRect.Width, tagRect.Y + tagRect.Height / 2);

                case PosSizableRect.UpMiddle:
                    return CreateRectSizableNode(tagRect.X + tagRect.Width / 2, tagRect.Y);
                default:
                    return new Rectangle();
            }
        }

        private Rectangle CreateRectSizableNode(int x, int y)
        {
            return new Rectangle(x - sizeNodeRect / 2, y - sizeNodeRect / 2, sizeNodeRect, sizeNodeRect);
        }

        private void TagBox_MouseDown(MouseEventArgs e)
        {
            if (!tagRadio.Checked)
            {
                return;
            }
            isDragging = true;
            nodeSelected = PosSizableRect.None;
            nodeSelected = GetNodeSelectable(e.Location);
            if (tagRect.Contains(new Point(e.X, e.Y)))
            {
                isMoving = true;
            }
        }

        private void TagBoxRotate_MouseDown(MouseEventArgs e)
        {
            if (!tagRadio.Checked)
            {
                return;
            }
            if (tagRect != null)
            {
                float dx = e.X - tagRect.X;
                float dy = e.Y - tagRect.Y;
                StartAngle = (float)Math.Atan2(dy, dx);
            }
        }

        private void TagBox_MouseMove(MouseEventArgs e)
        {
            ChangeCursor(e.Location);

            Rectangle backupRect = tagRect;

            switch (nodeSelected)
            {
                case PosSizableRect.LeftUp:
                    tagRect.X += e.X - startX;
                    tagRect.Width -= e.X - startX;
                    tagRect.Y += e.Y - startY;
                    //tagRect.Height -= e.Y - startY;
                    tagRect.Height = tagRect.Width;
                    break;
                case PosSizableRect.LeftMiddle:
                    tagRect.X += e.X - startX;
                    tagRect.Width -= e.X - startX;
                    //cuong add
                    tagRect.Height = tagRect.Width;
                    break;
                case PosSizableRect.LeftBottom:
                    tagRect.Width -= e.X - startX;
                    tagRect.X += e.X - startX;
                    //tagRect.Height += e.Y - startY;
                    tagRect.Height = tagRect.Width;
                    break;
                case PosSizableRect.BottomMiddle:
                    tagRect.Height += e.Y - startY;
                    //cuong add
                    tagRect.Width = tagRect.Height;
                    break;
                case PosSizableRect.RightUp:
                    tagRect.Width += e.X - startX;
                    tagRect.Y += e.Y - startY;
                    //tagRect.Height -= e.Y - startY;
                    tagRect.Height = tagRect.Width;
                    break;
                case PosSizableRect.RightBottom:
                    tagRect.Width += e.X - startX;
                    //tagRect.Height += e.Y - startY;
                    tagRect.Height = tagRect.Width;
                    break;
                case PosSizableRect.RightMiddle:
                    tagRect.Width += e.X - startX;
                    // cuong add
                    tagRect.Height = tagRect.Width;
                    break;

                case PosSizableRect.UpMiddle:
                    tagRect.Y += e.Y - startY;
                    tagRect.Height -= e.Y - startY;
                    //cuogn add
                    tagRect.Width = tagRect.Height;
                    break;

                default:
                    if (isMoving)
                    {
                        tagRect.X = tagRect.X + (e.X - startX);
                        tagRect.Y = tagRect.Y + (e.Y - startY);
                    }
                    break;
            }

            startX = e.X;
            startY = e.Y;

            if (tagRect.Width < 5 || tagRect.Height < 5)
            {
                tagRect = backupRect;
            }

            TestIfRectInsideArea();

            previewBox.Invalidate();
            drawToPreview();
        }

        private void TagBoxRotate_MouseMove(MouseEventArgs e)
        {
            if (OriginTagImage == null)
            {
                return;
            }
            // Get the angle from horizontal to the
            // vector between the center and the current point.
            float dx = e.X - tagRect.X;
            float dy = e.Y - tagRect.Y;
            float new_angle = (float)Math.Atan2(dy, dx);

            // Calculate the change in angle.
            CurrentAngle = new_angle - StartAngle;

            // Convert to degrees.
            CurrentAngle *= 180 / (float)Math.PI;

            CurrentAngle += TotalAngle;
            // Rotate the original image to make the result bitmap.

            tagImage.Dispose();
            // Display the result.
            tagImage = ImageUtil.RotateBitmap((Bitmap)OriginTagImage, CurrentAngle);
            drawToPreview();
        }

        private void ChangeCursor(Point p)
        {
            previewBox.Cursor = GetCursor(GetNodeSelectable(p));
        }

        private Cursor GetCursor(PosSizableRect p)
        {
            switch (p)
            {
                case PosSizableRect.LeftUp:
                    return Cursors.SizeNWSE;

                case PosSizableRect.LeftMiddle:
                    return Cursors.SizeWE;

                case PosSizableRect.LeftBottom:
                    return Cursors.SizeNESW;

                case PosSizableRect.BottomMiddle:
                    return Cursors.SizeNS;

                case PosSizableRect.RightUp:
                    return Cursors.SizeNESW;

                case PosSizableRect.RightBottom:
                    return Cursors.SizeNWSE;

                case PosSizableRect.RightMiddle:
                    return Cursors.SizeWE;

                case PosSizableRect.UpMiddle:
                    return Cursors.SizeNS;
                default:
                    return Cursors.Default;
            }
        }

        private PosSizableRect GetNodeSelectable(Point p)
        {
            foreach (PosSizableRect r in Enum.GetValues(typeof(PosSizableRect)))
            {
                if (GetRect(r).Contains(p))
                {
                    return r;
                }
            }
            return PosSizableRect.None;
        }

        private async void ExportButton_Click(object sender, EventArgs e)
        {
            if (tag != null)
            {
                tag.Rect = background.AdditionalRect;
                tag.Angle = background.Angle;
            }
            ImageItem effect = new ImageItem()
            {
                FullPath = background.FullPath,
                Opacity = background.Opacity
            };
            ImageItem bg = new ImageItem()
            {
                FullPath = background.AdditionalImageFullPath
            };
            if (design != null)
            {
                design.Rect = background.Rect;
            }
            string userRoot = System.Environment.GetEnvironmentVariable("USERPROFILE");
            string downloadFolder = Path.Combine(userRoot, "Downloads\\");
            string path = downloadFolder + @"teescape_example_mockup." + ImageFormat.Jpeg.ToString();
            await Task.Run(() =>
            {
                Bitmap exported = ImageUtil.merged(2000, 0, 500, selectedColor.ColorCode, bg,
                    effect, design, tag);
                exported.Save(path, ImageFormat.Jpeg);
                exported.Dispose();
            });
            MessageBox.Show("Export as " + path);

        }

        private void TestIfRectInsideArea()
        {
            // Test if rectangle still inside the area.
            if (tagRect.X < 0) tagRect.X = 0;
            if (tagRect.Y < 0) tagRect.Y = 0;
            if (tagRect.Width <= 0) tagRect.Width = 1;
            if (tagRect.Height <= 0) tagRect.Height = 1;

            if (tagRect.X + tagRect.Width > previewBox.Width)
            {
                tagRect.Width = previewBox.Width - tagRect.X - 1; // -1 to be still show 
                isMoving = false;
            }
            if (tagRect.Y + tagRect.Height > previewBox.Height)
            {
                tagRect.Height = previewBox.Height - tagRect.Y - 1;// -1 to be still show 
                isMoving = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // update save
            saveData();
        }

        private void addColorRadio(string styleId, string styleName, StyleColor sColor)
        {

            TeescapeColor color = new TeescapeColor(styleId, styleName, sColor, sColor.Name == selectedColor.Name, true, selectedColor.IsDemo);
            color.Checked += (object sender, string sId, string sName, StyleColor styleColor, bool isChecked) =>
            {
                if (sender != null)
                {
                    foreach (TeescapeColor control in this.colorsControls.Controls)
                    {
                        if (control.getStyleColor().Value != styleColor.Value)
                        {
                            control.setCheck(false);
                        }
                    }
                    selectedColor = styleColor;

                    selectedTeescapeColor = (TeescapeColor)((CheckBox)sender).Parent;
                    opacityBar.Value = styleColor.Opacity;
                    CreatePreviewImage();
                }
            };
            //string styleReturnName,
            color.OpacityChanged += (string parentReturnId, classes.StyleColor styleReturnColor) =>
            {
                OpacityChanged(parentReturnId, styleReturnColor);
            };
            if (sColor.Name == selectedColor.Name)
            {
                selectedTeescapeColor = color;
            }
            colorsControls.Controls.Add(color);
        }

    }
}