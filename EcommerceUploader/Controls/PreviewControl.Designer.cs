﻿namespace Main.Controls
{
    partial class PreviewControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.ExportButton = new System.Windows.Forms.Button();
            this.opacityBar = new System.Windows.Forms.TrackBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RotateRadio = new System.Windows.Forms.RadioButton();
            this.tagRadio = new System.Windows.Forms.RadioButton();
            this.designRadio = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.colorsControls = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonSave = new System.Windows.Forms.Button();
            this.previewBox = new System.Windows.Forms.PictureBox();
            this.pictureBoxPreview = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opacityBar)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ExportButton);
            this.panel1.Controls.Add(this.opacityBar);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(250, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1004, 45);
            this.panel1.TabIndex = 15;
            // 
            // ExportButton
            // 
            this.ExportButton.BackColor = System.Drawing.Color.RoyalBlue;
            this.ExportButton.FlatAppearance.BorderSize = 0;
            this.ExportButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExportButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportButton.ForeColor = System.Drawing.Color.White;
            this.ExportButton.Location = new System.Drawing.Point(236, 7);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(67, 31);
            this.ExportButton.TabIndex = 17;
            this.ExportButton.Text = "Export";
            this.ExportButton.UseVisualStyleBackColor = false;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // opacityBar
            // 
            this.opacityBar.Location = new System.Drawing.Point(364, 17);
            this.opacityBar.Maximum = 100;
            this.opacityBar.Name = "opacityBar";
            this.opacityBar.Size = new System.Drawing.Size(128, 45);
            this.opacityBar.TabIndex = 16;
            this.opacityBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.opacityBar.Value = 100;
            this.opacityBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.opacity_MouseUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RotateRadio);
            this.groupBox1.Controls.Add(this.tagRadio);
            this.groupBox1.Controls.Add(this.designRadio);
            this.groupBox1.Location = new System.Drawing.Point(3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 38);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mode";
            // 
            // RotateRadio
            // 
            this.RotateRadio.AutoSize = true;
            this.RotateRadio.Location = new System.Drawing.Point(132, 15);
            this.RotateRadio.Name = "RotateRadio";
            this.RotateRadio.Size = new System.Drawing.Size(65, 17);
            this.RotateRadio.TabIndex = 2;
            this.RotateRadio.Text = "Rotation";
            this.RotateRadio.UseVisualStyleBackColor = true;
            this.RotateRadio.Visible = false;
            // 
            // tagRadio
            // 
            this.tagRadio.AutoSize = true;
            this.tagRadio.Location = new System.Drawing.Point(79, 15);
            this.tagRadio.Name = "tagRadio";
            this.tagRadio.Size = new System.Drawing.Size(44, 17);
            this.tagRadio.TabIndex = 1;
            this.tagRadio.Text = "Tag";
            this.tagRadio.UseVisualStyleBackColor = true;
            // 
            // designRadio
            // 
            this.designRadio.AutoSize = true;
            this.designRadio.Checked = true;
            this.designRadio.Location = new System.Drawing.Point(12, 15);
            this.designRadio.Name = "designRadio";
            this.designRadio.Size = new System.Drawing.Size(58, 17);
            this.designRadio.TabIndex = 0;
            this.designRadio.TabStop = true;
            this.designRadio.Text = "Design";
            this.designRadio.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.colorsControls);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 794);
            this.panel2.TabIndex = 16;
            // 
            // colorsControls
            // 
            this.colorsControls.AutoScroll = true;
            this.colorsControls.Location = new System.Drawing.Point(0, 45);
            this.colorsControls.Name = "colorsControls";
            this.colorsControls.Size = new System.Drawing.Size(250, 500);
            this.colorsControls.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonSave);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 45);
            this.panel3.TabIndex = 0;
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSave.FlatAppearance.BorderSize = 0;
            this.buttonSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.ForeColor = System.Drawing.Color.White;
            this.buttonSave.Location = new System.Drawing.Point(183, 7);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(61, 32);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.button1_Click);
            // 
            // previewBox
            // 
            this.previewBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewBox.Location = new System.Drawing.Point(250, 45);
            this.previewBox.Name = "previewBox";
            this.previewBox.Size = new System.Drawing.Size(1004, 749);
            this.previewBox.TabIndex = 13;
            this.previewBox.TabStop = false;
            this.previewBox.Paint += new System.Windows.Forms.PaintEventHandler(this.previewBox_Paint);
            this.previewBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.previewBox_MouseDown);
            this.previewBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.previewBox_MouseMove);
            this.previewBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.previewBox_MouseUp);
            // 
            // pictureBoxPreview
            // 
            this.pictureBoxPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPreview.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBoxPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPreview.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxPreview.Name = "pictureBoxPreview";
            this.pictureBoxPreview.Size = new System.Drawing.Size(820, 645);
            this.pictureBoxPreview.TabIndex = 12;
            this.pictureBoxPreview.TabStop = false;
            // 
            // PreviewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.previewBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBoxPreview);
            this.Name = "PreviewControl";
            this.Size = new System.Drawing.Size(1254, 794);
            this.Load += new System.EventHandler(this.PreviewControl_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opacityBar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBoxPreview;
        private System.Windows.Forms.PictureBox previewBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton tagRadio;
        private System.Windows.Forms.RadioButton designRadio;
        private System.Windows.Forms.TrackBar opacityBar;
        private System.Windows.Forms.RadioButton RotateRadio;
        private System.Windows.Forms.Button ExportButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.FlowLayoutPanel colorsControls;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonSave;
    }


}