﻿
using CefSharp;
using CefSharp.WinForms;
using Main.classes;
using Main.classes.chrome;
using Main.classes.data;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main.Controls
{
    public partial class ChromeItem : Form
    {
        public delegate void SaveHandler(string settingData);
        public event SaveHandler formCloseHandler;
        ShopeeSetting shopeeData;
        ColorsSetting colors;
        Chrome chrome;
        string MODE = "settingupload";
        public ChromeItem(Chrome _chrome,ShopeeSetting inputSetting, ColorsSetting _colors, string mode)
        {
            chrome = _chrome;
            this.MODE = mode;
            shopeeData = inputSetting;
            this.colors = _colors;
            InitializeComponent();
        }

        private void ChromeItem_Load(object sender, System.EventArgs e)
        {
            string workingdata;
            string url = System.AppDomain.CurrentDomain.BaseDirectory + "assets\\web\\index.html";
            if (MODE != "settingupload")
            {
                workingdata = JsonConvert.SerializeObject(colors);
                url = System.AppDomain.CurrentDomain.BaseDirectory + "assets\\web\\colorpage.html";
            }
            else
            {
                workingdata = JsonConvert.SerializeObject(shopeeData);
            }
            chrome = new Chrome(url, workingdata)
            {
                Dock = DockStyle.Fill
            };
            this.Controls.Add(chrome);
        }

        private async void ChromeItem_FormClosingAsync(object sender, FormClosingEventArgs e)
        {
            var domTask = this.chrome.browser.EvaluateScriptAsync("(function(){return $('#savebtn').click();})()");
            await Task.Delay(1000);
            if(this.formCloseHandler != null)
            {
                this.formCloseHandler(chrome.JsInteractive.getDataFromC());
            }
        }
    }
}
