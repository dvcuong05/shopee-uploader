﻿namespace Main.Controls
{
    partial class Logger
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logPanel = new System.Windows.Forms.Panel();
            this.copyLogBtn = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.lockBrowserChecbox = new System.Windows.Forms.CheckBox();
            this.shutdownCheckbox = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.totalRemainLabel = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.totalUploadedLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.totalItemLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.log = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.logPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // logPanel
            // 
            this.logPanel.BackColor = System.Drawing.Color.White;
            this.logPanel.Controls.Add(this.copyLogBtn);
            this.logPanel.Controls.Add(this.label18);
            this.logPanel.Controls.Add(this.lockBrowserChecbox);
            this.logPanel.Controls.Add(this.shutdownCheckbox);
            this.logPanel.Controls.Add(this.label20);
            this.logPanel.Controls.Add(this.totalRemainLabel);
            this.logPanel.Controls.Add(this.label19);
            this.logPanel.Controls.Add(this.totalUploadedLabel);
            this.logPanel.Controls.Add(this.label17);
            this.logPanel.Controls.Add(this.totalItemLabel);
            this.logPanel.Controls.Add(this.label15);
            this.logPanel.Controls.Add(this.log);
            this.logPanel.Controls.Add(this.label11);
            this.logPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.logPanel.Location = new System.Drawing.Point(0, 408);
            this.logPanel.Margin = new System.Windows.Forms.Padding(0);
            this.logPanel.Name = "logPanel";
            this.logPanel.Size = new System.Drawing.Size(543, 120);
            this.logPanel.TabIndex = 16;
            this.logPanel.Visible = false;
            // 
            // copyLogBtn
            // 
            this.copyLogBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyLogBtn.Location = new System.Drawing.Point(58, 3);
            this.copyLogBtn.Name = "copyLogBtn";
            this.copyLogBtn.Size = new System.Drawing.Size(56, 25);
            this.copyLogBtn.TabIndex = 37;
            this.copyLogBtn.Text = "copy log";
            this.copyLogBtn.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(26, 102);
            this.label18.Margin = new System.Windows.Forms.Padding(0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Lock browser";
            // 
            // lockBrowserChecbox
            // 
            this.lockBrowserChecbox.AutoSize = true;
            this.lockBrowserChecbox.Location = new System.Drawing.Point(8, 103);
            this.lockBrowserChecbox.Name = "lockBrowserChecbox";
            this.lockBrowserChecbox.Size = new System.Drawing.Size(15, 14);
            this.lockBrowserChecbox.TabIndex = 35;
            this.lockBrowserChecbox.UseVisualStyleBackColor = true;
            // 
            // shutdownCheckbox
            // 
            this.shutdownCheckbox.AutoSize = true;
            this.shutdownCheckbox.Location = new System.Drawing.Point(8, 84);
            this.shutdownCheckbox.Name = "shutdownCheckbox";
            this.shutdownCheckbox.Size = new System.Drawing.Size(125, 17);
            this.shutdownCheckbox.TabIndex = 9;
            this.shutdownCheckbox.Text = "Shutdown after done";
            this.shutdownCheckbox.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(8, 58);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 8;
            this.label20.Text = "-------------------";
            // 
            // totalRemainLabel
            // 
            this.totalRemainLabel.AutoSize = true;
            this.totalRemainLabel.BackColor = System.Drawing.Color.Transparent;
            this.totalRemainLabel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalRemainLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.totalRemainLabel.Location = new System.Drawing.Point(69, 68);
            this.totalRemainLabel.Name = "totalRemainLabel";
            this.totalRemainLabel.Size = new System.Drawing.Size(16, 16);
            this.totalRemainLabel.TabIndex = 7;
            this.totalRemainLabel.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 69);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Remaining";
            // 
            // totalUploadedLabel
            // 
            this.totalUploadedLabel.AutoSize = true;
            this.totalUploadedLabel.BackColor = System.Drawing.Color.Transparent;
            this.totalUploadedLabel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalUploadedLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.totalUploadedLabel.Location = new System.Drawing.Point(69, 45);
            this.totalUploadedLabel.Name = "totalUploadedLabel";
            this.totalUploadedLabel.Size = new System.Drawing.Size(16, 16);
            this.totalUploadedLabel.TabIndex = 5;
            this.totalUploadedLabel.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 45);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Uploaded:";
            // 
            // totalItemLabel
            // 
            this.totalItemLabel.AutoSize = true;
            this.totalItemLabel.BackColor = System.Drawing.Color.Transparent;
            this.totalItemLabel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalItemLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.totalItemLabel.Location = new System.Drawing.Point(69, 28);
            this.totalItemLabel.Name = "totalItemLabel";
            this.totalItemLabel.Size = new System.Drawing.Size(16, 16);
            this.totalItemLabel.TabIndex = 3;
            this.totalItemLabel.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Total:";
            // 
            // log
            // 
            this.log.BackColor = System.Drawing.SystemColors.Menu;
            this.log.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.log.Location = new System.Drawing.Point(139, 6);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(399, 111);
            this.log.TabIndex = 1;
            this.log.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "STATUS:";
            // 
            // Logger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.logPanel);
            this.Name = "Logger";
            this.Size = new System.Drawing.Size(543, 528);
            this.logPanel.ResumeLayout(false);
            this.logPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel logPanel;
        private System.Windows.Forms.Button copyLogBtn;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox lockBrowserChecbox;
        private System.Windows.Forms.CheckBox shutdownCheckbox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label totalRemainLabel;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label totalUploadedLabel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label totalItemLabel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RichTextBox log;
        private System.Windows.Forms.Label label11;
    }
}
