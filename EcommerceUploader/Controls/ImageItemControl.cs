﻿
using Main.classes;
using NCCLibs;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Main.Controls
{
    public partial class ImageItemControl : UserControl
    {
        public delegate void RemoveClickedHandler(ImageItemControl control, ImageItem img);
        public event RemoveClickedHandler RemoveClicked;

        private ImageItem img;
        private Image thumbnailImage = null;
        private string styleId;
        private string previewDesignPath;
        public ImageItemControl(ImageItem img, string styleId, string previewDesignPath)
        {
            this.img = img;
            this.styleId = styleId;
            //this.originalStyles = img!=null && img.Colors != null && img.Colors.Count >0 ? originalStyles;
            this.previewDesignPath = previewDesignPath;
            InitializeComponent();
        }

        public void updateRectDraw(ImageItem upatedImageInfo)
        {
            this.img.Rect = upatedImageInfo.Rect;
        }

        private void panel_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                using (var bmpTemp = new Bitmap(img.Thumbnail))
                {
                    thumbnailImage = new Bitmap(bmpTemp);
                }
                pictureBox.Image = thumbnailImage;
                label.Text = img.Name;
                this.checkBoxSelect.Checked = this.img.IsChecked;
                if (this.img.DisplayMode)
                {
                    this.checkBoxSelect.Visible = false;
                    this.delete.Visible = false;
                }
            }
            catch (System.Exception) { }
            
        }

        private void delete_Click(object sender, System.EventArgs e)
        {
            DialogResult val = MessageBox.Show("Are you sure to delete this image?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (val == DialogResult.Yes)
            {
                pictureBox.Image.Dispose();
                thumbnailImage.Dispose();
                RemoveClicked(this, img);
            }
        }

        public void ReleaseImg()
        {
            try
            {
                pictureBox.Image.Dispose();
                thumbnailImage.Dispose();
            }
            catch (System.Exception ex)
            {
            }
        }

        public bool isCheckedItem()
        {
            return this.checkBoxSelect.Checked;
        }

        public ImageItem getSelectedMockupData()
        {
            return img;
        }

        private void ImageItemControl_DoubleClick(object sender, System.EventArgs e)
        {
        }

        private void pictureBox_DoubleClick(object sender, System.EventArgs e)
        {

            //if (img.DisplayMode)
            {
                ImageItem background = img;//
                Form f = new Form();
                f.Width = 750;
                f.Height = 650;
                f.StartPosition = FormStartPosition.CenterScreen;
                ImageItem brandTag = ToolUtils.BRAND_TAG != null & ToolUtils.BRAND_TAG.Count > 0 ? ToolUtils.BRAND_TAG[0] : null ;
                //TeescapeStyle selectedRootStyle = this.originalStyles.Find(s => s.Id == styleId);
                Controls.PreviewControl p = new Controls.PreviewControl("tayngan", ToolUtils.COLOR_SETTING.Colors, background, new ImageItem()
                {
                    FullPath = previewDesignPath,
                    Colors = img.Colors
                }, brandTag);
                //string styleReturnName,
                p.OpacityChanged += (string parentReturnId,  classes.StyleColor styleReturnColor) =>
                {

                };

                p.UpdateData += (ImageItem img, string colorName) =>
                {
                    background.Rect = img.Rect;
                    background.AdditionalRect = img.AdditionalRect;
                    background.Angle = img.Angle;
                    background.Opacity = img.Opacity;
                    background.Colors = img.Colors;
                    // save to disck
                    //UpdateData(background, "tayngan", colorName);
                    DrawSelectedMockup(background, "tayngan");
                };

                f.Controls.Add(p);
                f.FormClosing += Preview_FormClosing;
                f.ShowDialog();
            }
        }

        private void DrawSelectedMockup(ImageItem updatedMockup, string styleId)
        {
            string currentVariantMockupFolder = ToolUtils.getMockupConfigPath(styleId);
            if(ToolUtils.MOCKUP_IMAGES == null)
            {
                ToolUtils.MOCKUP_IMAGES = new List<ImageItem>();
            }
            bool foundExisting = false;
            foreach (NCCLibs.ImageItem item in ToolUtils.MOCKUP_IMAGES)
            {
                // Clone các phần chung về ô vẽ hình và opacity. phần riêng về tag position, độ xoay lưu riêng
                item.Rect = updatedMockup.Rect;
                item.Opacity = updatedMockup.Opacity;
                if(item.Name == updatedMockup.Name)//là item đang updated => lưu thêm phần về tag
                {
                    foundExisting = true;
                    item.AdditionalRect = updatedMockup.AdditionalRect;
                    item.Angle = updatedMockup.Angle;
                }
            }
            if (!foundExisting)
            {
                ToolUtils.MOCKUP_IMAGES.Add(updatedMockup);
            }
            string tmp2 = currentVariantMockupFolder + @"\mockups.json";
            ToolUtils.WriteFile<List<ImageItem>>(tmp2, ToolUtils.MOCKUP_IMAGES);
            // currentSelectedMockups = selectedMockups;
            //saveCurrentSelectedMockups();
        }

        private void updateOriginalColor(string sId, StyleColor styleColor)
        {
            // store
          //  TeescapeStyle os = originalStyles.Find(s => s.Id == sId);
            //StyleColor osc = os.Colors.Find(c => c.Name == styleColor.Name);
           // osc.Opacity = styleColor.Opacity;
            // TODO need to save to local data config/style.json
           // string updatedVariantStyleColor = JsonConvert.SerializeObject(this.originalStyles);
           // ToolUtils.saveVariantStyleSetup(updatedVariantStyleColor);
        }

        private void Preview_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form form = (Form)sender;
            MessageBoxManager.Yes = "Save and close";
            MessageBoxManager.No = "Just close";
            DialogResult val = MessageBox.Show("Do you want to save your changes before exit?", "Close form", MessageBoxButtons.YesNoCancel);
            if (val == DialogResult.Yes)
            {
                Controls.PreviewControl p = (Controls.PreviewControl)form.Controls[0];
                p.saveData();
            }
            else if (val == DialogResult.Cancel)
            {
                e.Cancel = true;
                
                form.Activate();
            }
        }
    }
}
