﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Main.classes;
using Main.classes.data;

namespace Main.Controls
{
    public partial class LoginControl : UserControl
    {
        private Chrome chrome;
        public delegate void FinishLoginHandler(string username, string password, string store, string baseUrl);
        public event FinishLoginHandler FinishLogin;

        public delegate void ModeChanged(string mode);
        public event ModeChanged modeChanged;
        private BindingSource bs = new BindingSource();

        public LoginControl(Chrome chrome)
        {
            InitializeComponent();
            this.chrome = chrome;
            this.chrome.LoginHandler += LoginSuccess;
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {

            //START DEBUG login already
            //chrome.LoginOnBrowser("https://banhang.shopee.vn/account/signin", "https://" + store.Text + ".myshopify.com/admin", userNameCombox.Text, password.Text);
            //LoginSuccess("https://"+ store.Text+".myshopify.com");
            //return;
            // END DEBUG
            chrome.browser.Load("https://banhang.shopee.vn/account/signin");
            if (userNameCombox.Text == "" || password.Text == "")
            {
                MessageBox.Show("vui lòng nhập thông tin login!", "Opps", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                RemeberMe(userNameCombox.Text, password.Text, "");
                modeChanged("login");
                //chrome.LoginOnBrowser("https://" + store.Text + ".myshopify.com/admin", userNameCombox.Text, password.Text);
                chrome.LoginOnBrowser("https://banhang.shopee.vn/account/signin", "", userNameCombox.Text, password.Text);
            }
        }

        private void RemeberMe(string username, string password, string store)
        {
            try
            {
                Authentication data = new Authentication();
                data.ShopeeUser = username;
                data.ShopeePass = password;
                ToolUtils.saveAuthentication(data);
            }
            catch (Exception)
            {

            }
        }

        private void LoginControl_Load(object sender, EventArgs e)
        {
            Left = (Parent.Width - Width) / 2;
            Top = (Parent.Height - Height) / 2;
            SetAuthentication();

            //Load all store configed to display on login form
            bs.DataSource = ToolUtils.getAuthentication();
            this.userNameCombox.DataSource = bs;
            this.userNameCombox.DisplayMember = "shopeeUser";

            try
            {
                labelVersion.Text = "V " + Assembly.GetExecutingAssembly().GetName().Version;
            }
            catch (Exception ee) { }
        }

        private void SetAuthentication()
        {
            try
            {
                String filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/authenticate.txt";
                String fileContent = File.ReadAllText(filePath);
                string[] data = fileContent.Split('\n');
                string username = ToolUtils.Decrypt(data[0]);
                string password = ToolUtils.Decrypt(data[1]);
                string storeName = ToolUtils.Decrypt(data[2]);
                this.userNameCombox.Text = username;
                this.password.Text = password;
            }
            catch (Exception)
            {

            }
        }

        public void LoginSuccess(string baseUrl)
        {
            userNameCombox.Enabled = true;
            password.Enabled = true;
            buttonLogin.Enabled = true;
            ToolUtils.currentAuthentication = new Authentication();
            ToolUtils.currentAuthentication.ShopeeUser = userNameCombox.Text;
            ToolUtils.currentAuthentication.ShopeePass = password.Text;

            FinishLogin(userNameCombox.Text, password.Text, "", baseUrl);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            modeChanged("setup");
        }

        private void LoginControl_Paint(object sender, PaintEventArgs e)
        {
            //Left = (Parent.Width - Width) / 2;
            //Top = (Parent.Height - Height) / 2;
        }

        private void userNameCombox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //ToolUtils.appConfig = (AppConfig)this.userNameCombox.SelectedItem;
                Authentication selectData = (Authentication)this.userNameCombox.SelectedItem;
                this.password.Text = ToolUtils.Decrypt(selectData.ShopeePass);
            }
            catch (Exception ex) { }
        }
    }
}
