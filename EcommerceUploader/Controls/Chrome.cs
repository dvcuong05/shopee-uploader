﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using Main.classes;
using System.IO;
using Main.classes.chrome;

namespace Main.Controls
{
    public partial class Chrome : UserControl
    {
        public ChromiumWebBrowser browser;
        private bool stopApp = false;

        public delegate void FinishLoginHandler(string storeUrl);
        public event FinishLoginHandler LoginHandler;

        public delegate void FinishLogoutHandler();
        public event FinishLogoutHandler LogoutHandler;

        public delegate void Logger(string text);
        public event Logger WirteLog;


        public delegate void IsReadyUpload(int total, bool isRegisteredBrand, string colors);
        public event IsReadyUpload IsReadyForUpload;

        public delegate void Published();
        public event Published IsPublished;

        public delegate void FinishedUpload();
        public event FinishedUpload IsFinishedUpload;

        JavaScriptInteractionObj jsInteractive;

        internal JavaScriptInteractionObj JsInteractive { get => jsInteractive; set => jsInteractive = value; }

        public Chrome(string url, string chromeLocalData)
        {
            InitializeComponent();

            CefSettings settings = new CefSettings
            {
                PersistSessionCookies = true,
                IgnoreCertificateErrors = true,
                RemoteDebuggingPort = 8088,
                LogSeverity = LogSeverity.Error,
                //CachePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\assets\\caches"
            };
            settings.RegisterScheme(new CefCustomScheme
            {
                SchemeName = LocalSchemeHandlerFactory.SchemeName,
                SchemeHandlerFactory = new LocalSchemeHandlerFactory()
            });
            Cef.EnableHighDPISupport();
            if (!Cef.IsInitialized)
            {
                Cef.Initialize(settings);
            }
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;
            BrowserSettings bs = new BrowserSettings
            {
                WebSecurity = CefState.Disabled
            };
            browser = new ChromiumWebBrowser(url == null ? "" : url)
            {
                JsDialogHandler = new JsHandler(),
                BrowserSettings = bs,
                Dock = DockStyle.Fill
            };
            JsInteractive = new JavaScriptInteractionObj(browser, chromeLocalData);
            this.browser.RegisterJsObject("winformObj", JsInteractive);
        }

        private void Chrome_Load(object sender, EventArgs e)
        {
            browser.IsBrowserInitializedChanged += OnIsBrowserInitializedChanged2;
            Controls.Add(browser);
        }

        private void OnIsBrowserInitializedChanged2(object sender, EventArgs e)
        {
            if (1==1)
            {
                try
                {
                    String pathStr = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/assets/configs/debuger.txt";
                    String isEnableStr = File.ReadAllText(pathStr);
                    if (isEnableStr == "true")
                    {
                        browser.ShowDevTools();
                    }
                }
                catch (Exception) { }
            }
        }

        private void ChromeIsReadyForUpload(int total, bool isRegisteredBrand, string colors)
        {
            IsReadyForUpload(total, isRegisteredBrand, colors);
        }
        private void ChromeIsPublished()
        {
            IsPublished();
        }
        private void ChormeIsFinishedUpload()
        {
            IsFinishedUpload();
        }
        // login
        public void LoginOnBrowser(string url, string storeName, string username, string password)
        {
            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    browser.LoadingStateChanged -= handler;
                    if (!this.stopApp)
                    {
                        browser.Invoke((MethodInvoker)delegate
                        {
                            CheckingLoginForm(storeName, username, password);
                            WirteLog("Login...");
                        });
                       
                    }
                }
            };
            browser.LoadingStateChanged += handler;
            browser.Load(url);
        }
        // process login
        private async void CheckingLoginForm(string storeName, string username, string password)
        {
            try
            {
                await Task.Delay(1000);
                var domTask = browser.EvaluateScriptAsync("(function(){if(window.location.href=='https://banhang.shopee.vn/'){return 200;};return document.getElementsByClassName('signin-panel').length;})()");
                await domTask.ContinueWith(t =>
                {
                   if (!t.IsFaulted)
                    {
                        var response = t.Result;
                        if (response.Success && response.Result != null)
                        {
                            int codeNum = Convert.ToInt32(response.Result.ToString());
                            if(codeNum == 200)
                            {
                                this.browser.Load("https://banhang.shopee.vn/account/signin");
                                FinishLogin(storeName, username, password);
                            }
                            else if(codeNum > 0) { 
                                browser.Invoke((MethodInvoker)async delegate
                                {
                                    browser.ExecuteScriptAsync("(function(userName,password){let event=new Event('input',{bubbles:true});event.simulated=true;var inputs=document.querySelectorAll('.shopee-input__input');inputs[0].value=userName;inputs[0].dispatchEvent(event);let event2=new Event('blur',{bubbles:true});event2.simulated=true;var inputs=document.querySelectorAll('.shopee-input__input');inputs[0].dispatchEvent(event2);inputs[1].value=password;inputs[1].dispatchEvent(event);document.getElementsByClassName('shopee-checkbox__input')[0].click();setTimeout(()=>{document.getElementsByClassName('shopee-button--primary')[0].click();},1500)})('" + username + "','" + password + "')");
                                   // await Task.Delay(2000);
                                   // browser.ExecuteScriptAsync("(function(username,password){var evt=document.createEvent('MouseEvents');evt.initEvent('input',true,true);var form=$('form');var email=form.find('[id=account_email]')[0];email.value=username;email.dispatchEvent(evt);setTimeout(function(){try{var LoginSubmit=form.find('[type=submit]')[0];LoginSubmit.click();}catch(e){}},1000)})('" + username + "','" + password + "')");
                                    FinishLogin(storeName, username, password);
                                });
                            }
                            else
                            {
                                browser.Invoke((MethodInvoker)delegate
                                {
                                    CheckingLoginForm(storeName, username, password);
                                });
                            }
                        }
                        else
                        {
                            browser.Invoke((MethodInvoker)delegate
                            {
                                WirteLog(response.Message);
                            });
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("checkingLoginForm has error:" + ex.Message);
            }
        }
        private void FinishLogin(string storeName, string username, string password)
        {
            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading && !args.Browser.MainFrame.Url.Contains("/login"))
                {
                    browser.LoadingStateChanged -= handler;
                    if (!this.stopApp)
                    {
                        browser.Invoke((MethodInvoker)delegate
                        {
                            LoginHandler("");
                        });
                    }
                }
                else
                {
                    WirteLog("The login cannot pass because capcha..please play with it.");
                    browser.Invoke((MethodInvoker)async delegate
                    {

                        await Task.Delay(5000);
                        browser.ExecuteScriptAsync("(function(userName,password){let event=new Event('input',{bubbles:true});event.simulated=true;var inputs=document.querySelectorAll('.shopee-input__input');inputs[0].value=userName;inputs[0].dispatchEvent(event);inputs[1].value=password;inputs[1].dispatchEvent(event);document.getElementsByClassName('shopee-checkbox__input')[0].click();setTimeout(()=>{document.getElementsByClassName('shopee-button--primary')[0].click();},1500)})('" + username + "','" + password + "')");
                    });
                }
            };
            browser.LoadingStateChanged += handler;
        }

        public void LogoutOnBrowser()
        {
            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    browser.LoadingStateChanged -= handler;
                    browser.Invoke((MethodInvoker)delegate
                    {
                        LogoutHandler();
                    });
                    
                }
            };
            browser.LoadingStateChanged += handler;
            this.browser.EvaluateScriptAsync("(function(){var accountDom=document.querySelectorAll('.user-info-box');if(accountDom&&accountDom.length>0){var spans=accountDom[0].querySelectorAll('span');spans[spans.length-1].click();}})()");
            browser.ExecuteScriptAsync("(function(){document.cookie='subdomain=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';document.cookie ='last_shop=;expires=Thu, 01 Jan 1970 00:00:01 GMT;domain=.shopify.com';$('#subdomain').val('');})()");
            //browser.Load("https://www.shopify.com/logout");
         }
    }
}
