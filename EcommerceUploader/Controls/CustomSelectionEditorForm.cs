﻿using Main.classes.data;
using NCCLibs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Security;
using System.Windows.Forms;

namespace Main.Controls
{
    public partial class CustomSelectionEditorForm : Form
    {
        public delegate void CloseFormHandler(List<ImageItem> selectedMockups, string styleId);
        public event CloseFormHandler closeFormHandler;

        private string path;
        private string styleId = "tayngan";
        Chrome chrome;

        public CustomSelectionEditorForm(Chrome _chrome, string styleId)
        {
            this.chrome = _chrome;
            this.styleId = styleId;
            string currentVariantFolder = @"\assets\configs\mockups\";
            currentVariantFolder = ToolUtils.BASE_FOLDER + currentVariantFolder + styleId;
            DirectoryUtil.createFolder(currentVariantFolder);
            path = currentVariantFolder;
            InitializeComponent();
        }

        private void CustomSelectionEditorForm_Load(object sender, EventArgs e)
        {
            LoadImages();
        }

        private void open_Click(object sender, EventArgs e)
        {
            browseFolder();
        }

        private void LoadImages()
        {
            Content.Controls.Clear();
            List<ImageItem> images = DirectoryUtil.GetImages(this.path, ToolUtils.ACCEPT_IMAGE_TYPE, true);
            ToolUtils.MOCKUP_IMAGES = images;
            string mockupJsonPath = ToolUtils.getMockupConfigPath(styleId) + @"\mockups.json";
            List<ImageItem> mockupHasJsons = ToolUtils.ReadFile<List<ImageItem>>(mockupJsonPath);
            if(mockupHasJsons != null && mockupHasJsons.Count > 0)
            {
                for (var i = 0; i < ToolUtils.MOCKUP_IMAGES.Count; i++)
                {
                    for (var j = 0; j < mockupHasJsons.Count; j++)
                    {
                        if (mockupHasJsons[j].Name == ToolUtils.MOCKUP_IMAGES[i].Name)
                        {
                            ToolUtils.MOCKUP_IMAGES[i] = mockupHasJsons[j];
                        }
                    }
                }
            }
            ShowImages(ToolUtils.MOCKUP_IMAGES, false);
        }
        private void ShowImages(List<ImageItem> images, bool isAddMore)
        {
            foreach (ImageItem image in images)
            {
                if (isAddMore)
                {
                    ToolUtils.MOCKUP_IMAGES.Add(image);
                }
                ImageItemControl imageItemControl = new ImageItemControl(image, styleId, ToolUtils.BASE_FOLDER + @"\assets\pictures\demo.png");
                imageItemControl.RemoveClicked += RemoveClicked;
                Content.Controls.Add(imageItemControl);
            }
        }
        private void RemoveClicked(ImageItemControl control, ImageItem img)
        {
            try
            {
                control.ReleaseImg();
                Content.Controls.Remove(control);
                control.Dispose();
                DirectoryUtil.DeleteFile(img.FullPath);
                DirectoryUtil.DeleteFile(img.Thumbnail);
                DirectoryUtil.DeleteFile(img.AdditionalImageFullPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot delete images. Detail:" + ex.Message, "Error");
            }
        }

        private void browseFolder()
        {
            OpenFileDialog browse = new OpenFileDialog();
            browse.Filter = "Images (*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.PNG| All files (*.*)|*.*";
            browse.Multiselect = true;
            browse.FileName = "";
            DialogResult dr = browse.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                List<ImageItem> images = DirectoryUtil.ExtractImages(browse.FileNames, null, true);
                List<ImageItem> displayedImages = new List<ImageItem>();
                foreach (ImageItem img in images)
                {
                    try
                    {
                        string copyLink = path + "\\" + img.Name + img.Extendsion;
                        if (!DirectoryUtil.IsExistedFile(copyLink))
                        {
                            DirectoryUtil.CopyFile(img.FullPath, copyLink);
                            if (img.AdditionalImageFullPath != null)
                            {
                                string copyLinkAdditional = path + "\\" + System.IO.Path.GetFileName(img.AdditionalImageFullPath);
                                if (!DirectoryUtil.IsExistedFile(copyLinkAdditional))
                                {
                                    DirectoryUtil.CopyFile(img.AdditionalImageFullPath, copyLinkAdditional);
                                    img.AdditionalImageFullPath = copyLinkAdditional;
                                }
                            }
                            img.FullPath = copyLink;
                            img.Thumbnail = path + "/" + img.Name + "#THUMB.PNG";
                            Image originalImage = Image.FromFile(img.FullPath);
                            Image thumb = originalImage.GetThumbnailImage(100, 112, () => false, IntPtr.Zero);
                            thumb.Save(img.Thumbnail, ImageFormat.Png);
                            thumb.Dispose();
                            originalImage.Dispose();
                            img.DirectoryPath = System.IO.Path.GetDirectoryName(img.FullPath);
                            displayedImages.Add(img);
                        }
                    }
                    catch (SecurityException ex)
                    {
                        // The user lacks appropriate permissions to read files, discover paths, etc.
                        MessageBox.Show("Security error. Please contact your administrator for details.\n\n" +
                            "Error message: " + ex.Message + "\n\n" +
                            "Details (send to Support):\n\n" + ex.StackTrace
                        );
                    }
                    catch (Exception ex)
                    {
                        // Could not load the image - probably related to Windows file system permissions.
                        MessageBox.Show("Cannot display the image: " + img.Name
                            + ". You may not have permission to read the file, or " +
                            "it may be corrupt.\n\nReported error: " + ex.Message);
                    }
                }
                ShowImages(displayedImages, true);
            }

        }

        private void CustomSelectionEditorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            List<ImageItem> selectedMockups = new List<ImageItem>();
            foreach (ImageItemControl imageItemControl in this.Content.Controls)
            {
                if (imageItemControl.isCheckedItem())
                {
                    selectedMockups.Add(imageItemControl.getSelectedMockupData());
                }
            }
            List<ImageItem> images = DirectoryUtil.GetImages(this.path, ToolUtils.ACCEPT_IMAGE_TYPE, true);
            ToolUtils.loadMockupImages();
            closeFormHandler(ToolUtils.MOCKUP_IMAGES, styleId);
        }

        private void linkLabelHelp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                String filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/assets/pictures/mockup-setting.png";
                System.Diagnostics.Process photoViewer = new System.Diagnostics.Process();
                photoViewer.StartInfo.FileName = @filePath;
                photoViewer.StartInfo.Arguments = @filePath;
                photoViewer.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot show example image with follow error is: " + ex.Message);
            }
        }

        private void buttonSetupColor_Click(object sender, EventArgs e)
        {
            ChromeItem chromeItem = new ChromeItem(chrome, null, ToolUtils.COLOR_SETTING, "colorsetting");
            chromeItem.formCloseHandler += saveParentShopeeSetting;
            chromeItem.ShowDialog();
        }
        private void saveParentShopeeSetting(string colorSettingData)
        {
            if (colorSettingData == null)
            {
                return;
            }
            ColorsSetting savedData = JsonConvert.DeserializeObject<ColorsSetting>(colorSettingData);
            if (savedData != null)
            {
                ToolUtils.COLOR_SETTING = savedData;
                ToolUtils.saveColorSetting(savedData);
                ToolUtils.loadTagImages();
            }
        }

    }
}
