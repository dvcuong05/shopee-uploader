﻿
using NCCLibs;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class ExportForm : Form
    {
        private FlowLayoutPanel listItemFlow;
        private List<TeescapeStyle> styles;
        private List<ImageItem> brandTags;
        public ExportForm(FlowLayoutPanel listItemFlow, List<TeescapeStyle> styles, List<ImageItem> brandTags)
        {
            this.listItemFlow = listItemFlow;
            this.styles = styles;
            this.brandTags = brandTags;
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private async void ExportForm_Load(object sender, EventArgs e)
        {
            Enabled = false;
            string output = ToolUtils.FolderPath + @"/mockups";
            if (!DirectoryUtil.IsExistedFile(output))
            {
                DirectoryUtil.createFolder(output);
            }
            foreach (TeescapeItemPanel panel in listItemFlow.Controls)
            {
                var pickedStyles = ToolUtils.Clone(styles);
                ShopeeItemSetting item = panel.getShopeeItemSetting();
               // if(item.Styles != null && item.Styles.Count > 0)
               // {
               //     pickedStyles = item.Styles;
               // }
                for (int i = 0; i < pickedStyles.Count; i++)
                {
                    var currentStyle = pickedStyles[i];
                    var tasks = new List<Task>();
                    for (int i1 = 0; i1 < currentStyle.Colors.Count; i1++)
                    {
                        if (currentStyle.SelectedMockups != null && currentStyle.SelectedMockups.Count > 0)
                        {
                            ImageItem background = currentStyle.SelectedMockups[ToolUtils.GetNextBgFile(currentStyle.SelectedMockups.Count)];
                            //Correct mockup path current installation location
                            background.DirectoryPath = System.IO.Path.Combine(ToolUtils.BASE_FOLDER + "assets\\configs\\mockups", DirectoryUtil.GetDirectoryName(background.DirectoryPath));
                            background.AdditionalImageFullPath = System.IO.Path.Combine(background.DirectoryPath, System.IO.Path.GetFileName(background.AdditionalImageFullPath));
                            background.Thumbnail = System.IO.Path.Combine(background.DirectoryPath, System.IO.Path.GetFileName(background.Thumbnail));
                            background.FullPath = System.IO.Path.Combine(background.DirectoryPath, System.IO.Path.GetFileName(background.FullPath));

                            ImageItem tag = null;
                            if (brandTags != null && brandTags.Count > 0)
                            {
                                tag = brandTags[ToolUtils.GetNextTagFile(brandTags.Count)];
                                tag.Angle = background.Angle;
                                tag.Rect = background.AdditionalRect;
                            }
                            ImageItem effect = new ImageItem()
                            {
                                FullPath = background.FullPath,
                                //Opacity = currentStyle.Colors[i1].Opacity //Lay opacity cua mau cung ten trong mockup.colors
                                Opacity = ToolUtils.getOpacity(currentStyle.Colors[i1].Opacity, currentStyle.Colors[i1].Name, background)
                            };
                            ImageItem bg = new ImageItem()
                            {
                                FullPath = background.AdditionalImageFullPath
                            };
                            ImageItem designItem = new ImageItem()
                            {
                                FullPath = item.TmpFullPath,
                                Rect = background.Rect
                            };
                            string fileName = item.Name + "#" + currentStyle.Label + "#" + currentStyle.Colors[i1].Name + ".jpeg";
                            string outputName = output + @"\" + fileName;
                            string colorCode = currentStyle.Colors[i1].ColorCode;
                            tasks.Add(exportAnImg(colorCode, bg, effect, designItem, tag, outputName));
                        }
                    }
                    await Task.WhenAll(tasks);
                }
            }
            Enabled = true;
            MessageBox.Show("DONE");
            Close();
        }

        private async Task exportAnImg(string ColorCode, ImageItem background, ImageItem effect, ImageItem designItem,
            ImageItem tag, string outputName)
        {
            await Task.Run(() =>
            {
                var createdImg = ImageUtil.merged(1000, 0, 500, ColorCode, background, effect, designItem, tag);
                if (createdImg != null)
                {
                    createdImg.Save(outputName, ImageFormat.Jpeg);
                    createdImg.Dispose();
                    createdImg = null;
                }
            });
        }
    }
}
