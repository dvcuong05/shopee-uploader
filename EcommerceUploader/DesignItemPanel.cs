﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class DesignItemPanel : UserControl
    {
        public DesignItemPanel(String imgPath,int counter)
        {
            InitializeComponent();
            this.pictureBox1.ImageLocation = imgPath;
            this.teeLink.Text = this.teeLink.Text + counter + ":";
            this.teeName.Text = this.teeName.Text + counter + ":";
            
            this.teeLinkTxt.Text = imgPath;            
            this.teeNameTxt.Text = imgPath;

            ToolTip teeLinkTxtTooltip = new ToolTip();
            teeLinkTxtTooltip.SetToolTip(this.teeLinkTxt, imgPath);

            ToolTip teeNameTxtTooltip = new ToolTip();
            teeNameTxtTooltip.SetToolTip(this.teeNameTxt, imgPath);

            ToolTip copyBtnTooltip = new ToolTip();
            copyBtnTooltip.SetToolTip(this.copyBtn,"Copy keyword list");

            ToolTip pasteBtnTooltip = new ToolTip();
            pasteBtnTooltip.SetToolTip(this.pasteBtn, "Paste keywords just coppied!");

            ToolTip clearBtnTooltip = new ToolTip();
            clearBtnTooltip.SetToolTip(this.clearBtn, "Clear all keywords");
        }

        public String getTeeName(){
            return this.teeNameTxt.Text;
        }

        public String getTeeLink()
        {
            return this.teeLinkTxt.Text;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void editItemUrlBtn_Click(object sender, EventArgs e)
        {

        }

        private void editItemNameBtn_Click(object sender, EventArgs e)
        {

        }

        private void uploadItemBtn_Click(object sender, EventArgs e)
        {
            
        }

        private void teeLinkTxt_TextChanged(object sender, EventArgs e)
        {

        }
        private void teeLinkTxt_Enter(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.Show("Test tooltip", this.teeLinkTxt, 0);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void copyBtn_Click(object sender, EventArgs e)
        {
            TeespringUtils.keywords.Clear();
            System.Collections.ArrayList keywords = new System.Collections.ArrayList();
            TeespringUtils.keywords.Add(this.keyword1.Text);
            TeespringUtils.keywords.Add(this.keyword2.Text);
            TeespringUtils.keywords.Add(this.keyword3.Text);
            TeespringUtils.keywords.Add(this.keyword4.Text);
            TeespringUtils.keywords.Add(this.keyword5.Text);
            TeespringUtils.keywords.Add(this.keyword6.Text);
            TeespringUtils.keywords.Add(this.keyword7.Text);
            TeespringUtils.keywords.Add(this.keyword8.Text);
            TeespringUtils.keywords.Add(this.keyword9.Text);
            TeespringUtils.keywords.Add(this.keyword10.Text);            
        }

        private void pasteBtn_Click(object sender, EventArgs e)
        {
            for(int i =0; i<TeespringUtils.keywords.Count; i++){
                this.Controls["keyword" + (i+1)].Text = TeespringUtils.keywords[i].ToString();
            }            
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                this.Controls["keyword" + (i + 1)].Text = "";
            } 
        }
    }
}
