﻿using System;
using System.Windows.Forms;

namespace Main
{

    public partial class TeescapeColor : UserControl
    {
        private Boolean isChecked = false;
        private string parentId;
        private Boolean isCheckboxInitial = true;
        private classes.StyleColor styleColor;
        private string styleName;
        public delegate void IsCheckedHandler(object sender, string parentId,
        string styleName, classes.StyleColor styleColor, Boolean isChecked);
        public event IsCheckedHandler Checked;
        private bool isDemo = false;
        public delegate void OpacityChangeHandler(string parentId,classes.StyleColor styleColor);
        public event OpacityChangeHandler OpacityChanged;
        bool isRunning = true;
        bool isPreviewMode = false;

        public TeescapeColor(string parentId, string styleName, classes.StyleColor styleColor, Boolean isChecked, Boolean isPreviewMode, Boolean isDemo)
        {
            this.isChecked = isChecked;
            this.parentId = parentId;
            this.styleColor = styleColor;
            this.styleName = styleName;
            this.isDemo = isDemo;
            this.isPreviewMode = isPreviewMode;
            InitializeComponent();
        }

        public classes.StyleColor getStyleColor()
        {
            return styleColor;
        }

        public void setOpacity(int val)
        {
            this.linkLabelOpacity.Text = val.ToString();
            this.textBoxOpacity.Text = val.ToString();
        }

        private void TeescapeColor_Load(object sender, EventArgs e)
        {
            System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml(styleColor.ColorCode);
            color.BackColor = col;
            isCheckboxInitial = isChecked;

            check.Checked = isChecked;
            if (styleColor.Opacity >= 0 || styleColor.Opacity <= 100)
            {
                this.linkLabelOpacity.Text = styleColor.Opacity.ToString();
                this.textBoxOpacity.Text = styleColor.Opacity.ToString();
            }
            //An trong che do Teescape selection.. cho phep setuo trong mockup thoi
            if (!this.isPreviewMode)
            {
                this.linkLabelOpacity.Visible = false;
                this.textBoxOpacity.Visible = false;
            }
            if (!this.isDemo)
            {
                this.color.Text = "";
                this.color.Padding = new Padding(10, 1, 10, 1);
            }
            else
            {
                this.color.Text = "✔";
                this.color.Padding = new Padding(1, 1, 1, 1);
            }
        }

        private void check_CheckedChanged(object sender, EventArgs e)
        {
            if (isCheckboxInitial)
            {
                isCheckboxInitial = false;
                return;
            }
            try
            {
                int opa = 100;
                if (!string.IsNullOrEmpty(textBoxOpacity.Text))
                {
                    opa = Int32.Parse(textBoxOpacity.Text);
                }
                styleColor.Opacity = opa;
                Checked(sender, parentId, styleName, styleColor, check.Checked);
            }
            catch
            {

            }
        }

        private void linkLabelOpacity_DoubleClick(object sender, EventArgs e)
        {
            isRunning = false;
            focusTextboxOpacity();
            if (this.isPreviewMode)
            {
                this.check.Checked = true;
                check_CheckedChanged(this, null);
            }
        }

        private void textBoxOpacity_Leave(object sender, EventArgs e)
        {
            changeOpacityBoxToDisplayMode();
        }

        private void changeOpacityBoxToDisplayMode()
        {
            linkLabelOpacity.Visible = true;
            linkLabelOpacity.Text = textBoxOpacity.Text;
            textBoxOpacity.Visible = false;
        }

        private void focusTextboxOpacity()
        {
            linkLabelOpacity.Visible = false;
            textBoxOpacity.Visible = true;
            textBoxOpacity.Text = linkLabelOpacity.Text;
            textBoxOpacity.Focus();
        }

        public void setCheck(bool state)
        {
            if (check.Checked != state)
            {
                isCheckboxInitial = true;
            }
            check.Checked = state;
        }

        private void persistOpacityChanged()
        {
            if (!isRunning)
            {
                return;
            }

            try
            {
                int opa = 100;
                if (!string.IsNullOrEmpty(textBoxOpacity.Text))
                {
                    opa = Int32.Parse(textBoxOpacity.Text.Trim());
                    if (opa < 0 || opa > 100)
                    {
                        MessageBox.Show("Invalid number value should in range 0 - 100");
                        textBoxOpacity.Text = "0";
                        focusTextboxOpacity();
                        return;
                    }
                }
                styleColor.Opacity = opa;
                Checked(null, parentId, styleName, styleColor, check.Checked);
                // save order;
                OpacityChanged(parentId, styleColor);
            }
            catch (Exception e)
            {
                MessageBox.Show("Invalid number value should in range 0 - 100.Exception:"+e.Message);
                focusTextboxOpacity();
            }
        }

        private void textBoxOpacity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                changeOpacityBoxToDisplayMode();
            }
        }

        private void textBoxOpacity_TextChanged(object sender, EventArgs e)
        {
            if (isRunning)
            {
                return;
            }
            isRunning = true;
            System.Threading.Tasks.Task.Delay(2000).ContinueWith(t =>
            {
                Invoke(new Action(() =>
                {
                    persistOpacityChanged();
                    isRunning = false;
                }));
            });
        }

        private void color_Click(object sender, EventArgs e)
        {
            if (1 == 1)
            {
                return;
            }
            if (this.color.Text.Contains("✔"))
            {
                this.color.Text = "";
                this.color.Padding = new Padding(10,1,10,1);
                this.isDemo = false;
            }
            else
            {
                this.color.Text = "✔";
                this.color.Padding = new Padding(1, 1, 1, 1);
                this.isDemo = true;
            }
            styleColor.IsDemo = this.isDemo;
            Checked(sender, parentId, styleName, styleColor, check.Checked);
        }
    }
}
