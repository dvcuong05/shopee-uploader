﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Main.Controls;
using Main.classes.data;
using System.Diagnostics;

namespace Main
{
    public partial class TeescapeItemPanel : UserControl
    {
        private bool isFinishedState;
        private ShopeeSetting shopeeSetting;
        public delegate void RemoveClickedHandler(TeescapeItemPanel sender);
        public event RemoveClickedHandler RemoveClicked;
        public String fileNameInDisk = "";
        public string tmpFullPath = "";
        private Chrome chrome;

        public TeescapeItemPanel(String imgPath, int counter, Chrome _chrome)
        {
            chrome = _chrome;
            InitializeComponent();
            this.resetBtn.Enabled = true;
            this.removeBtn.Enabled = true;            
            this.indexLabel.Text = counter.ToString();
            this.tmpFullPath = imgPath;
            fileNameInDisk = Path.GetFileNameWithoutExtension(imgPath);
            this.Name = fileNameInDisk;
            this.nameTxt.Text = fileNameInDisk;
            this.imagePathTxt.Text = imgPath;
            this.isFinishedState = false;
        }        

        public ShopeeItemSetting getObjWithMergedDataFromParent()
        {
            ShopeeItemSetting obj = getShopeeItemSetting();
            if (shopeeSetting == null)
            {
                obj.ShopeeSetting = ToolUtils.PARENT_SHOPEE_SETTING;
            }
            if(ToolUtils.isEmpty(obj.Price))
            {
                obj.Price = ToolUtils.PARENT_PRICE;
            }
            return obj;
        }

        public ShopeeItemSetting getShopeeItemSetting()
        {
            ShopeeItemSetting obj = new ShopeeItemSetting
            {
                TmpFullPath = this.tmpFullPath,
                ImagePath = Path.GetFileNameWithoutExtension(this.imagePathTxt.Text),
                Name = this.nameTxt.Text,
                IsFinished = this.isFinishedState,
                ShopeeSetting = this.shopeeSetting,
                Sku = textBoxSKU.Text,
                Price = textBoxPrice.Text
            };
            return obj;
        }

        public void reset(bool isEverything)
        {
            if (!isEverything)
            {
                this.isFinishedState = false;
            }
            else
            {
                this.isFinishedState = false;
                this.Enabled = true;
                this.BackColor = Color.White;
                this.shopeeSetting = null;
            }
            stop();
        }

        public void setShopeeItemSettingData(ShopeeItemSetting data)
        {
            try
            {
                this.nameTxt.Text = data.Name;
                this.textBoxSKU.Text = data.Sku;
                this.textBoxPrice.Text = data.Price;
                this.isFinishedState = data.IsFinished;
                this.shopeeSetting = data.ShopeeSetting;
                if (data.IsFinished)
                {
                    handleSuccess();
                }
            }
            catch (Exception)
            {

            }
        }

        private void handleSuccess()
        {
            this.imagePathTxt.ReadOnly = true;
            this.BackColor = Color.DarkGray;
            this.progressBarMain.Visible = false;
        }
        public void uploadSuccessful()
        {
            //this.Enabled = false;
            handleSuccess();
            this.isFinishedState = true;
            //this.mainForm.uploadSuccess();
        }

        public void updateProgressBar(int value)
        {
            this.progressBarMain.Style = ProgressBarStyle.Continuous;
            this.progressBarMain.Value = value;
        }
        public void isUploading()
        {
            this.progressBarMain.Visible = true;
        }
        public bool isFinished()
        {
            return this.isFinishedState;
        }
        public void disable()
        {
            this.Enabled = false;
        }

        public void enable()
        {
            this.Enabled = true;
        }
        public void stop()
        {
            this.Enabled = true;
            this.BackColor = Color.White;
            this.progressBarMain.Visible = false;
        }

        private void TeespringItemPanel_Load(object sender, EventArgs e)
        {
            this.progressBarMain.Style = ProgressBarStyle.Marquee;
            this.progressBarMain.Maximum = 6;
            this.progressBarMain.Value = 0;
            this.imagePathTxt.ReadOnly = true;
            this.progressBarMain.Visible = false;
        }

        public void updateIndex(int index)
        {
            this.indexLabel.Text = index.ToString();
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            RemoveClicked(this);
            //this.mainForm.removePanel(this);
        }

        private void resetBtn_Click(object sender, EventArgs e)
        {
            MessageBoxManager.Yes = "Everything";
            MessageBoxManager.No = "Finished state";
            DialogResult val = MessageBox.Show("Which reset action do you want to go ?", "Reset confirmation", MessageBoxButtons.YesNoCancel);
            if (val == DialogResult.Yes)
            {
                reset(true);
            }
            else if (val == DialogResult.No)
            {
                reset(false);
            }
            else if (val == DialogResult.Cancel)
            {
                Console.WriteLine("vncud cancel button is clicked");
                return;
            }
        }

        private void stylesBtn_Click(object sender, EventArgs e)
        {
            //List<TeescapeStyle> parentStyle = this.getParentStyle();
            //if(this.styles == null)
            {
           //     this.isSingleUloadForChild = this.getParentSingleUpload();
            }
            //TeescapeSelection selection = new TeescapeSelection(parentStyle, styles, isSingleUloadForChild, tmpFullPath);
            //selection.saveClicked += this.saveStyles;
            //selection.ShowDialog();
            ShopeeSetting data = this.shopeeSetting == null ? ToolUtils.Clone(ToolUtils.PARENT_SHOPEE_SETTING) : this.shopeeSetting;
            ChromeItem chromeItem = new ChromeItem(chrome, data, ToolUtils.COLOR_SETTING, "settingupload");
            chromeItem.formCloseHandler += saveStyles;
            chromeItem.ShowDialog();
        }

        void saveStyles(string configData)
        {
            ShopeeSetting savedData = JsonConvert.DeserializeObject<ShopeeSetting>(configData);
            if (savedData != null)
            {
                this.shopeeSetting = savedData;
            }
        }

        private bool isNotEmptyString(String str)
        {
            return (str != null && str.Trim().Length > 0);
        }

        public void renameFileInDisk(int fileIncrementNumber)
        {
            try
            {
                int foundItemInCfg = -1;
                ShopeeSetup teescapeConfigObj = null;
                String filePath = ToolUtils.FolderPath + "/setup.txt";
                if (File.Exists(filePath))
                {
                    String fileContent = File.ReadAllText(filePath);
                    teescapeConfigObj = JsonConvert.DeserializeObject<ShopeeSetup>(fileContent);
                    if (teescapeConfigObj != null && teescapeConfigObj.ShopeeItemSettings != null && teescapeConfigObj.ShopeeItemSettings.Count > 0)
                    {
                        for (int i = 0; i < teescapeConfigObj.ShopeeItemSettings.Count; i++)
                        {
                            if (teescapeConfigObj.ShopeeItemSettings[i].ImagePath == Path.GetFileNameWithoutExtension(this.imagePathTxt.Text))
                            {
                                foundItemInCfg = i;
                            }
                        }
                    }
                }

                string rgPattern = @"[\\\/:\*\?""<>|]";
                Regex objRegEx = new Regex(rgPattern);
                string newFileName = objRegEx.Replace(this.nameTxt.Text, "") + (fileIncrementNumber == 0 ? "" : "_" + fileIncrementNumber);
                string newFile = ToolUtils.FolderPath + "\\" + newFileName + Path.GetExtension(this.imagePathTxt.Text);
                if (File.Exists(newFile))
                {
                    int nextIncrement = fileIncrementNumber + 1;
                    renameFileInDisk(nextIncrement);
                }
                System.IO.File.Move(this.imagePathTxt.Text, newFile);
                this.imagePathTxt.Text = newFile;
                this.tmpFullPath = newFile;

                if (foundItemInCfg >= 0)
                {
                    ShopeeItemSetting configNew = getShopeeItemSetting();
                    configNew.ImagePath = Path.GetFileNameWithoutExtension(this.imagePathTxt.Text);
                    teescapeConfigObj.ShopeeItemSettings[foundItemInCfg] = configNew;
                    string json = JsonConvert.SerializeObject(teescapeConfigObj);
                    File.WriteAllText(filePath, json);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Cannot rename file in disk ("+ ex.Message+")", "Error", MessageBoxButtons.OK);
                Console.Write("Rename is failed:" + ex.Message);
            }
        }

        private void changeNameBtn_Click(object sender, EventArgs e)
        {
            renameFileInDisk(0);
        }

        private void linkLabelShowImg_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                String filePath = @tmpFullPath;
                Process photoViewer = new Process();
                photoViewer.StartInfo.FileName = @filePath;
                photoViewer.StartInfo.Arguments = @filePath;
                photoViewer.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot show example image with follow error is: " + ex.Message);
            }
        }
    }
}
