﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CefSharp;
using Main.localhost;

public class LocalSchemeHandlerFactory : ISchemeHandlerFactory
{
    public const string SchemeName = "local";

    public IResourceHandler Create(IBrowser browser, IFrame frame, string schemeName, IRequest request)
    {
        return new LocalProtocolSchemeHandler();
    }
}