﻿namespace Main
{
    partial class TeescapeColor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.color = new System.Windows.Forms.Label();
            this.check = new System.Windows.Forms.CheckBox();
            this.textBoxOpacity = new System.Windows.Forms.TextBox();
            this.linkLabelOpacity = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // color
            // 
            this.color.AutoSize = true;
            this.color.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.color.Location = new System.Drawing.Point(3, 24);
            this.color.Margin = new System.Windows.Forms.Padding(3);
            this.color.Name = "color";
            this.color.Padding = new System.Windows.Forms.Padding(10, 5, 10, 0);
            this.color.Size = new System.Drawing.Size(22, 20);
            this.color.TabIndex = 0;
            this.color.Click += new System.EventHandler(this.color_Click);
            // 
            // check
            // 
            this.check.AutoSize = true;
            this.check.Location = new System.Drawing.Point(7, 47);
            this.check.Margin = new System.Windows.Forms.Padding(0);
            this.check.Name = "check";
            this.check.Size = new System.Drawing.Size(15, 14);
            this.check.TabIndex = 1;
            this.check.UseVisualStyleBackColor = true;
            this.check.CheckedChanged += new System.EventHandler(this.check_CheckedChanged);
            // 
            // textBoxOpacity
            // 
            this.textBoxOpacity.Location = new System.Drawing.Point(2, 2);
            this.textBoxOpacity.Name = "textBoxOpacity";
            this.textBoxOpacity.Size = new System.Drawing.Size(25, 20);
            this.textBoxOpacity.TabIndex = 2;
            this.textBoxOpacity.Visible = false;
            this.textBoxOpacity.TextChanged += new System.EventHandler(this.textBoxOpacity_TextChanged);
            this.textBoxOpacity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxOpacity_KeyDown);
            this.textBoxOpacity.Leave += new System.EventHandler(this.textBoxOpacity_Leave);
            // 
            // linkLabelOpacity
            // 
            this.linkLabelOpacity.AutoSize = true;
            this.linkLabelOpacity.Location = new System.Drawing.Point(4, 6);
            this.linkLabelOpacity.Name = "linkLabelOpacity";
            this.linkLabelOpacity.Size = new System.Drawing.Size(19, 13);
            this.linkLabelOpacity.TabIndex = 3;
            this.linkLabelOpacity.TabStop = true;
            this.linkLabelOpacity.Text = "55";
            this.linkLabelOpacity.DoubleClick += new System.EventHandler(this.linkLabelOpacity_DoubleClick);
            // 
            // TeescapeColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.linkLabelOpacity);
            this.Controls.Add(this.textBoxOpacity);
            this.Controls.Add(this.check);
            this.Controls.Add(this.color);
            this.Name = "TeescapeColor";
            this.Size = new System.Drawing.Size(31, 62);
            this.Load += new System.EventHandler(this.TeescapeColor_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label color;
        private System.Windows.Forms.CheckBox check;
        private System.Windows.Forms.TextBox textBoxOpacity;
        private System.Windows.Forms.LinkLabel linkLabelOpacity;
    }
}
