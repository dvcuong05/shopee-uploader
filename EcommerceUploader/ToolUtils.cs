﻿using Main.classes;
using Main.classes.data;
using NCCLibs;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
namespace Main
{
    public class ToolUtils
    {
        private static string folderPath;
        public static string category;
        public static List<TeescapeStyle> stylesOfParent;
        public static List<TeescapeStyle> copiedStyles;
        public static String parentDescription;
        public static AppConfig appConfig = new AppConfig();
        // Instanciating with base URL  
        public static List<ProductFixedName> productFixedList = new List<ProductFixedName>();
        public static List<VariantStyleConfig> defaultVariantConfig = new List<VariantStyleConfig>();
        public static List<string> ACCEPT_IMAGE_TYPE = new List<string>(new string[] { ".JPG", ".JPEG", ".PNG", ".BMP" });

        public static string BASE_FOLDER = AppDomain.CurrentDomain.BaseDirectory;
        public static string LAST_CONFIG_PATH = BASE_FOLDER + @"\assets\configs\lastconfig.json";
        public static string STYLE_CONFIG_FILE = ToolUtils.BASE_FOLDER + @"\assets\configs\styles.txt";
        public static string PARENT_CONFIG_FILE = ToolUtils.BASE_FOLDER + @"\assets\configs\parentsetting.txt";
        public static string COLOR_CONFIG_FILE = ToolUtils.BASE_FOLDER + @"\assets\configs\colorssetting.txt";
        public static string AUTHENTICATION_FILE = ToolUtils.BASE_FOLDER + "authenticate.txt";

        public static string SHOPEE_LOGIN = @"https://banhang.shopee.vn/account/signin";
        public static string TIKI_LOGIN = @"https://banhang.shopee.vn/account/signin";
        public static string LAZADA_LOGIN = @"https://banhang.shopee.vn/account/signin";

        public static bool DEBUG_MODE = false;
        public static int CURRENT_TAG_INDEX = -1;
        public static int CURRENT_BG_INDEX = -1;
        public static Authentication currentAuthentication;
        public static ShopeeSetting PARENT_SHOPEE_SETTING = new ShopeeSetting();
        public static string PARENT_PRICE;
        public static ColorsSetting COLOR_SETTING = new ColorsSetting();
        public static List<NCCLibs.ImageItem> BRAND_TAG;
        public static List<NCCLibs.ImageItem> MOCKUP_IMAGES = new List<ImageItem>();

        public static void loadMockupImages()
        {
            string mockupJsonPath = ToolUtils.getMockupConfigPath("tayngan") + @"\mockups.json";
            ToolUtils.MOCKUP_IMAGES = ToolUtils.ReadFile<List<ImageItem>>(mockupJsonPath);
        }

        public static void loadTagImages()
        {
            if (!string.IsNullOrEmpty(ToolUtils.COLOR_SETTING.TagFolder))
            {
                BRAND_TAG = DirectoryUtil.GetImages(ToolUtils.COLOR_SETTING.TagFolder, ToolUtils.ACCEPT_IMAGE_TYPE, false);
            }
        }

        public static string[] getFileStartWith(string filePath)
        {
            string[] files = Directory.GetFiles(Path.GetDirectoryName(filePath), Path.GetFileNameWithoutExtension(filePath) +"#*");
            return files;
        }

        public static ColorsSetting getColorSetting()
        {
            try
            {
                ColorsSetting datas = ToolUtils.ReadFile<ColorsSetting>(COLOR_CONFIG_FILE);
                if (datas != null)
                {
                    return datas;
                }
            }
            catch (Exception e)
            {
                return new ColorsSetting();
            }
            return new ColorsSetting();
        }

        public static void saveColorSetting(ColorsSetting data)
        {
            try
            {
                ToolUtils.WriteFile<ColorsSetting>(COLOR_CONFIG_FILE, data);
            }
            catch (Exception e)
            {
                MessageBox.Show("Lưu thất bại." + e.Message);
            }
        }

        public static ShopeeSetting getParentShopeeSetting()
        {
            try
            {
                ShopeeSetting datas = ToolUtils.ReadFile<ShopeeSetting>(PARENT_CONFIG_FILE);
                if (datas != null)
                {
                    return datas;
                }
            }
            catch (Exception e)
            {
                return new ShopeeSetting();
            }
            return new ShopeeSetting();
        }

        public static void saveParentShopeeSetting(ShopeeSetting data)
        {
            try
            {
                ToolUtils.WriteFile<ShopeeSetting>(PARENT_CONFIG_FILE, data);
            }
            catch (Exception e)
            {
                MessageBox.Show("Lưu thất bại." + e.Message);
            }
        }

        public static List<Authentication> getAuthentication()
        {
            try
            {
                List<Authentication> datas = ToolUtils.ReadFile<List<Authentication>>(AUTHENTICATION_FILE);
                if (datas != null)
                {
                    return datas;
                }
            }
            catch(Exception e)
            {
                return new List<Authentication>();
            }
            return new List<Authentication>();
        }

        public static void saveAuthentication(Authentication data)
        {
            List<Authentication> datas = getAuthentication();
            Authentication found = null;
            foreach(Authentication item in datas)
            {
                if(item.ShopeeUser == data.ShopeeUser)
                {
                    found = item;
                    break;
                }
            }
            if (found != null)
            {
                found.ShopeePass = Encrypt(data.ShopeePass);
            }
            else
            {
                data.ShopeePass = Encrypt(data.ShopeePass);
                datas.Add(data);
            }
            ToolUtils.WriteFile<List<Authentication>> (AUTHENTICATION_FILE, datas);
        }

        public static int getOpacity(int defaultOpacity, String colorName, NCCLibs.ImageItem mockup)
        {
            int res = defaultOpacity != 0 ? defaultOpacity : 50;
            if (mockup.Colors != null && mockup.Colors.Count > 0)
            {
                classes.ColorOpacity color = mockup.Colors.Find(x => x.Name == colorName);
                res = color != null ? color.Opacity : res;
            }
            return res;
        }

        public static List<ColorOpacity> convertStyleColorToColorOpacity(List<StyleColor> data)
        {
            List<ColorOpacity> result = new List<ColorOpacity>();
            if(data != null)
            {
                foreach(var item in data)
                {
                    ColorOpacity colorOpacity = new ColorOpacity();
                    colorOpacity.ColorCode = item.ColorCode;
                    colorOpacity.Name = item.Name;
                    colorOpacity.Opacity = item.Opacity;
                    colorOpacity.Value = item.Value;
                    result.Add(colorOpacity);
                }
            }
            return result;
        }

        public static List<StyleColor> convertStyleColorToStyleColor(List<ColorOpacity> data)
        {
            List<StyleColor> result = new List<StyleColor>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    StyleColor colorOpacity = new StyleColor();
                    colorOpacity.ColorCode = item.ColorCode;
                    colorOpacity.Name = item.Name;
                    colorOpacity.Opacity = item.Opacity;
                    colorOpacity.Value = item.Value;
                    result.Add(colorOpacity);
                }
            }
            return result;
        }

        
        public static T Clone<T>(T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        public static bool isEmpty(string data)
        {
            return (data == null || data.Trim() == "");
        }

        public static int calculateTotalVariants(List<TeescapeStyle> styles)
        {
            int totalVariants = 0;
            if (styles != null && styles.Count > 0)
            {
                for (int i = 0; i < styles.Count; i++)
                {
                    if (styles[i].Colors != null && styles[i].Sizes != null)
                    {
                        totalVariants = totalVariants + (styles[i].Colors.Count * styles[i].Sizes.Count);
                    }
                }
            }
            return totalVariants;
        }

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "ncc";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "ncc";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static string creatHandler(string title)
        {
            title = title.ToLower();
            char[] arr = title.ToCharArray();

            arr = Array.FindAll<char>(arr, (c => (char.IsLetterOrDigit(c)
                                              || char.IsWhiteSpace(c)
                                              || c == '_'
                                              || c == '-')));

            string res = new string(arr);
            string[] doubleCheckArr = res.Split(' ');
            string final = "";
            foreach (string i in doubleCheckArr)
            {
                if (i != "-" && i != "")
                {
                    if (final != "")
                    {
                        final += "-";
                    }
                    final += i;
                }
            }
            return final;
        }

        public static string getFixedName(string originalName, string time)
        {
            int maxLength = 70 - time.Length;
            string fixedName = "";
            string[] arr = originalName.Split(' ');
            for (int i = 0; i < arr.Count(); i++)
            {
                string tmp = arr[i] + " ";
                if ((fixedName.Length + tmp.Length) <= maxLength)
                {
                    fixedName += tmp;
                }
            }
            productFixedList.Add(new ProductFixedName(fixedName, fixedName + time));
            return fixedName + time;
        }

        public static string getFixedNameForSingle(string originalName)
        {
            int maxLength = 70;
            string fixedName = "";
            string[] arr = originalName.Split(' ');
            for (int i = 0; i < arr.Count(); i++)
            {
                string tmp = arr[i] + " ";
                if ((fixedName.Length + tmp.Length) <= maxLength)
                {
                    fixedName += tmp;
                }
            }
            return fixedName;
        }

        public static void saveProductIds(List<string> productIds)
        {
            try
            {

                String filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/assets/configs/productids.txt";
                String productIdsString = JsonConvert.SerializeObject(productIds);
                File.WriteAllText(filePath, productIdsString);
            }
            catch (Exception ex)
            {
                Console.WriteLine("VNCUD saveProductIds has error:" + ex.Message);
            }
        }

        public static List<string> readProductIds()
        {
            try
            {

                String filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/assets/configs/productids.txt";
                String fileContent = File.ReadAllText(filePath);
                return JsonConvert.DeserializeObject<List<string>>(fileContent);
            }
            catch (Exception ex)
            {
                Console.WriteLine("VNCUD saveProductIds has error:" + ex.Message);
                return new List<string>();
            }
        }

        private static string resizeImageGreaterThan4MB(string filePath, double expectSize)
        {
            try
            {
                string newfilePath = filePath;
                FileInfo fileInfo = new FileInfo(filePath);
                fileInfo.Refresh();
                double fileSize = 1.0 * fileInfo.Length / 1024 / 1024;
                if (fileSize > expectSize)
                {
                    FileStream myFileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    Image origImg = Image.FromStream(myFileStream, true, false);
                    int newOrigWidth = 2500;
                    int newOrigHeight = 3000;
                    Bitmap dstImg = new Bitmap(newOrigWidth, newOrigHeight);
                    dstImg.SetResolution(72, 72);
                    Graphics g = Graphics.FromImage(dstImg);
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.CompositingQuality = CompositingQuality.HighQuality;

                    // Resize the original
                    g.DrawImage(origImg, 0, 0, newOrigWidth, newOrigHeight);
                    g.Dispose();

                    newfilePath = Path.GetDirectoryName(newfilePath) + "\\" + Path.GetFileNameWithoutExtension(newfilePath) + "_RESIZED" + Path.GetExtension(newfilePath);

                    origImg.Dispose();
                    dstImg.Save(newfilePath);
                    dstImg.Dispose();
                    myFileStream.Close();
                }
                return newfilePath;
            }
            catch (Exception ex)
            {
                MessageBox.Show("resizeImageGreaterThan4MB has an error:" + ex.Message);
                return null;
            }
        }

        public static FileObject ConvertImageToBase64(string _filePath)
        {
            FileObject fileObject = null;
            //Check file size > 3MB
            FileInfo fileInfo = new FileInfo(_filePath);
            double fileSize = 1.0 * fileInfo.Length / 1024 / 1024;
            bool hasResize = false;
            double expectSize = 2.0;
            if (fileSize > expectSize)
            {
                Console.WriteLine("file bigger 2MB. Rezising...");
                _filePath = resizeImageGreaterThan4MB(_filePath, expectSize);
                hasResize = true;
            }
            FileStream myFileStream = new FileStream(_filePath, FileMode.Open, FileAccess.Read);
            using (Image image = Image.FromStream(myFileStream, true, false))
            {
                // Convert byte[] to Base64 String
                byte[] imageBytes = File.ReadAllBytes(_filePath);
                string base64String = Convert.ToBase64String(imageBytes);

                fileObject = new FileObject();
                fileObject.FileName = Path.GetFileName(_filePath);
                fileObject.Base64Content = base64String;
                string fileExtension = Path.GetExtension(_filePath);

                if (ImageFormat.Jpeg.Equals(image.RawFormat))
                {
                    fileObject.FileType = "image/jpeg";
                }
                else if (ImageFormat.Png.Equals(image.RawFormat))
                {
                    fileObject.FileType = "image/png";
                }
                image.Dispose();
                myFileStream.Close();
                if (hasResize)
                {
                    File.Delete(_filePath);
                }
                return fileObject;
            }
        }

        public static FileObject getMockupInBase64(Bitmap image, string fileName)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Jpeg);
            byte[] bmpBytes = ms.ToArray();
            string base64String = Convert.ToBase64String(bmpBytes);
            FileObject fileObject = new FileObject
            {
                FileName = fileName,
                Base64Content = base64String,
                FileType = "image/jpeg"
            };
            return fileObject;
        }

        public static ShopeeItemSetting CustomeActiveItem(ShopeeItemSetting activeItem, ShopeeSetting parentShopeeSetting, String[] replaceKeywords, string mainKey)
        {
            activeItem.Name = activeItem.Name.Trim();
            ShopeeSetting renderedStyles;
            if (activeItem.ShopeeSetting == null)
            {
                renderedStyles = Clone(parentShopeeSetting);
            }
            else
            {
                renderedStyles = Clone(activeItem.ShopeeSetting);
            }
            renderedStyles.Description = System.Web.HttpUtility.HtmlEncode(renderedStyles.Description).Replace("\n", "<br/>");
            activeItem.ShopeeSetting = renderedStyles;
            return activeItem;
        }

        //public static string GetAMZStyleCategory(string path)
        //{
        //    String fileContent = File.ReadAllText(path + "/config.txt");
        //    AmazonStyleConfig config = JsonConvert.DeserializeObject<AmazonStyleConfig>(fileContent);
        //    return config.Category;
        //}

        private static void AddToStringList(List<String> resultList, string str)
        {
            if (!String.IsNullOrEmpty(str))
            {
                resultList.Add(System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(str.ToLower()));
            }
        }

        public static void addImageKeyword(string imageFlePath, string comments)
        {
            bool isPNG = false;
            if (Path.GetExtension(imageFlePath).ToLower().EndsWith("png"))
            {
                isPNG = true;
            }
            BitmapDecoder decoder = null;
            BitmapFrame bitmapFrame = null;
            BitmapMetadata metadata = null;
            FileInfo originalImage = new FileInfo(imageFlePath);

            if (File.Exists(imageFlePath))
            {
                using (Stream jpegStreamIn = File.Open(imageFlePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    if (isPNG)
                    {
                        decoder = new PngBitmapDecoder(jpegStreamIn, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                    }
                    else
                    {
                        decoder = new JpegBitmapDecoder(jpegStreamIn, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                    }
                }

                bitmapFrame = decoder.Frames[0];
                metadata = (BitmapMetadata)bitmapFrame.Metadata;

                if (bitmapFrame != null)
                {
                    BitmapMetadata metaData = (BitmapMetadata)bitmapFrame.Metadata.Clone();

                    if (metaData != null)
                    {
                        // modify the metadata   
                        if (isPNG)
                        {
                            metaData.SetQuery("/Text/Mainkey", comments);
                        }
                        else
                        {
                            metaData.Comment = comments;
                        }

                        if (isPNG)
                        {
                            // get an encoder to create a new jpg file with the new metadata.      
                            var encoder = new PngBitmapEncoder();
                            encoder.Frames.Add(BitmapFrame.Create(bitmapFrame, bitmapFrame.Thumbnail, metaData, bitmapFrame.ColorContexts));

                            // Delete the original
                            originalImage.Delete();

                            // Save the new image 
                            using (Stream jpegStreamOut = File.Open(imageFlePath, FileMode.CreateNew, FileAccess.ReadWrite))
                            {
                                encoder.Save(jpegStreamOut);
                            }
                        }
                        else
                        {
                            // get an encoder to create a new jpg file with the new metadata.      
                            var encoder = new JpegBitmapEncoder();
                            encoder.Frames.Add(BitmapFrame.Create(bitmapFrame, bitmapFrame.Thumbnail, metaData, bitmapFrame.ColorContexts));

                            // Delete the original
                            originalImage.Delete();

                            // Save the new image 
                            using (Stream jpegStreamOut = File.Open(imageFlePath, FileMode.CreateNew, FileAccess.ReadWrite))
                            {
                                encoder.Save(jpegStreamOut);
                            }
                        }
                    }
                }
            }
        }

        public static string getImageKeyword(string imageFlePath)
        {
            bool isPNG = false;
            if (Path.GetExtension(imageFlePath).ToLower().EndsWith("png"))
            {
                isPNG = true;
            }
            BitmapDecoder decoder = null;
            BitmapFrame bitmapFrame = null;
            BitmapMetadata metadata = null;
            FileInfo originalImage = new FileInfo(imageFlePath);

            if (File.Exists(imageFlePath))
            {
                // load the jpg file with a JpegBitmapDecoder    
                using (Stream jpegStreamIn = File.Open(imageFlePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    if (isPNG)
                    {
                        decoder = new PngBitmapDecoder(jpegStreamIn, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                    }
                    else
                    {
                        decoder = new JpegBitmapDecoder(jpegStreamIn, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                    }
                }

                bitmapFrame = decoder.Frames[0];
                metadata = (BitmapMetadata)bitmapFrame.Metadata;

                if (bitmapFrame != null)
                {
                    BitmapMetadata metaData = (BitmapMetadata)bitmapFrame.Metadata.Clone();

                    if (metaData != null)
                    {
                        if (isPNG)
                        {
                            object value = metaData.GetQuery("/Text/Mainkey");
                            return value != null ? value.ToString() : null;
                        }
                        else
                        {
                            return metaData.Comment;
                        }

                    }
                }
            }
            return null;

        }

        public static string EncodeJSString(string data)
        {
            return System.Web.HttpUtility.JavaScriptStringEncode(data);
        }

        public static int GetNextTagFile(int count)
        {
            ToolUtils.CURRENT_TAG_INDEX = ToolUtils.CURRENT_TAG_INDEX + 1;
            if (ToolUtils.CURRENT_TAG_INDEX >= count)
            {
                ToolUtils.CURRENT_TAG_INDEX = 0;
            }
            return ToolUtils.CURRENT_TAG_INDEX;
        }

        public static int GetNextBgFile(int count)
        {
            ToolUtils.CURRENT_BG_INDEX = ToolUtils.CURRENT_BG_INDEX + 1;
            if (ToolUtils.CURRENT_BG_INDEX >= count)
            {
                ToolUtils.CURRENT_BG_INDEX = 0;
            }
            return ToolUtils.CURRENT_BG_INDEX;
        }

        public static string getMockupConfigPath(string variantStyleId)
        {
            string currentVariantFolder = @"\assets\configs\mockups\";
            return ToolUtils.BASE_FOLDER + currentVariantFolder + variantStyleId;
        }

        public static void saveVariantStyleSetup(string json)
        {
            File.WriteAllText(STYLE_CONFIG_FILE, json);
        }

        public static List<TeescapeStyle> readOriginalVariantStyles()
        {
            try
            {
                String fileContent = File.ReadAllText(STYLE_CONFIG_FILE);
                return JsonConvert.DeserializeObject<List<TeescapeStyle>>(fileContent);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not read Teescape Size/Color config from styles.txt. Exception:" + ex.Message);
                return new List<TeescapeStyle>();
            }

        }

        public static List<TeescapeStyle> mergeToCurrentStyle(string updatedStyles)
        {
            List<TeescapeStyle> updatedStyleList = JsonConvert.DeserializeObject<List<TeescapeStyle>>(updatedStyles);
            List<TeescapeStyle> currentStyles = readOriginalVariantStyles();
            for (var i = updatedStyleList.Count - 1; i >= 0; i--)
            {
                for (var j = currentStyles.Count - 1; j >= 0; j--)
                {
                    if (currentStyles[j].Id == updatedStyleList[i].Id)
                    {
                        foreach (StyleColor color in updatedStyleList[i].Colors)
                        {
                            foreach (StyleColor oldColor in currentStyles[j].Colors)
                            {
                                if (color.ColorCode == oldColor.ColorCode)
                                {
                                    color.Opacity = oldColor.Opacity;
                                }
                            }
                        }
                        currentStyles.RemoveAt(j);
                    }
                }
            }

            return updatedStyleList;
        }

        public static T ReadFile<T>(string path)
        {
            try
            {
                String fileContent = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<T>(fileContent);
            }
            catch
            {
                return default(T);
            }
        }

        public static void WriteFile<T>(string path, T data)
        {
            try
            {
                string content = JsonConvert.SerializeObject(data);
                File.WriteAllText(path, content);
            }
            catch(Exception e)
            {
                MessageBox.Show("Lưu file thất bại." + e.Message);
            }

        }

        public static string FolderPath { get => folderPath; set => folderPath = value; }

    }
}
