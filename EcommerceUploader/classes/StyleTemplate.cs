﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main.classes
{
    class StyleTemplate
    {
        long id;
        String name;
        List<TeescapeStyle> teescapeStyle;

        public StyleTemplate(long _id, String _name, List<TeescapeStyle> style)
        {
            this.Id = _id;
            this.name = _name;
            this.teescapeStyle = style;
        }

        public long Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public List<TeescapeStyle> TeescapeStyle
        {
            get
            {
                return teescapeStyle;
            }

            set
            {
                teescapeStyle = value;
            }
        }
    }
}
