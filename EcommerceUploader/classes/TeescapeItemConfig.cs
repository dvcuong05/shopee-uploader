﻿using Main.classes.data;
using System;
using System.Collections.Generic;

namespace Main
{
    public class ShopeeItemSetting
    {
        private string tmpFullPath;
        private String imagePath;
        private String name;
        private string sku;
        private string price;
        private bool isFinished;
        private bool isOnBack;
        private ShopeeSetting shopeeSetting;

        public ShopeeItemSetting(String _imagePath, String shirtName, String _sku,string price, ShopeeSetting _shopeeSetting, bool _isOnBack)
        {
            this.ImagePath = _imagePath;
            this.Name = shirtName;
            this.sku = _sku;
            this.price = price;
            this.shopeeSetting = _shopeeSetting;
            this.IsOnBack = _isOnBack;
        }

        public ShopeeItemSetting()
        {
            // TODO: Complete member initialization
        }

        public ShopeeSetting ShopeeSetting { get => shopeeSetting; set => shopeeSetting = value; }
        public string TmpFullPath { get => tmpFullPath; set => tmpFullPath = value; }
        public string ImagePath { get => imagePath; set => imagePath = value; }
        public string Name { get => name; set => name = value; }
        public bool IsFinished { get => isFinished; set => isFinished = value; }
        public bool IsOnBack { get => isOnBack; set => isOnBack = value; }
        public string Sku { get => sku; set => sku = value; }
        public string Price { get => price; set => price = value; }
    }
}
