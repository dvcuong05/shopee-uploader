﻿
using CefSharp.WinForms;
using Newtonsoft.Json;

namespace Main.classes.chrome
{
    class JavaScriptInteractionObj
    {
        public ChromiumWebBrowser chromeBrowser { get; set; }
        public string settingData { get; set; }
        public string settingColorsData { get; set; }

        public JavaScriptInteractionObj()
        {
        }

        public JavaScriptInteractionObj(ChromiumWebBrowser chromium, string _settingData)
        {
            chromeBrowser = chromium;
            settingData = _settingData;
        }

        public string getDataFromC()
        {
            return settingData;
        }
        public string getColorsDataFromC()
        {
            return JsonConvert.SerializeObject(ToolUtils.COLOR_SETTING);
        }
        public void setDataToC(string jsData)
        {
            settingData = jsData;
        }
    }
}
