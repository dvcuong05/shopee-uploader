﻿using System.Collections.Generic;

namespace Main.classes.data
{
    public class ShopeeSetting
    {
        string category = "Thời Trang Nam > Áo thun > Áo ngắn tay không cổ";
        string description;
        string brand ="No Brand";
        string material = "Cotton";
        DemoProduct demoProductInfo = new DemoProduct();
        string quantity = "100";
        string weight ="230";
        string shipRong ="16";
        string shipDai ="23";
        string shipCao ="4";
        string preorder  = "0";
        string saveAction  = "1";
        string sizePicture;
        System.Collections.Generic.List<string> sizes;
        System.Collections.Generic.List<StyleColor> colors;
        bool isGenMockup = false;

        public ShopeeSetting() {
        }

        public string Category { get => category; set => category = value; }
        public string Description { get => description; set => description = value; }
        public string Brand { get => brand; set => brand = value; }
        public string Material { get => material; set => material = value; }
        public string Quantity { get => quantity; set => quantity = value; }
        public string Weight { get => weight; set => weight = value; }
        public string ShipRong { get => shipRong; set => shipRong = value; }
        public string ShipDai { get => shipDai; set => shipDai = value; }
        public string ShipCao { get => shipCao; set => shipCao = value; }
        public string Preorder { get => preorder; set => preorder = value; }
        public string SaveAction { get => saveAction; set => saveAction = value; }
        public string SizePicture { get => sizePicture; set => sizePicture = value; }
        public List<string> Sizes { get => sizes; set => sizes = value; }
        public List<StyleColor> Colors { get => colors; set => colors = value; }
        public bool IsGenMockup { get => isGenMockup; set => isGenMockup = value; }
        public DemoProduct DemoProductInfo { get => demoProductInfo; set => demoProductInfo = value; }

        public class DemoProduct
        {
            string priceDemo;
            string sizeDemo;
            string colorDemo;
            public DemoProduct() { }

            public string PriceDemo { get => priceDemo; set => priceDemo = value; }
            public string SizeDemo { get => sizeDemo; set => sizeDemo = value; }
            public string ColorDemo { get => colorDemo; set => colorDemo = value; }
        }
    }
}
