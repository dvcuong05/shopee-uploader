﻿
using System.Collections.Generic;

namespace Main.classes.data
{
    public class ColorsSetting
    {
        List<StyleColor> colors = new List<StyleColor>();
        string tagFolder;

        public List<StyleColor> Colors { get => colors; set => colors = value; }
        public string TagFolder { get => tagFolder; set => tagFolder = value; }
    }
}
