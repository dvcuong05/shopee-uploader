﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main.classes
{
    public class ProductFixedName
    {
        private string preName;
        private string fixedName;
        public ProductFixedName(string preName, string fixedName)
        {
            this.preName = preName;
            this.fixedName = fixedName;
        }
        public string PreName { get => preName; set => preName = value; }
        public string FixedName { get => fixedName; set => fixedName = value; }
    }
}
