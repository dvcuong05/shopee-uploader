﻿using Main.classes.data;
using System;
using System.Collections.Generic;
namespace Main
{
    public class ShopeeSetup
    {
        private String folderPath;
        private string parentPrice;
        private String description;
        private Boolean isOnBack;
        private List<ShopeeItemSetting> shopeeItemSettings;

        public string FolderPath { get => folderPath; set => folderPath = value; }
        public string Description { get => description; set => description = value; }
        public bool IsOnBack { get => isOnBack; set => isOnBack = value; }
        public List<ShopeeItemSetting> ShopeeItemSettings { get => shopeeItemSettings; set => shopeeItemSettings = value; }
        public string ParentPrice { get => parentPrice; set => parentPrice = value; }
    }
}
