﻿
namespace Main.classes
{
    class ShopConfig
    {
        private string shopName;
        private string userName;
        private string storeName;

        public string ShopName
        {
            get
            {
                return shopName;
            }

            set
            {
                shopName = value;
            }
        }

        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        public string StoreName
        {
            get
            {
                return storeName;
            }

            set
            {
                storeName = value;
            }
        }
    }
}
