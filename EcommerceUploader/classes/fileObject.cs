﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main.classes
{
    public class FileObject
    {
        string base64Content;
        string fileType;
        string fileName;
        string colorName;

        public string FileName {
            get { return this.fileName; }
            set { this.fileName = value; }
        }
        public string FileType {
            get { return this.fileType; }
            set { this.fileType = value; }
        }
        public string Base64Content
        {
            get { return this.base64Content; }
            set { this.base64Content = value; }
        }

        public string ColorName { get => colorName; set => colorName = value; }
    }
}
