﻿
namespace Main.classes
{
    public class StyleSize
    {
        string value;
        bool isDemo;
        string label;

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public bool IsDemo { get => isDemo; set => isDemo = value; }
    }
}
