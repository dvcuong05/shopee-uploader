﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main.classes
{
    public class VariantStyleConfig
    {
        private string styleId;
        private string category;
        private string suffix;
        private string variantImagePath;
        private string demoPrice;

        public VariantStyleConfig(string styleId, string category, string prefix, string variantImagePath, string demoPrice)
        {
            this.styleId = styleId;
            this.category = category;
            this.suffix = prefix;
            this.variantImagePath = variantImagePath;
            this.demoPrice = demoPrice;
        }

        public string VariantImagePath { get => variantImagePath; set => variantImagePath = value; }
        public string Suffix { get => suffix; set => suffix = value; }
        public string Category { get => category; set => category = value; }
        public string StyleId { get => styleId; set => styleId = value; }
        public string DemoPrice { get => demoPrice; set => demoPrice = value; }
    }
}
