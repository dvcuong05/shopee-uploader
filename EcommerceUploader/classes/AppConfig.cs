﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main.classes
{
    public class AppConfig
    {
        private string excelKeywordPath;
        private string shopifyStoreName;
        private string brandName;
        private string brandPictureFolderPath;
        private string upcCodeFilePath;
        private string productTime;
        private string email;
        private string password;
        private List<VariantStyleConfig> variantStyleConfigs = new List<VariantStyleConfig>();
        private string tagPath;
        private int exportWidth = 1000;
        private int exportHeight = 0;
        private Boolean skipSaleChanel = false;
        private Boolean amzSaleChanel = true;
        private Boolean googleAdsSaleChanel = false;

        public string BrandName { get => brandName; set => brandName = value; }
        public string BrandPictureFolderPath { get => brandPictureFolderPath; set => brandPictureFolderPath = value; }
        public string UpcCodeFilePath { get => upcCodeFilePath; set => upcCodeFilePath = value; }
        public string ExcelKeywordPath { get => excelKeywordPath; set => excelKeywordPath = value; }
        public string ShopifyStoreName { get => shopifyStoreName; set => shopifyStoreName = value; }
        public List<VariantStyleConfig> VariantStyleConfigs { get => variantStyleConfigs; set => variantStyleConfigs = value; }
        public string ProductTime { get => productTime; set => productTime = value; }
        public string Email { get => email; set => email = value; }
        public string Password { get => password; set => password = value; }
        public string TagPath { get => tagPath; set => tagPath = value; }
        public int ExportHeight { get => exportHeight; set => exportHeight = value; }
        public int ExportWidth { get => exportWidth; set => exportWidth = value; }
        public bool SkipSaleChanel { get => skipSaleChanel; set => skipSaleChanel = value; }
        public bool AmzSaleChanel { get => amzSaleChanel; set => amzSaleChanel = value; }
        public bool GoogleAdsSaleChanel { get => googleAdsSaleChanel; set => googleAdsSaleChanel = value; }
    }
}
