﻿using CefSharp;
using Main.classes.data;
using Main.Controls;
using NCCLibs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main.classes
{
    class ShirtUploader
    {
        private bool stopApp = false;
        private Chrome chrome;
        private string baseUrl = "";
        private Timer timer;
        private ShopeeSetting parentStyles;
        private ShopeeItemSetting activeItem;
        private int index;
        private bool checkingLoginSessionForm = false;

        public delegate void FinishUpload (int index);
        public event FinishUpload FinishUploadAStyle;

        public delegate void Logger(string data);
        public event Logger WriteLog;

        private string step = "";

        public ShirtUploader(string baseUrl, Chrome chrome, Timer timer)
        {
            this.baseUrl = baseUrl;
            this.chrome = chrome;
            this.timer = timer;
            this.timer.Tick += Timer_Tick;
            this.timer.Interval = 20 * 60 * 1000;
            // this.delay.Text = 20 + "";
            this.timer.Stop();
            this.stopApp = false;
        }

        public void StopApp()
        {
            this.stopApp = true;
            activeItem = null;
            timer.Stop();
            timer.Tick -= Timer_Tick;
            checkingLoginSessionForm = false;
        }

        private async void Timer_Tick(object sender, EventArgs e)
        {
            this.timer.Stop();
            WriteLog("Reached time out of step...");
            await Task.Delay(10000);
            WriteLog("Start re-upload for: " + this.activeItem.Name);
            Upload(activeItem, index, parentStyles);
        }

        // upload step
        public void Upload(ShopeeItemSetting activeItem, int index, ShopeeSetting parentStyles)
        {
            this.index = index;
            this.activeItem = activeItem;
            this.parentStyles = parentStyles;
            stopApp = false;
            this.step = "begin";
            Begin(false);
        }

        private void Begin(bool isUpdatingStyle)
        {
            if(this.step != "begin")
            {
                return;
            }
            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                if (!args.IsLoading)
                {
                    chrome.browser.LoadingStateChanged -= handler;
                    if (!stopApp)
                    {
                        timer.Stop();
                        timer.Start();
                        this.step = "setProductNameAndCategory";
                        setProductNameAndCategory(isUpdatingStyle);
                        WriteLog(index + " ---> Step 1");
                    }
                }
            };

            chrome.browser.LoadingStateChanged += handler;
            string url = "https://banhang.shopee.vn/portal/product/category";
            WriteLog("Begin at " + index + ": " + url);
            chrome.browser.Load(url);
        }

        // Upload file
        private async void setProductNameAndCategory(bool isUpdatingStyle)
        {
            try
            {
                if (this.step != "setProductNameAndCategory")
                {
                    return;
                }
                await Task.Delay(5000);
                WriteLog("Step1 - setProductNameAndCategory");
                var domTask = chrome.browser.EvaluateScriptAsync("(function(){var dk1=document.querySelectorAll('.product-name-edit .shopee-input__input').length;var dk2=document.querySelectorAll('.category-item').length;return(dk1+dk2>=2)?1:0;})();");
                await domTask.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        var response = t.Result;

                        if (response.Success && response.Result != null)
                        {
                            bool isDomExisted = Convert.ToInt32(response.Result.ToString()) != 0;
                            if (isDomExisted)
                            {
                                chrome.browser.Invoke((MethodInvoker)async delegate
                                {
                                    string prodName = this.activeItem.Name;
                                    string fullCateGory = this.activeItem.ShopeeSetting.Category;
                                    await chrome.browser.EvaluateScriptAsync("(function(prodName,fullCateGory){debugger;var productName=document.querySelectorAll('.product-name-edit .shopee-input__input');productName[0].value=prodName;let event=new Event('input',{bubbles:true});event.simulated=true;productName[0].dispatchEvent(event);if(!fullCateGory){alert('Bạn chưa thiết lập category cho áo');};var NCselectACategory=function(selectedText){var categories=document.getElementsByClassName('category-item');for(var idx in categories){if(categories[idx].childNodes&&categories[idx].childNodes[0]&&categories[idx].childNodes[0].innerText==selectedText){categories[idx].click();}};setTimeout(()=>{var nextButton=document.querySelectorAll('.shopee-button');nextButton[0].click();},1500);};function clone(obj){if(null==obj||'object'!=typeof obj);return obj;var copy=obj.constructor();for(var attr in obj){if(obj.hasOwnProperty(attr));copy[attr]=obj[attr];};return copy;};var listCateNames=fullCateGory.split('>');for(var index=0;index<listCateNames.length;index++){var selectedText=listCateNames[index].trim();setTimeout(NCselectACategory,(index+1)*2000,clone(selectedText));}})('" + prodName + "','" + fullCateGory + "')");
                                    this.step = "setProductDescriptionStep";
                                    setProductDescriptionStep();
                                });
                            }
                            else
                            {
                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    setProductNameAndCategory(isUpdatingStyle);
                                });

                            }
                        }
                        else
                        {
                            chrome.browser.Invoke((MethodInvoker)delegate
                            {
                                setProductNameAndCategory(isUpdatingStyle);
                            });
                            WriteLog(response.Message);
                        }
                    }
                });
            }
            catch (Exception ee)
            {
                WriteLog("checkingUploadButton has an exception: " + ee.Message);
            }

        }
        private async void setProductDescriptionStep()
        {
            try
            {
                if (this.step != "setProductDescriptionStep")
                {
                    return;
                }
                await Task.Delay(3000);
                WriteLog("Step2 - setProductDescriptionStep");
                var domTask = chrome.browser.EvaluateScriptAsync("(function() {return document.querySelectorAll('.textarea,.description').length;})();");
                await domTask.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        var response = t.Result;
                        if (response.Success && response.Result != null)
                        {
                            bool isDomExisted = Convert.ToInt32(response.Result.ToString()) != 0;
                            if (isDomExisted)
                            {
                                this.step = "setProductDesc";
                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    WriteLog("---> Set product description.");
                                    setProductDesc();
                                });
                            }
                            else
                            {
                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    setProductDescriptionStep();
                                });
                            }
                        }
                        else
                        {
                            WriteLog(response.Message);
                        }
                    }
                });
            }
            catch (Exception ee)
            {
                Console.WriteLine("waitingForUploaded has an exception: " + ee.Message);
            }
        }
        private async void setProductDesc()
        {
            try
            {
                if (this.step != "setProductDesc")
                {
                    return;
                }
                await Task.Delay(3000);
                WriteLog("Step2 - setProductDescriptionStep - run");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                var domTask = chrome.browser.EvaluateScriptAsync("(function(ShopeeSetting){var desc=ShopeeSetting.Description;var brandName=ShopeeSetting.Brand;var material=ShopeeSetting.Material;var descNodeParent=document.querySelectorAll('.textarea,.description');descNodeParent[0].querySelectorAll('.textarea__edit')[0].click();function clone(obj){if(null==obj||'object'!=typeof obj);return obj;var copy=obj.constructor();for(var attr in obj){if(obj.hasOwnProperty(attr));copy[attr]=obj[attr];};return copy;};var setMeterial=function(meterial){var event=new Event('input',{bubbles:true});event.simulated=true;var merterialDom=document.querySelectorAll('[placeholder=\"Tìm kiếm Chất liệu\"]');if(merterialDom.length<=0){alert('Không tìm thấy chất liệu, bạn đã setup chất liệu chưa?');};merterialDom[0].value=meterial;merterialDom[0].dispatchEvent(event);setTimeout(()=>{var brandmerterialValue=document.querySelectorAll('.dropdown-input__item, .shopee-dropdown-item');brandmerterialValue[0].click();},1000);};var setBrand=function(brand){var event=new Event('input',{bubbles:true});event.simulated=true;var brand=document.querySelectorAll('[placeholder=\"Tìm kiếm Thương hiệu\"]');if(brand.length<=0){alert('Không tìm thấy brand, bạn đã setup brand chưa?');};brand[0].value=brand;brand[0].dispatchEvent(event);var brandValue=document.querySelectorAll('.dropdown-input__item, .shopee-dropdown-item');brandValue[0].click();setTimeout(setMeterial,2000,clone(material));};var setDesc=function(text){var event=new Event('input',{bubbles:true});event.simulated=true;var descNodeParent=document.querySelectorAll('.textarea,.description');var textDesc=descNodeParent[0].getElementsByClassName('shopee-input__inner');textDesc[0].value=text;textDesc[0].dispatchEvent(event);setTimeout(setBrand,1500,clone(brandName));};setTimeout(setDesc,1500,clone(desc));})(" + shopeeSetting + ")");
                this.step = "checkFinishedProductDesc";
                checkFinishedProductDesc();
            }
            catch (Exception ee)
            {
                Console.WriteLine("addDesigName has an exception: " + ee.Message);
            }

        }

        private async void checkFinishedProductDesc()
        {
            try
            {
                if (this.step != "checkFinishedProductDesc")
                {
                    return;
                }
                await Task.Delay(2000);
                WriteLog("Step3 - checkFinishedProductDesc - check isDone?");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                string script = "(function(){var merterialDom=document.querySelectorAll('[placeholder=\"Tìm kiếm Chất liệu\"]');if(merterialDom.length<=0){return false;};return merterialDom[0].value && merterialDom[0].value!='';})()";
                var domTask = chrome.browser.EvaluateScriptAsync(script);

                await domTask.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        var response = t.Result;
                        if (response.Success && !string.IsNullOrEmpty(response.Result.ToString()))
                        {
                            bool isDone = Convert.ToBoolean(response.Result.ToString());
                            if (isDone == true)
                            {

                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    this.step = "setProductVariants";
                                    setProductVariants();
                                });
                            }
                            else
                            {
                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    checkFinishedProductDesc();
                                });
                            }
                        }
                        else
                        {
                            chrome.browser.Invoke((MethodInvoker)delegate
                            {
                                checkFinishedProductDesc();
                            });
                        }
                    }
                });
            }
            catch (Exception ee)
            {
                WriteLog("checkFinishedProductDesc has exception: " + ee.Message);
            }
        }

        private async void setProductVariants()
        {
            try
            {
                if (this.step != "setProductVariants")
                {
                    return;
                }
                if(activeItem.ShopeeSetting.Colors == null || activeItem.ShopeeSetting.Colors.Count <= 0)
                {
                    MessageBox.Show("Bạn chưa setup bất kỳ màu áo nào trong phần setting cho áo hiện hiện. Nên tool stop");
                    this.StopApp();
                    return;
                }
                if (activeItem.ShopeeSetting.Sizes == null || activeItem.ShopeeSetting.Sizes.Count <= 0)
                {
                    MessageBox.Show("Bạn chưa setup bất kỳ size áo nào trong phần setting cho áo hiện hiện. Nên tool stop");
                    this.StopApp();
                    return;
                }
                await Task.Delay(2000);
                WriteLog("Step3 - setProductVariants");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                string script = "(function(ShopeeSetting,sku,pricePro){debugger;var labelSize='Size';var labelColor='Màu';var listSizes=ShopeeSetting.Sizes;var listColorObj=ShopeeSetting.Colors;var listColors=[];if(listColorObj&&listColorObj.length>0){for(var mk=0;mk<listColorObj.length;mk++){listColors.push(listColorObj[mk].Name);}};var price=pricePro;var quantity=ShopeeSetting.Quantity;function clone(obj){if(null==obj||'object'!=typeof obj);return obj;var copy=obj.constructor();for(var attr in obj){if(obj.hasOwnProperty(attr));copy[attr]=obj[attr];};return copy;};var addButtons=document.querySelectorAll('.repeater-add');if(addButtons&&addButtons[0]){addButtons[0].click();};var initVariantValues=function(index,isSetupSize){debugger;var event=new Event('input',{bubbles:true});event.simulated=true;var listItem=!isSetupSize?listColors:listSizes;var plusIndexIfVariant2=!isSetupSize?index:(listColors.length+index);var variantDetailDoms=document.querySelectorAll('[placeholder*=\"Nhập phân loại hàng\"]');if(variantDetailDoms){variantDetailDoms[plusIndexIfVariant2].value=listItem[index];variantDetailDoms[plusIndexIfVariant2].dispatchEvent(event);};if(index==listItem.length-1&&!isSetupSize){var addButtons=document.querySelectorAll('.repeater-add');if(addButtons&&addButtons[0]){addButtons[1].click();};setTimeout(setVariantLabel,2000,true);}else if(index==listItem.length-1){var priceAllDom=document.querySelectorAll('[placeholder*=\"Giá\"]');priceAllDom[0].value=price;priceAllDom[0].dispatchEvent(event);var stockDom=document.querySelectorAll('[placeholder*=\"Kho hàng\"]');stockDom[0].value=quantity;stockDom[0].dispatchEvent(event);var skuDom=document.querySelectorAll('[placeholder*=\"SKU phân loại\"]');skuDom[0].value=sku;skuDom[0].dispatchEvent(event);setTimeout(()=>{var applyAllBtn=document.querySelectorAll('.shopee-button--primary');applyAllBtn[0].click();},1700);}};var initVariantInputs=function(isSetupSize){var count=isSetupSize?listSizes.length:listColors.length;var addMoreIndex=!isSetupSize?0:1;for(var ind=0;ind<count;ind++){var addButtons=document.querySelectorAll('.repeater-add');if(addButtons&&addButtons[addMoreIndex]&&ind<count-1){addButtons[addMoreIndex].click();};setTimeout(initVariantValues,1000,clone(ind),clone(isSetupSize));};};var setVariantLabel=function(isSetupSize){var index=isSetupSize?1:0;var event=new Event('input',{bubbles:true});event.simulated=true;var nameVariantDom=document.querySelectorAll('[placeholder*=\"Nhập tên Nhóm phân loại hàng\"]');nameVariantDom[index].value=isSetupSize?labelSize:labelColor;nameVariantDom[index].dispatchEvent(event);setTimeout(initVariantInputs,2000,clone(isSetupSize));};setTimeout(setVariantLabel,2000,false);})(" + shopeeSetting + ",'" + activeItem.Sku + "','" + activeItem.Price + "')";
                await chrome.browser.EvaluateScriptAsync(script);
                this.step = "checkFinishedSetVariants";
                checkFinishedSetVariants();
            }
            catch (Exception ee)
            {
                Console.WriteLine("setProductVariants has an exception: " + ee.Message);
            }
        }

        private async void checkFinishedSetVariants()
        {
            try
            {
                if (this.step != "checkFinishedSetVariants")
                {
                    return;
                }
                await Task.Delay(2000);
                WriteLog("Step3 - setProductVariants - check isDone?");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                string script = "(function(ShopeeSetting){var parentDom=document.querySelector('.variation-table');if(parentDom!=null){var listPrices=parentDom.querySelectorAll('.shopee-input__input');for(var i=0;i<listPrices.length;i++){if(listPrices[i].value==''){return false;}}}else{return false};if(ShopeeSetting.DemoProductInfo&&ShopeeSetting.DemoProductInfo.PriceDemo&&ShopeeSetting.DemoProductInfo.SizeDemo&&ShopeeSetting.DemoProductInfo.ColorDemo){debugger;var count=0;for(var i=0;i<ShopeeSetting.Colors.length;i++){for(var j=0;j<ShopeeSetting.Sizes.length;j++){if(ShopeeSetting.Colors[i].Name==ShopeeSetting.DemoProductInfo.ColorDemo&&ShopeeSetting.Sizes[j]==ShopeeSetting.DemoProductInfo.SizeDemo){count=count+1;break;}else{count=count+1;}}};var event=new Event('input',{bubbles:true});event.simulated=true;count=(count*2)-2;var listPrices=parentDom.querySelectorAll('.shopee-input__input');listPrices[count].value=ShopeeSetting.DemoProductInfo.PriceDemo;listPrices[count].dispatchEvent(event);};return true;})(" + shopeeSetting + ")";
                var domTask = chrome.browser.EvaluateScriptAsync(script);
               
                await domTask.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        var response = t.Result;
                        if (response.Success && response.Result != null)
                        {
                            bool isDone = Convert.ToBoolean(response.Result.ToString());
                            if (isDone == true)
                            {

                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    this.step = "setProductIamges";
                                    setProductIamges();
                                });
                            }
                            else
                            {
                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    checkFinishedSetVariants();
                                });
                            }
                        }
                        else
                        {
                            chrome.browser.Invoke((MethodInvoker)delegate
                            {
                                checkFinishedSetVariants();
                            });
                        }
                    }
                });
            }
            catch (Exception ee)
            {
                WriteLog("checkFinishedSetVariants has exception: " + ee.Message);
            }
        }

        private async void setProductIamges()
        {
            try
            {
                if (this.step != "setProductIamges")
                {
                    return;
                }
                await Task.Delay(2000);
                WriteLog("Step4 - setProductIamges");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);

                List<FileObject> fileObjects = new List<FileObject>();
                //current picture
                //FileObject originalFileObj = ToolUtils.ConvertImageToBase64(activeItem.TmpFullPath);
                //fileObjects.Add(originalFileObj);
                //all related picture start with #
                string[] relatedFilePaths = ToolUtils.getFileStartWith(activeItem.TmpFullPath);
                var counter = 0;
                if(relatedFilePaths != null && relatedFilePaths.Length > 0)
                {
                    for(var index =0;index < relatedFilePaths.Count();index ++)
                    {
                        if(counter > 8)
                        {
                            break;
                        }
                        FileObject fObj = ToolUtils.ConvertImageToBase64(relatedFilePaths[index]);
                        fileObjects.Add(fObj);
                        counter = counter + 1;
                    }
                }
                if(activeItem.ShopeeSetting.IsGenMockup == true)
                {
                    generateMockup(fileObjects, counter);
                }
                if(fileObjects == null || fileObjects.Count <= 0)
                {
                    MessageBox.Show("Không có áo hay mockup nào được generate cho design hiện tại. Coi lại có áo cùng tên dấu '#' hoặc chưa check 'Genmockup' trong setting.");
                    this.StopApp();
                }
                string fileObjectsJsonString = JsonConvert.SerializeObject(fileObjects);
                string script = "(function(fileObjects){debugger;function base64toBlob(base64Data,contentType){contentType=contentType||'';var sliceSize=1024;var byteCharacters=atob(base64Data);var bytesLength=byteCharacters.length;var slicesCount=Math.ceil(bytesLength/sliceSize);var byteArrays=new Array(slicesCount);for(var sliceIndex=0;sliceIndex<slicesCount;++sliceIndex){var begin=sliceIndex*sliceSize;var end=Math.min(begin+sliceSize,bytesLength);var bytes=new Array(end-begin);for(var offset=begin,i=0;offset<end;++i,++offset){bytes[i]=byteCharacters[offset].charCodeAt(0);};byteArrays[sliceIndex]=new Uint8Array(bytes);};return new Blob(byteArrays,{type:contentType});};var productiamgeEvent=new Event('change',{bubbles:true});productiamgeEvent.simulated=true;var transfer=new DataTransfer();for(var inx=0;inx<fileObjects.length;inx++){var file=new File([base64toBlob(fileObjects[inx].Base64Content,fileObjects[inx].FileType)],fileObjects[inx].FileName,{type:fileObjects[inx].FileType});transfer.items.add(file);};var uploader=document.querySelector('.shopee-image-manager').querySelector('.shopee-upload__input');uploader.files=transfer.files;uploader.dispatchEvent(productiamgeEvent);})(" + fileObjectsJsonString + ")";
                await chrome.browser.EvaluateScriptAsync(script);


                if (activeItem.ShopeeSetting.IsGenMockup == true)
                {
                    this.step = "setProductIamgesByColor";
                    setProductIamgesByColor();
                }
                else
                {
                    this.step = "setSizeChart";
                    setSizeChartAsync();
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine("setProductIamges has an exception: " + ee.Message);
            }
        }

        private async void setProductIamgesByColor()
        {
            try
            {
                if (this.step != "setProductIamgesByColor")
                {
                    return;
                }
                await Task.Delay(2000);
                WriteLog("Step4.1 - setProductIamgesByColor");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);

                List<FileObject> fileObjects = new List<FileObject>();
                
                if (activeItem.ShopeeSetting.IsGenMockup == true)
                {
                    //gen mockup cho tung color đe up theo mau iamge
                    generateMockup(fileObjects, 0);
                }
                if (fileObjects == null || fileObjects.Count <= 0)
                {
                    MessageBox.Show("Không có áo hay mockup nào được generate cho design hiện tại. Coi lại có áo cùng tên dấu '#' hoặc chưa check 'Genmockup' trong setting.");
                    this.StopApp();
                }
                string fileObjectsJsonString = JsonConvert.SerializeObject(fileObjects);
                string script = "(function(fileObjects){debugger;function base64toBlob(base64Data,contentType){contentType=contentType||'';var sliceSize=1024;var byteCharacters=atob(base64Data);var bytesLength=byteCharacters.length;var slicesCount=Math.ceil(bytesLength/sliceSize);var byteArrays=new Array(slicesCount);for(var sliceIndex=0;sliceIndex<slicesCount;++sliceIndex){var begin=sliceIndex*sliceSize;var end=Math.min(begin+sliceSize,bytesLength);var bytes=new Array(end-begin);for(var offset=begin,i=0;offset<end;++i,++offset){bytes[i]=byteCharacters[offset].charCodeAt(0);};byteArrays[sliceIndex]=new Uint8Array(bytes);};return new Blob(byteArrays,{type:contentType});};var productiamgeEvent=new Event('change',{bubbles:true});productiamgeEvent.simulated=true;var transfer=new DataTransfer();for(var inx=0;inx<fileObjects.length;inx++){var file=new File([base64toBlob(fileObjects[inx].Base64Content,fileObjects[inx].FileType)],fileObjects[inx].FileName,{type:fileObjects[inx].FileType});transfer.items.add(file);};var uploader=document.querySelectorAll('.shopee-image-manager')[1].querySelector('.shopee-upload__input');uploader.files=transfer.files;uploader.dispatchEvent(productiamgeEvent);})(" + fileObjectsJsonString + ")";
                await chrome.browser.EvaluateScriptAsync(script);

                this.step = "setSizeChart";
                setSizeChartAsync();
            }
            catch (Exception ee)
            {
                Console.WriteLine("setProductIamgesByColor has an exception: " + ee.Message);
            }
        }

        private void generateMockup(List<FileObject> fileObjects, int counter)
        {
            foreach(StyleColor color in activeItem.ShopeeSetting.Colors)
            {
                if(counter > 8)
                {
                    break;
                }
                Bitmap createdImg = null;
                if (ToolUtils.MOCKUP_IMAGES != null && ToolUtils.MOCKUP_IMAGES.Count > 0)
                {
                    ImageItem main = ToolUtils.MOCKUP_IMAGES[ToolUtils.GetNextBgFile(ToolUtils.MOCKUP_IMAGES.Count)];
                    //Correct mockup path current installation location
                    main.DirectoryPath = System.IO.Path.Combine(ToolUtils.BASE_FOLDER + "assets\\configs\\mockups", DirectoryUtil.GetDirectoryName(main.DirectoryPath));
                    main.AdditionalImageFullPath = System.IO.Path.Combine(main.DirectoryPath, System.IO.Path.GetFileName(main.AdditionalImageFullPath));
                    main.Thumbnail = System.IO.Path.Combine(main.DirectoryPath, System.IO.Path.GetFileName(main.Thumbnail));
                    main.FullPath = System.IO.Path.Combine(main.DirectoryPath, System.IO.Path.GetFileName(main.FullPath));

                    ImageItem tag = null;
                    if (ToolUtils.BRAND_TAG != null && ToolUtils.BRAND_TAG.Count > 0)
                    {
                        tag = ToolUtils.BRAND_TAG[ToolUtils.GetNextTagFile(ToolUtils.BRAND_TAG.Count)];
                        tag.Angle = main.Angle;
                        tag.Rect = main.AdditionalRect;
                    }

                    var selectedColor = main.Colors.Find(c => c.Name.ToLower() == color.Name.ToLower());
                    ImageItem effect = new ImageItem()
                    {
                        FullPath = main.FullPath,
                        //Opacity = selectedColor.Opacity//Opacity theo color nhe; main.Opacity
                        Opacity = ToolUtils.getOpacity(selectedColor.Opacity, selectedColor.Name, main)
                    };
                    ImageItem background = new ImageItem()
                    {
                        FullPath = main.AdditionalImageFullPath
                    };
                    ImageItem designItem = new ImageItem()
                    {
                        FullPath = activeItem.TmpFullPath,
                        Rect = main.Rect
                    };
                    var fileName = activeItem.Name + "#" + color.Name +".jpeg";
                    createdImg = ImageUtil.merged(1500, 0, 500, selectedColor.ColorCode, background, effect, designItem, tag);
                    if (createdImg != null)
                    {
                        var fileObject = ToolUtils.getMockupInBase64(createdImg, fileName);
                        fileObjects.Add(fileObject);
                        createdImg.Dispose();
                    }
                }
            }
        }

        private async void setSizeChartAsync()
        {
            try
            {
                if (this.step != "setSizeChart")
                {
                    return;
                }
                if (ToolUtils.isEmpty(activeItem.ShopeeSetting.SizePicture))
                {
                    this.step = "setShippingInfo";
                    setShippingInfo();
                }
                await Task.Delay(2000);
                WriteLog("Step5 - setSizeChart");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                FileObject fileObject = ToolUtils.ConvertImageToBase64(activeItem.ShopeeSetting.SizePicture);
                string fileObjectJsonString = JsonConvert.SerializeObject(fileObject);
                string script = "(function(fileObject){debugger;var base64toBlob=function(base64Data,contentType){contentType=contentType||'';var sliceSize=1024;var byteCharacters=atob(base64Data);var bytesLength=byteCharacters.length;var slicesCount=Math.ceil(bytesLength/sliceSize);var byteArrays=new Array(slicesCount);for(var sliceIndex=0;sliceIndex<slicesCount;++sliceIndex){var begin=sliceIndex*sliceSize;var end=Math.min(begin+sliceSize,bytesLength);var bytes=new Array(end-begin);for(var offset=begin,i=0;offset<end;++i,++offset){bytes[i]=byteCharacters[offset].charCodeAt(0);};byteArrays[sliceIndex]=new Uint8Array(bytes);};return new Blob(byteArrays,{type:contentType});};var file=new File([base64toBlob(fileObject.Base64Content,fileObject.FileType)],fileObject.FileName,{type:fileObject.FileType});var sizeChartZone=document.querySelectorAll('.size-chart-content');var fileInput=sizeChartZone[0].querySelectorAll('[type=\"file\"]');var eventChart=new Event('change',{bubbles:true});eventChart.simulated=true;var transfer=new DataTransfer();transfer.items.add(file);var uploader=fileInput[0];uploader.files=transfer.files;uploader.dispatchEvent(eventChart);})(" + fileObjectJsonString + ")";
                await chrome.browser.EvaluateScriptAsync(script);

                this.step = "setShippingInfo";
                setShippingInfo();
            }
            catch (Exception ee)
            {
                Console.WriteLine("setSizeChart has an exception: " + ee.Message);
            }
        }

        private async void setShippingInfo()
        {
            try
            {
                if (this.step != "setShippingInfo")
                {
                    return;
                }
                await Task.Delay(2000);
                WriteLog("Step6 - setShippingInfo");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                string script = "(function(ShopeeSetting,sku){debugger;var weight=ShopeeSetting.Weight;var rong=ShopeeSetting.ShipRong;var dai=ShopeeSetting.ShipDai;var cao=ShopeeSetting.ShipCao;var preoder=ShopeeSetting.Preorder;var event=new Event('input',{bubbles:true});event.simulated=true;var containerDom=document.querySelectorAll('.product-shipping');var inputs=containerDom[0].getElementsByClassName('shopee-input__input');inputs[0].value=weight;inputs[0].dispatchEvent(event);inputs[1].value=rong;inputs[1].dispatchEvent(event);inputs[2].value=dai;inputs[2].dispatchEvent(event);inputs[3].value=cao;inputs[3].dispatchEvent(event);var thongTinKhac=function(){var listInput=document.querySelectorAll('.shopee-input__input');listInput[listInput.length-1].value=sku;listInput[listInput.length-1].dispatchEvent(event);var radioButton=document.querySelectorAll('.shopee-radio');if(preoder&&preoder.toString()=='1'){radioButton[1].click();}};var setSwitchButton=function(){var buttons=document.querySelectorAll('.shopee-switch--close');for(var index=0;index<buttons.length;index++){buttons[index].click();};thongTinKhac();};setTimeout(setSwitchButton,2000);})(" + shopeeSetting + ",'" + activeItem.Sku + "')";
                await chrome.browser.EvaluateScriptAsync(script);

                this.step = "SetupFinishPublish";
                SetupFinishPublish();
            }
            catch (Exception ee)
            {
                Console.WriteLine("setShippingInfo has an exception: " + ee.Message);
            }
        }

        private async void SetupFinishPublish()
        {
            try
            {
                if (this.step != "SetupFinishPublish")
                {
                    return;
                }
                await Task.Delay(6000);
                WriteLog("Step7 - SetupFinishPublish");
                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                string script = "(function(ShopeeSetting){var saveActions=document.querySelector('.product-detail-button').querySelectorAll('.shopee-button--xl-large');if(saveActions&&saveActions.length>2){if(ShopeeSetting.SaveAction=='1'){saveActions[2].click();}else{saveActions[1].click();}}})(" + shopeeSetting + ")";
                await chrome.browser.EvaluateScriptAsync(script);
                IsFinished();
            }
            catch (Exception ee)
            {
                Console.WriteLine("SetupFinishPublish has an exception: " + ee.Message);
            }
        }
        private async void IsFinished()
        {
            try
            {
                if (activeItem == null)
                {
                    return;
                }
                await Task.Delay(1000);
                var domTask = chrome.browser.EvaluateScriptAsync("(function(){debugger;if(window.location.href.indexOf('/list/') > 0){return true;}else{return false;}})()");
                await domTask.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        var response = t.Result;
                        if (response.Success && response.Result != null)
                        {
                            bool isDone = Convert.ToBoolean(response.Result.ToString());
                            if (isDone == true)
                            {
                                
                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    WriteLog("Finished upload: " + this.activeItem.Name + "----------------");
                                    FinishUploadAStyle(index+1);
                                });
                            }
                            else
                            {
                                chrome.browser.Invoke((MethodInvoker)delegate
                                {
                                    string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                                    string script = "(function(ShopeeSetting){var saveActions=document.querySelector('.product-detail-button').querySelectorAll('.shopee-button--xl-large');if(saveActions&&saveActions.length>2){if(ShopeeSetting.SaveAction=='1'){saveActions[2].click();}else{saveActions[1].click();}}})(" + shopeeSetting + ")";
                                    chrome.browser.EvaluateScriptAsync(script);
                                    IsFinished();
                                });
                            }
                        }
                        else
                        {
                            chrome.browser.Invoke((MethodInvoker)delegate
                            {
                                string shopeeSetting = JsonConvert.SerializeObject(activeItem.ShopeeSetting);
                                string script = "(function(ShopeeSetting){var saveActions=document.querySelector('.product-detail-button').querySelectorAll('.shopee-button--xl-large');if(saveActions&&saveActions.length>2){if(ShopeeSetting.SaveAction=='1'){saveActions[2].click();}else{saveActions[1].click();}}})(" + shopeeSetting + ")";
                                chrome.browser.EvaluateScriptAsync(script);
                                IsFinished();
                            });
                        }
                    }
                });
            }
            catch (Exception ee)
            {
                Console.WriteLine("isFinshed has exception: " + ee.Message);
            }
        }

        private async void CheckingLoginSession(string username, string password)
        {
            if(this.step != "CheckingLoginSession")
            {
                return;
            }
            try
            {
                if (checkingLoginSessionForm && !stopApp)
                {
                    await Task.Delay(5000);
                    if (checkingLoginSessionForm && !stopApp)
                    {
                        try
                        {
                            var domTask = chrome.browser.GetBrowser().GetFrame("app-iframe").EvaluateScriptAsync("(function() {return $('#LoginBox:visible').length})();");
                            await domTask.ContinueWith(t =>
                            {
                                if (!t.IsFaulted)
                                {
                                    var response = t.Result;
                                    if (response.Success && response.Result != null)
                                    {
                                        this.step = "CheckingLoginSessionDone";
                                        bool isDomExisted = Convert.ToInt32(response.Result.ToString()) != 0;
                                        if (isDomExisted)
                                        {
                                            chrome.browser.Invoke((MethodInvoker)delegate
                                            {
                                                chrome.browser.GetBrowser().GetFrame("app-iframe").ExecuteJavaScriptAsync("(function(username,password){var evt=document.createEvent('MouseEvents');evt.initEvent('input',true,true);var form=$('#LoginBox:visible');var email=form.find('#liemail')[0];email.value=username;email.dispatchEvent(evt);var passwordDom=form.find('#lipw')[0];passwordDom.value=password;passwordDom.dispatchEvent(evt);form.find('#btnLogin')[0].click();})('" + username + "','" + password + "')");
                                                this.CheckingLoginSession(username, password);
                                            });
                                        }
                                        else
                                        {
                                            chrome.browser.Invoke((MethodInvoker)delegate
                                            {
                                                this.CheckingLoginSession(username, password);
                                            });
                                        }

                                    }
                                    else
                                    {
                                        chrome.browser.Invoke((MethodInvoker)delegate
                                        {
                                            this.CheckingLoginSession(username, password);
                                        });
                                    }
                                }
                                else
                                {
                                    chrome.browser.Invoke((MethodInvoker)delegate
                                    {
                                        this.CheckingLoginSession(username, password);
                                    });
                                }
                            });
                        }
                        catch (Exception ee)
                        {
                            chrome.browser.Invoke((MethodInvoker)delegate
                            {
                                this.CheckingLoginSession(username, password);
                            });
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine("checkingLoginSession has an exception:" + ee.Message);
            }
        }

        public void StartCheckingLoginForm(string username, string password)
        {
            checkingLoginSessionForm = true;
            stopApp = false;
            this.step = "CheckingLoginSession";
            CheckingLoginSession(username, password);
        }
        public void StopCheckingLoginForm()
        {
            checkingLoginSessionForm = false;
        }
    }
}
