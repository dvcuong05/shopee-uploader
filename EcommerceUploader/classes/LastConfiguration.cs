﻿using NCCLibs;
using System.Collections.Generic;

namespace Main.classes
{
    public class LastConfiguration
    {
        private List<ImageItem> mockups = new List<ImageItem>();
        public List<ImageItem> Mockups { get => mockups; set => mockups = value; }
    }
}
