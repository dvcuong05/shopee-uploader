﻿namespace Main.classes
{
    public class StyleColor
    {
        string value;
        string colorCode;
        string name;
        int opacity;
        bool isDemo;
        bool selected;

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public string ColorCode
        {
            get { return colorCode; }
            set { colorCode = value; }
        }

        public string Name { get => name; set => name = value; }
        public int Opacity { get => opacity; set => opacity = value; }
        public bool IsDemo { get => isDemo; set => isDemo = value; }
        public bool Selected { get => selected; set => selected = value; }
    }
}
