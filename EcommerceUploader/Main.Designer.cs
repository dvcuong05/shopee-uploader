﻿using System.Windows.Forms;
namespace Main
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.listItemFlow = new System.Windows.Forms.FlowLayoutPanel();
            this.countDownLb = new System.Windows.Forms.Label();
            this.ExportButton = new System.Windows.Forms.Panel();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupWorking = new System.Windows.Forms.GroupBox();
            this.buttonSetupShopee = new System.Windows.Forms.Button();
            this.browseFile = new System.Windows.Forms.Button();
            this.folderPath = new System.Windows.Forms.TextBox();
            this.GroupAction = new System.Windows.Forms.GroupBox();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.ButtonSetting = new System.Windows.Forms.Button();
            this.ButtonStop = new System.Windows.Forms.Button();
            this.ButtonStart = new System.Windows.Forms.Button();
            this.ButtonSave = new System.Windows.Forms.Button();
            this.ButtonReset = new System.Windows.Forms.Button();
            this.PanelLog = new System.Windows.Forms.Panel();
            this.copyLogBtn = new System.Windows.Forms.Button();
            this.lockBrowserChecbox = new System.Windows.Forms.CheckBox();
            this.totalRemainLabel = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.totalUploadedLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.totalItemLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Logger = new System.Windows.Forms.RichTextBox();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.toolTipTeescape = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.listItemFlow.SuspendLayout();
            this.ExportButton.SuspendLayout();
            this.GroupWorking.SuspendLayout();
            this.GroupAction.SuspendLayout();
            this.PanelLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.AutoScroll = true;
            this.splitContainer.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer.Panel1.Controls.Add(this.listItemFlow);
            this.splitContainer.Panel1.Controls.Add(this.ExportButton);
            this.splitContainer.Panel1.Controls.Add(this.PanelLog);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.AutoScroll = true;
            this.splitContainer.Panel2.BackColor = System.Drawing.SystemColors.Menu;
            this.splitContainer.Panel2Collapsed = true;
            this.splitContainer.Size = new System.Drawing.Size(1354, 733);
            this.splitContainer.SplitterDistance = 546;
            this.splitContainer.TabIndex = 0;
            // 
            // listItemFlow
            // 
            this.listItemFlow.AutoScroll = true;
            this.listItemFlow.AutoSize = true;
            this.listItemFlow.BackColor = System.Drawing.Color.DarkGray;
            this.listItemFlow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.listItemFlow.Controls.Add(this.countDownLb);
            this.listItemFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listItemFlow.Location = new System.Drawing.Point(0, 134);
            this.listItemFlow.Margin = new System.Windows.Forms.Padding(0);
            this.listItemFlow.Name = "listItemFlow";
            this.listItemFlow.Size = new System.Drawing.Size(1352, 470);
            this.listItemFlow.TabIndex = 0;
            this.listItemFlow.Visible = false;
            this.listItemFlow.Scroll += new System.Windows.Forms.ScrollEventHandler(this.listItemFlow_Scroll);
            this.listItemFlow.SizeChanged += new System.EventHandler(this.listItemFlow_SizeChanged);
            // 
            // countDownLb
            // 
            this.countDownLb.AutoSize = true;
            this.countDownLb.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countDownLb.ForeColor = System.Drawing.Color.SpringGreen;
            this.countDownLb.Location = new System.Drawing.Point(3, 0);
            this.countDownLb.Name = "countDownLb";
            this.countDownLb.Size = new System.Drawing.Size(0, 23);
            this.countDownLb.TabIndex = 70;
            this.countDownLb.Visible = false;
            // 
            // ExportButton
            // 
            this.ExportButton.BackColor = System.Drawing.Color.Gainsboro;
            this.ExportButton.Controls.Add(this.textBoxPrice);
            this.ExportButton.Controls.Add(this.label1);
            this.ExportButton.Controls.Add(this.GroupWorking);
            this.ExportButton.Controls.Add(this.GroupAction);
            this.ExportButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExportButton.Location = new System.Drawing.Point(0, 0);
            this.ExportButton.Margin = new System.Windows.Forms.Padding(0);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(1352, 134);
            this.ExportButton.TabIndex = 13;
            this.ExportButton.Visible = false;
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(92, 106);
            this.textBoxPrice.MaxLength = 150;
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(147, 20);
            this.textBoxPrice.TabIndex = 76;
            this.toolTipTeescape.SetToolTip(this.textBoxPrice, "Được dùng khi từng áo không setup giá riêng");
            this.textBoxPrice.TextChanged += new System.EventHandler(this.textBoxPrice_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 75;
            this.label1.Text = "Giá bán chung";
            // 
            // GroupWorking
            // 
            this.GroupWorking.BackColor = System.Drawing.Color.Transparent;
            this.GroupWorking.Controls.Add(this.buttonSetupShopee);
            this.GroupWorking.Controls.Add(this.browseFile);
            this.GroupWorking.Controls.Add(this.folderPath);
            this.GroupWorking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GroupWorking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupWorking.Location = new System.Drawing.Point(0, 64);
            this.GroupWorking.Name = "GroupWorking";
            this.GroupWorking.Size = new System.Drawing.Size(401, 42);
            this.GroupWorking.TabIndex = 0;
            this.GroupWorking.TabStop = false;
            // 
            // buttonSetupShopee
            // 
            this.buttonSetupShopee.BackColor = System.Drawing.Color.MediumVioletRed;
            this.buttonSetupShopee.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSetupShopee.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSetupShopee.ForeColor = System.Drawing.Color.White;
            this.buttonSetupShopee.Location = new System.Drawing.Point(276, 0);
            this.buttonSetupShopee.Name = "buttonSetupShopee";
            this.buttonSetupShopee.Size = new System.Drawing.Size(119, 44);
            this.buttonSetupShopee.TabIndex = 2;
            this.buttonSetupShopee.Text = "Setup Product data";
            this.buttonSetupShopee.UseVisualStyleBackColor = false;
            this.buttonSetupShopee.Click += new System.EventHandler(this.buttonSetupShopee_Click);
            // 
            // browseFile
            // 
            this.browseFile.BackColor = System.Drawing.Color.Transparent;
            this.browseFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.browseFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.browseFile.FlatAppearance.BorderSize = 0;
            this.browseFile.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.browseFile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.browseFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.browseFile.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browseFile.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.browseFile.Image = ((System.Drawing.Image)(resources.GetObject("browseFile.Image")));
            this.browseFile.Location = new System.Drawing.Point(242, 9);
            this.browseFile.Name = "browseFile";
            this.browseFile.Padding = new System.Windows.Forms.Padding(3);
            this.browseFile.Size = new System.Drawing.Size(28, 27);
            this.browseFile.TabIndex = 1;
            this.browseFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.browseFile.UseVisualStyleBackColor = false;
            this.browseFile.Click += new System.EventHandler(this.BrowseFile_Click);
            // 
            // folderPath
            // 
            this.folderPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.folderPath.BackColor = System.Drawing.Color.White;
            this.folderPath.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.folderPath.Location = new System.Drawing.Point(7, 15);
            this.folderPath.MaxLength = 2000;
            this.folderPath.Name = "folderPath";
            this.folderPath.Size = new System.Drawing.Size(232, 21);
            this.folderPath.TabIndex = 0;
            this.folderPath.TabStop = false;
            this.folderPath.TextChanged += new System.EventHandler(this.LinkFolder_TextChanged);
            // 
            // GroupAction
            // 
            this.GroupAction.BackColor = System.Drawing.Color.Transparent;
            this.GroupAction.Controls.Add(this.buttonLogout);
            this.GroupAction.Controls.Add(this.ButtonSetting);
            this.GroupAction.Controls.Add(this.ButtonStop);
            this.GroupAction.Controls.Add(this.ButtonStart);
            this.GroupAction.Controls.Add(this.ButtonSave);
            this.GroupAction.Controls.Add(this.ButtonReset);
            this.GroupAction.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GroupAction.Location = new System.Drawing.Point(3, 0);
            this.GroupAction.Name = "GroupAction";
            this.GroupAction.Size = new System.Drawing.Size(381, 63);
            this.GroupAction.TabIndex = 12;
            this.GroupAction.TabStop = false;
            // 
            // buttonLogout
            // 
            this.buttonLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.buttonLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLogout.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateGray;
            this.buttonLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.buttonLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.buttonLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogout.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogout.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonLogout.Location = new System.Drawing.Point(324, 9);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(57, 50);
            this.buttonLogout.TabIndex = 71;
            this.buttonLogout.Text = "Login / Logout";
            this.toolTipTeescape.SetToolTip(this.buttonLogout, "Login/Logout Shope");
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // ButtonSetting
            // 
            this.ButtonSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(79)))), ((int)(((byte)(32)))));
            this.ButtonSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonSetting.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSetting.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateGray;
            this.ButtonSetting.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.ButtonSetting.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.ButtonSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSetting.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonSetting.Location = new System.Drawing.Point(186, 11);
            this.ButtonSetting.Name = "ButtonSetting";
            this.ButtonSetting.Size = new System.Drawing.Size(64, 48);
            this.ButtonSetting.TabIndex = 70;
            this.ButtonSetting.Text = "Setup Mockup";
            this.toolTipTeescape.SetToolTip(this.ButtonSetting, "Settup shop information for upload and link product to Sale Chanels");
            this.ButtonSetting.UseVisualStyleBackColor = false;
            this.ButtonSetting.Click += new System.EventHandler(this.ButtonSetting_Click);
            // 
            // ButtonStop
            // 
            this.ButtonStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.ButtonStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonStop.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateGray;
            this.ButtonStop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.ButtonStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.ButtonStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonStop.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonStop.ForeColor = System.Drawing.SystemColors.Control;
            this.ButtonStop.Image = ((System.Drawing.Image)(resources.GetObject("ButtonStop.Image")));
            this.ButtonStop.Location = new System.Drawing.Point(266, 11);
            this.ButtonStop.Name = "ButtonStop";
            this.ButtonStop.Size = new System.Drawing.Size(52, 48);
            this.ButtonStop.TabIndex = 3;
            this.toolTipTeescape.SetToolTip(this.ButtonStop, "Stop application");
            this.ButtonStop.UseVisualStyleBackColor = false;
            this.ButtonStop.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // ButtonStart
            // 
            this.ButtonStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(79)))), ((int)(((byte)(32)))));
            this.ButtonStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonStart.CausesValidation = false;
            this.ButtonStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonStart.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateGray;
            this.ButtonStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.ButtonStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.ButtonStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonStart.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonStart.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonStart.Image = ((System.Drawing.Image)(resources.GetObject("ButtonStart.Image")));
            this.ButtonStart.Location = new System.Drawing.Point(5, 11);
            this.ButtonStart.Name = "ButtonStart";
            this.ButtonStart.Size = new System.Drawing.Size(52, 48);
            this.ButtonStart.TabIndex = 0;
            this.toolTipTeescape.SetToolTip(this.ButtonStart, "Start to upload t-shirt");
            this.ButtonStart.UseVisualStyleBackColor = false;
            this.ButtonStart.Click += new System.EventHandler(this.ButtonStart_ClickAsync);
            // 
            // ButtonSave
            // 
            this.ButtonSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(79)))), ((int)(((byte)(32)))));
            this.ButtonSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSave.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateGray;
            this.ButtonSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.ButtonSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.ButtonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSave.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSave.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonSave.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSave.Image")));
            this.ButtonSave.Location = new System.Drawing.Point(126, 11);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(52, 48);
            this.ButtonSave.TabIndex = 2;
            this.toolTipTeescape.SetToolTip(this.ButtonSave, "Save all setting for current folder");
            this.ButtonSave.UseVisualStyleBackColor = false;
            this.ButtonSave.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // ButtonReset
            // 
            this.ButtonReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(79)))), ((int)(((byte)(32)))));
            this.ButtonReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonReset.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateGray;
            this.ButtonReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.ButtonReset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.ButtonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonReset.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonReset.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonReset.Image = ((System.Drawing.Image)(resources.GetObject("ButtonReset.Image")));
            this.ButtonReset.Location = new System.Drawing.Point(65, 11);
            this.ButtonReset.Name = "ButtonReset";
            this.ButtonReset.Size = new System.Drawing.Size(52, 48);
            this.ButtonReset.TabIndex = 1;
            this.toolTipTeescape.SetToolTip(this.ButtonReset, "Reset all setting of current folder");
            this.ButtonReset.UseVisualStyleBackColor = false;
            this.ButtonReset.Click += new System.EventHandler(this.ResetBtn_Click);
            // 
            // PanelLog
            // 
            this.PanelLog.BackColor = System.Drawing.Color.White;
            this.PanelLog.Controls.Add(this.copyLogBtn);
            this.PanelLog.Controls.Add(this.lockBrowserChecbox);
            this.PanelLog.Controls.Add(this.totalRemainLabel);
            this.PanelLog.Controls.Add(this.label19);
            this.PanelLog.Controls.Add(this.totalUploadedLabel);
            this.PanelLog.Controls.Add(this.label17);
            this.PanelLog.Controls.Add(this.totalItemLabel);
            this.PanelLog.Controls.Add(this.label15);
            this.PanelLog.Controls.Add(this.Logger);
            this.PanelLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelLog.Location = new System.Drawing.Point(0, 604);
            this.PanelLog.Margin = new System.Windows.Forms.Padding(0);
            this.PanelLog.Name = "PanelLog";
            this.PanelLog.Size = new System.Drawing.Size(1352, 127);
            this.PanelLog.TabIndex = 15;
            this.PanelLog.Visible = false;
            // 
            // copyLogBtn
            // 
            this.copyLogBtn.BackColor = System.Drawing.Color.DimGray;
            this.copyLogBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.copyLogBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyLogBtn.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.copyLogBtn.Location = new System.Drawing.Point(6, 98);
            this.copyLogBtn.Name = "copyLogBtn";
            this.copyLogBtn.Size = new System.Drawing.Size(66, 25);
            this.copyLogBtn.TabIndex = 37;
            this.copyLogBtn.Text = "copy log";
            this.copyLogBtn.UseVisualStyleBackColor = false;
            this.copyLogBtn.Click += new System.EventHandler(this.CopyLogBtn_Click);
            // 
            // lockBrowserChecbox
            // 
            this.lockBrowserChecbox.AutoSize = true;
            this.lockBrowserChecbox.Location = new System.Drawing.Point(337, 103);
            this.lockBrowserChecbox.Name = "lockBrowserChecbox";
            this.lockBrowserChecbox.Size = new System.Drawing.Size(90, 17);
            this.lockBrowserChecbox.TabIndex = 35;
            this.lockBrowserChecbox.Text = "Lock browser";
            this.lockBrowserChecbox.UseVisualStyleBackColor = true;
            this.lockBrowserChecbox.CheckedChanged += new System.EventHandler(this.LockBrowserChecbox_CheckedChanged);
            // 
            // totalRemainLabel
            // 
            this.totalRemainLabel.AutoSize = true;
            this.totalRemainLabel.BackColor = System.Drawing.Color.Transparent;
            this.totalRemainLabel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalRemainLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.totalRemainLabel.Location = new System.Drawing.Point(305, 103);
            this.totalRemainLabel.Name = "totalRemainLabel";
            this.totalRemainLabel.Size = new System.Drawing.Size(16, 16);
            this.totalRemainLabel.TabIndex = 7;
            this.totalRemainLabel.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(246, 104);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Remaining:";
            // 
            // totalUploadedLabel
            // 
            this.totalUploadedLabel.AutoSize = true;
            this.totalUploadedLabel.BackColor = System.Drawing.Color.Transparent;
            this.totalUploadedLabel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalUploadedLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.totalUploadedLabel.Location = new System.Drawing.Point(204, 103);
            this.totalUploadedLabel.Name = "totalUploadedLabel";
            this.totalUploadedLabel.Size = new System.Drawing.Size(16, 16);
            this.totalUploadedLabel.TabIndex = 5;
            this.totalUploadedLabel.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(147, 104);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Uploaded:";
            // 
            // totalItemLabel
            // 
            this.totalItemLabel.AutoSize = true;
            this.totalItemLabel.BackColor = System.Drawing.Color.Transparent;
            this.totalItemLabel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalItemLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.totalItemLabel.Location = new System.Drawing.Point(118, 103);
            this.totalItemLabel.Name = "totalItemLabel";
            this.totalItemLabel.Size = new System.Drawing.Size(16, 16);
            this.totalItemLabel.TabIndex = 3;
            this.totalItemLabel.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(84, 104);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Total:";
            // 
            // Logger
            // 
            this.Logger.BackColor = System.Drawing.SystemColors.Menu;
            this.Logger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Logger.Location = new System.Drawing.Point(7, 9);
            this.Logger.Name = "Logger";
            this.Logger.Size = new System.Drawing.Size(531, 84);
            this.Logger.TabIndex = 1;
            this.Logger.Text = "";
            // 
            // folderBrowser
            // 
            this.folderBrowser.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "..:Shopee Uploader::..";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.listItemFlow.ResumeLayout(false);
            this.listItemFlow.PerformLayout();
            this.ExportButton.ResumeLayout(false);
            this.ExportButton.PerformLayout();
            this.GroupWorking.ResumeLayout(false);
            this.GroupWorking.PerformLayout();
            this.GroupAction.ResumeLayout(false);
            this.PanelLog.ResumeLayout(false);
            this.PanelLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.GroupBox GroupAction;
        private System.Windows.Forms.Button ButtonStart;
        private System.Windows.Forms.Button ButtonStop;
        private System.Windows.Forms.Button ButtonReset;
        private System.Windows.Forms.GroupBox GroupWorking;
        private System.Windows.Forms.Button browseFile;
        private System.Windows.Forms.TextBox folderPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.Panel PanelLog;
        private System.Windows.Forms.FlowLayoutPanel listItemFlow;
        private System.Windows.Forms.RichTextBox Logger;
        private System.Windows.Forms.Label totalItemLabel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label totalRemainLabel;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label totalUploadedLabel;
        private System.Windows.Forms.Label label17;

        private Button ButtonSave;
        private CheckBox lockBrowserChecbox;
        private Button copyLogBtn;
        private Timer timer;
        private Label countDownLb;
        private Button ButtonSetting;
        private Panel ExportButton;
        private ToolTip toolTipTeescape;
        private Button buttonLogout;
        private Button buttonSetupShopee;
        public TextBox textBoxPrice;
        private Label label1;
    }
}

