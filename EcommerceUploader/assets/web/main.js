var app = angular.module("NCCModule", []);
app.controller("MainCtrl", function($scope) {
    $scope.shopeeData = JSON.parse(winformObj.getDataFromC());
    $scope.supportColors = JSON.parse(winformObj.getColorsDataFromC());
    debugger;
    if ($scope.shopeeData.Colors) {
        for (var i = 0; i < $scope.supportColors.Colors.length; i++) {
            for (var j = 0; j < $scope.shopeeData.Colors.length; j++) {
                if ($scope.supportColors.Colors[i].Name == $scope.shopeeData.Colors[j].Name) {
                    $scope.supportColors.Colors[i].Selected = true;
                    break;
                }
            }
        }
    }
    $scope.saveChangeShopee = function() {
        var result = winformObj.setDataToC(JSON.stringify($scope.shopeeData));
        console.log("Hello", $scope.shopeeData);

    };
    $scope.changePicture = function($event) {
        console.log($event);
    }

    $scope.addSize = function() {
        if (!$scope.shopeeData.Sizes) {
            $scope.shopeeData.Sizes = [];
        }
        $scope.shopeeData.Sizes.push('');
    }
    $scope.removeSize = function(index) {
        $scope.shopeeData.Sizes.splice(index, 1);
    }
    $scope.removeColor = function(index) {
        $scope.shopeeData.Colors.splice(index, 1);
    }
    $scope.checkChangeColor = function(data) {
        if (!$scope.shopeeData.Colors) {
            $scope.shopeeData.Colors = [];
        }
        var found = false;
        for (var i = 0; i < $scope.shopeeData.Colors.length; i++) {
            if ($scope.shopeeData.Colors[i].Name == data.Name) {
                found = true;
                if (!data.Selected) {
                    $scope.shopeeData.Colors.splice(i, 1);
                }
            }
        }
        if (!found && data.Selected == true) {
            $scope.shopeeData.Colors.push(data);
        }
        // $scope.shopeeData.Colors.push({
        //     'ColorCode': '#ffffff',
        //     'Name': ''
        // });
    }
    $scope.addColorSetting = function() {
        if (!$scope.shopeeData) {
            $scope.shopeeData = { Colors: [], TagFolder: '' };
        } else if (!$scope.shopeeData.Colors) {
            $scope.shopeeData.Colors = []
        }
        $scope.shopeeData.Colors.push({
            'ColorCode': '#ffffff',
            'Name': '',
            'Opacity': 50
        });
    }

    $scope.toUppercase = function(data) {
        data = data.toUppercase();
    }

    $('.selected-items-box').bind('click', function(e) {
        $('.wrapper .list').slideToggle('fast');
    });
});

app.directive("fileread", [function() {
    return {
        scope: {
            fileread: "="
        },
        link: function(scope, element, attributes) {
            element.bind("change", function(changeEvent) {
                var reader = new FileReader();
                reader.onload = function(loadEvent) {
                    scope.$apply(function() {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);

app.directive('capitalize', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            var capitalize = function(inputValue) {
                if (inputValue == undefined) inputValue = '';
                var capitalized = inputValue.toUpperCase();
                if (capitalized !== inputValue) {
                    // see where the cursor is before the update so that we can set it back
                    var selection = element[0].selectionStart;
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                    // set back the cursor after rendering
                    element[0].selectionStart = selection;
                    element[0].selectionEnd = selection;
                }
                return capitalized;
            }
            modelCtrl.$parsers.push(capitalize);
            capitalize(scope[attrs.ngModel]); // capitalize initial value
        }
    };
});