function setProdDesc(desc, brandName, material) {
    var descNodeParent = document.querySelectorAll('.textarea,.description');
    descNodeParent[0].querySelectorAll('.textarea__edit')[0].click();

    function clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }
    var setMeterial = function(meterial) {
        //set chat lieu
        var event = new Event('input', { bubbles: true });
        event.simulated = true;
        var merterialDom = document.querySelectorAll('[placeholder="Tìm kiếm Chất liệu"]');
        if (merterialDom.length <= 0) {
            alert('Không tìm thấy brand, bạn đã setup chất liệu chưa?');
        }
        merterialDom[0].value = meterial;
        merterialDom[0].dispatchEvent(event);
        setTimeout(() => {
            var brandmerterialValue = document.querySelectorAll('.dropdown-input__item, .shopee-dropdown-item');
            brandmerterialValue[0].click();
        }, 1000);

    }
    var setBrand = function(brand) {
        //set brand
        var event = new Event('input', { bubbles: true });
        event.simulated = true;
        var brand = document.querySelectorAll('[placeholder="Tìm kiếm Thương hiệu"]');
        if (brand.length <= 0) {
            alert('Không tìm thấy brand, bạn đã setup brand chưa?');
        }
        brand[0].value = brand;
        brand[0].dispatchEvent(event);
        var brandValue = document.querySelectorAll('.dropdown-input__item, .shopee-dropdown-item');
        brandValue[0].click();
        setTimeout(setMeterial, 2000, clone(material));
    }
    var setDesc = function(text) {
        var event = new Event('input', { bubbles: true });
        event.simulated = true;
        var descNodeParent = document.querySelectorAll('.textarea,.description');
        var textDesc = descNodeParent[0].getElementsByClassName('shopee-input__inner');
        textDesc[0].value = text;
        textDesc[0].dispatchEvent(event);
        setTimeout(setBrand, 1500, clone(brandName));
    }
    setTimeout(setDesc, 1500, clone(desc));
}


function NCProdNameAndCategory(prodName, fullCateGory) {
    var productName = document.querySelectorAll('.product-name-edit .shopee-input__input');
    productName[0].value = prodName;
    let event = new Event('input', { bubbles: true });
    // hack React15
    event.simulated = true;
    productName[0].dispatchEvent(event);

    if (!fullCateGory) {
        alert('Bạn chưa thiết lập category cho áo');
    }

    var NCselectACategory = function(selectedText) {
        var categories = document.getElementsByClassName("category-item");
        for (var idx in categories) {
            if (categories[idx].childNodes && categories[idx].childNodes[0] && categories[idx].childNodes[0].innerText == selectedText) {
                categories[idx].click();
            }
        }
        setTimeout(() => {
            var nextButton = document.querySelectorAll('.shopee-button');
            nextButton[0].click();
        }, 1500);
    }

    function clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }
    var listCateNames = fullCateGory.split('>');
    for (var index = 0; index < listCateNames.length; index++) {
        var selectedText = listCateNames[index].trim();
        setTimeout(NCselectACategory, (index + 1) * 2000, clone(selectedText));
    }
}

function NCSetVariants(labelSize, listSizes, labelColor, listColors, price, quantity, sku) {
    function clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }
    var addButtons = document.querySelectorAll('.shopee-button,.repeater-add');
    if (addButtons && addButtons[0]) {
        addButtons[0].click();
    }
    var initVariantValues = function(index, isVariant2) {
        var event = new Event('input', { bubbles: true });
        event.simulated = true;
        var listItem = isVariant2 ? listColors : listSizes;
        var plusIndexIfVariant2 = !isVariant2 ? index : (listSizes.length + index);
        //Set Ten size tung field
        var variantDetailDoms = document.querySelectorAll('[placeholder*="Nhập phân loại hàng"]');
        if (variantDetailDoms) {
            variantDetailDoms[plusIndexIfVariant2].value = listItem[index];
            variantDetailDoms[plusIndexIfVariant2].dispatchEvent(event);
        }
        if (index == listItem.length - 1 && !isVariant2) {
            //Set variant 2 - color thi index cong don
            var addButtons = document.querySelectorAll('.shopee-button,.repeater-add');
            if (addButtons && addButtons[0]) {
                addButtons[1].click();
            }
            setTimeout(setVariantLabel, 2000, true);
        } else if (index == listItem.length - 1) {
            var priceAllDom = document.querySelectorAll('[placeholder*="Giá"]');
            priceAllDom[0].value = price;
            priceAllDom[0].dispatchEvent(event);

            var stockDom = document.querySelectorAll('[placeholder*="Kho hàng"]');
            stockDom[0].value = quantity;
            stockDom[0].dispatchEvent(event);

            var skuDom = document.querySelectorAll('[placeholder*="SKU phân loại"]');
            skuDom[0].value = sku;
            skuDom[0].dispatchEvent(event);
            setTimeout(() => {
                var applyAllBtn = document.querySelectorAll('.shopee-button--primary');
                applyAllBtn[0].click();
            }, 1700);
        }
    }
    var initVariantInputs = function(isVariant2) {
        //Khoi tao n-lan size rong
        var count = !isVariant2 ? listSizes.length : listColors.length;
        var addMoreIndex = !isVariant2 ? 0 : 1;
        for (var ind = 0; ind < count; ind++) {
            var addButtons = document.querySelectorAll('.shopee-button,.repeater-add');
            if (addButtons && addButtons[addMoreIndex] && ind < count - 1) {
                addButtons[addMoreIndex].click();
            }
            setTimeout(initVariantValues, 1500, clone(ind), clone(isVariant2));
        }
    }
    var setVariantLabel = function(isVariant2) {
        var index = isVariant2 ? 1 : 0;
        var event = new Event('input', { bubbles: true });
        event.simulated = true;
        var nameVariantDom = document.querySelectorAll('[placeholder*="Nhập tên Nhóm phân loại hàng"]');
        nameVariantDom[index].value = !isVariant2 ? labelSize : labelColor;
        nameVariantDom[index].dispatchEvent(event);
        setTimeout(initVariantInputs, 2000, clone(isVariant2));
    }
    setTimeout(setVariantLabel, 2000, false);
}

function setShippingInfo(weight, rong, dai, cao, sku, preoder) {
    var event = new Event('input', { bubbles: true });
    event.simulated = true;
    var containerDom = document.querySelectorAll('.product-shipping');
    var inputs = containerDom[0].getElementsByClassName('shopee-input__input');

    inputs[0].value = weight;
    inputs[0].dispatchEvent(event);
    inputs[1].value = rong;
    inputs[1].dispatchEvent(event);
    inputs[2].value = dai;
    inputs[2].dispatchEvent(event);
    inputs[3].value = cao;
    inputs[3].dispatchEvent(event);
    var thongTinKhac = function() {
        var listInput = document.querySelectorAll('.shopee-input__input');
        //SKU tong
        listInput[listInput.length - 1].value = sku;
        inputs[3].dispatchEvent(event);

        var radioButton = document.querySelectorAll('.shopee-radio');
        if (preoder && preoder.toString() == 'true') {
            radioButton[1].click();
        }
    }
    var setSwitchButton = function() {
        var buttons = document.querySelectorAll('.shopee-switch--close');
        for (var index = 0; index < buttons.length; index++) {
            buttons[index].click();
        }
        thongTinKhac();
    }
    setTimeout(setSwitchButton, 2000);
}