(function(fileObjects) {
    function base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);

        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            var begin = sliceIndex * sliceSize;
            var end = Math.min(begin + sliceSize, bytesLength);

            var bytes = new Array(end - begin);
            for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
    }

    
    var transfer = new DataTransfer();
    transfer.items.add(file2);
    for (var inx = 0; inx < fileObjects.length; inx++) {
        var file = new File([base64toBlob(fileObject.Base64Content, fileObject.FileType)], fileObject.FileName, {
            type: fileObject.FileType
        });
        transfer.items.add(file);
    }
    var uploader = $0;
	var productiamgeEvent = new Event('change', {
        bubbles: true
    });
    productiamgeEvent.simulated = true;
    uploader.files = transfer.files;
    uploader.dispatchEvent(productiamgeEvent);
})(" + fileObjectsJsonString + ")