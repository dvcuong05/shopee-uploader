﻿namespace Main
{
    partial class SettingPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingPanel));
            this.label1 = new System.Windows.Forms.Label();
            this.brandNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.UPCExcelPath = new System.Windows.Forms.TextBox();
            this.brandPictureFolderPath = new System.Windows.Forms.TextBox();
            this.brandPictureBrowserBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.UPCBrowseBtn = new System.Windows.Forms.Button();
            this.shopifyStoreName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.productTimeTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonBrandTagBrowse = new System.Windows.Forms.Button();
            this.textBoxBrandTag = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxW = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxH = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panelGroupSaleChanel = new System.Windows.Forms.Panel();
            this.checkBoxNone = new System.Windows.Forms.CheckBox();
            this.checkBoxAMZ = new System.Windows.Forms.CheckBox();
            this.checkBoxGoogleAds = new System.Windows.Forms.CheckBox();
            this.panelGroupSaleChanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Brand name:";
            // 
            // brandNameTextBox
            // 
            this.brandNameTextBox.Location = new System.Drawing.Point(103, 48);
            this.brandNameTextBox.Name = "brandNameTextBox";
            this.brandNameTextBox.Size = new System.Drawing.Size(160, 20);
            this.brandNameTextBox.TabIndex = 2;
            this.brandNameTextBox.TextChanged += new System.EventHandler(this.brandNameTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Brand\'s pictures:";
            // 
            // UPCExcelPath
            // 
            this.UPCExcelPath.Location = new System.Drawing.Point(103, 138);
            this.UPCExcelPath.Name = "UPCExcelPath";
            this.UPCExcelPath.Size = new System.Drawing.Size(240, 20);
            this.UPCExcelPath.TabIndex = 5;
            this.UPCExcelPath.TextChanged += new System.EventHandler(this.UPCExcelPath_TextChanged);
            // 
            // brandPictureFolderPath
            // 
            this.brandPictureFolderPath.BackColor = System.Drawing.Color.White;
            this.brandPictureFolderPath.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brandPictureFolderPath.Location = new System.Drawing.Point(103, 82);
            this.brandPictureFolderPath.MaxLength = 20000;
            this.brandPictureFolderPath.Name = "brandPictureFolderPath";
            this.brandPictureFolderPath.Size = new System.Drawing.Size(240, 21);
            this.brandPictureFolderPath.TabIndex = 3;
            this.brandPictureFolderPath.TextChanged += new System.EventHandler(this.brandPictureFolderPath_TextChanged);
            // 
            // brandPictureBrowserBtn
            // 
            this.brandPictureBrowserBtn.BackColor = System.Drawing.Color.Transparent;
            this.brandPictureBrowserBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("brandPictureBrowserBtn.BackgroundImage")));
            this.brandPictureBrowserBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.brandPictureBrowserBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.brandPictureBrowserBtn.FlatAppearance.BorderSize = 0;
            this.brandPictureBrowserBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.brandPictureBrowserBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.brandPictureBrowserBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.brandPictureBrowserBtn.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brandPictureBrowserBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.brandPictureBrowserBtn.Location = new System.Drawing.Point(358, 82);
            this.brandPictureBrowserBtn.Name = "brandPictureBrowserBtn";
            this.brandPictureBrowserBtn.Padding = new System.Windows.Forms.Padding(3);
            this.brandPictureBrowserBtn.Size = new System.Drawing.Size(28, 21);
            this.brandPictureBrowserBtn.TabIndex = 85;
            this.brandPictureBrowserBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.brandPictureBrowserBtn.UseVisualStyleBackColor = false;
            this.brandPictureBrowserBtn.Click += new System.EventHandler(this.brandPictureBrowserBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 81;
            this.label3.Text = "Excel UPC code:";
            // 
            // UPCBrowseBtn
            // 
            this.UPCBrowseBtn.BackColor = System.Drawing.Color.Transparent;
            this.UPCBrowseBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("UPCBrowseBtn.BackgroundImage")));
            this.UPCBrowseBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UPCBrowseBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UPCBrowseBtn.FlatAppearance.BorderSize = 0;
            this.UPCBrowseBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.UPCBrowseBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.UPCBrowseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UPCBrowseBtn.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UPCBrowseBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.UPCBrowseBtn.Location = new System.Drawing.Point(358, 134);
            this.UPCBrowseBtn.Name = "UPCBrowseBtn";
            this.UPCBrowseBtn.Padding = new System.Windows.Forms.Padding(3);
            this.UPCBrowseBtn.Size = new System.Drawing.Size(28, 21);
            this.UPCBrowseBtn.TabIndex = 84;
            this.UPCBrowseBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.UPCBrowseBtn.UseVisualStyleBackColor = false;
            this.UPCBrowseBtn.Click += new System.EventHandler(this.UPCBrowseBtn_Click);
            // 
            // shopifyStoreName
            // 
            this.shopifyStoreName.Enabled = false;
            this.shopifyStoreName.Location = new System.Drawing.Point(103, 17);
            this.shopifyStoreName.Name = "shopifyStoreName";
            this.shopifyStoreName.Size = new System.Drawing.Size(160, 20);
            this.shopifyStoreName.TabIndex = 0;
            this.shopifyStoreName.TextChanged += new System.EventHandler(this.shopifyStoreName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 83;
            this.label4.Text = "Shopify storeName";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 86;
            this.label5.Text = "Product time:";
            // 
            // productTimeTxt
            // 
            this.productTimeTxt.Location = new System.Drawing.Point(103, 165);
            this.productTimeTxt.Name = "productTimeTxt";
            this.productTimeTxt.Size = new System.Drawing.Size(53, 20);
            this.productTimeTxt.TabIndex = 87;
            this.productTimeTxt.Text = "4";
            this.productTimeTxt.TextChanged += new System.EventHandler(this.productTimeTxt_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(162, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 88;
            this.label6.Text = "(in days)";
            // 
            // buttonBrandTagBrowse
            // 
            this.buttonBrandTagBrowse.BackColor = System.Drawing.Color.Transparent;
            this.buttonBrandTagBrowse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonBrandTagBrowse.BackgroundImage")));
            this.buttonBrandTagBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBrandTagBrowse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonBrandTagBrowse.FlatAppearance.BorderSize = 0;
            this.buttonBrandTagBrowse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.buttonBrandTagBrowse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.buttonBrandTagBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonBrandTagBrowse.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBrandTagBrowse.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonBrandTagBrowse.Location = new System.Drawing.Point(358, 110);
            this.buttonBrandTagBrowse.Name = "buttonBrandTagBrowse";
            this.buttonBrandTagBrowse.Padding = new System.Windows.Forms.Padding(3);
            this.buttonBrandTagBrowse.Size = new System.Drawing.Size(28, 21);
            this.buttonBrandTagBrowse.TabIndex = 91;
            this.buttonBrandTagBrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonBrandTagBrowse.UseVisualStyleBackColor = false;
            this.buttonBrandTagBrowse.Click += new System.EventHandler(this.buttonBrandTagBrowse_Click);
            // 
            // textBoxBrandTag
            // 
            this.textBoxBrandTag.BackColor = System.Drawing.Color.White;
            this.textBoxBrandTag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBrandTag.Location = new System.Drawing.Point(103, 110);
            this.textBoxBrandTag.MaxLength = 20000;
            this.textBoxBrandTag.Name = "textBoxBrandTag";
            this.textBoxBrandTag.Size = new System.Drawing.Size(240, 21);
            this.textBoxBrandTag.TabIndex = 90;
            this.textBoxBrandTag.TextChanged += new System.EventHandler(this.textBoxBrandTag_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(-2, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 89;
            this.label7.Text = "Brand\'s tag pictures:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(162, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 94;
            this.label8.Text = "x";
            // 
            // textBoxW
            // 
            this.textBoxW.Location = new System.Drawing.Point(103, 192);
            this.textBoxW.Name = "textBoxW";
            this.textBoxW.Size = new System.Drawing.Size(53, 20);
            this.textBoxW.TabIndex = 93;
            this.textBoxW.Text = "1000";
            this.textBoxW.TextChanged += new System.EventHandler(this.textBoxW_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-1, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 92;
            this.label9.Text = "Export mockup size";
            // 
            // textBoxH
            // 
            this.textBoxH.Location = new System.Drawing.Point(180, 191);
            this.textBoxH.Name = "textBoxH";
            this.textBoxH.Size = new System.Drawing.Size(53, 20);
            this.textBoxH.TabIndex = 95;
            this.textBoxH.Text = "0";
            this.textBoxH.TextChanged += new System.EventHandler(this.textBoxH_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.DarkOrange;
            this.label10.Location = new System.Drawing.Point(239, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(227, 13);
            this.label10.TabIndex = 96;
            this.label10.Text = "(* The image file size when generate mockups)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 228);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 97;
            this.label11.Text = "Sale chanel:";
            // 
            // panelGroupSaleChanel
            // 
            this.panelGroupSaleChanel.Controls.Add(this.checkBoxGoogleAds);
            this.panelGroupSaleChanel.Controls.Add(this.checkBoxAMZ);
            this.panelGroupSaleChanel.Controls.Add(this.checkBoxNone);
            this.panelGroupSaleChanel.Location = new System.Drawing.Point(101, 218);
            this.panelGroupSaleChanel.Name = "panelGroupSaleChanel";
            this.panelGroupSaleChanel.Size = new System.Drawing.Size(365, 38);
            this.panelGroupSaleChanel.TabIndex = 98;
            // 
            // checkBoxNone
            // 
            this.checkBoxNone.AutoSize = true;
            this.checkBoxNone.Location = new System.Drawing.Point(8, 10);
            this.checkBoxNone.Name = "checkBoxNone";
            this.checkBoxNone.Size = new System.Drawing.Size(52, 17);
            this.checkBoxNone.TabIndex = 0;
            this.checkBoxNone.Text = "None";
            this.checkBoxNone.UseVisualStyleBackColor = true;
            this.checkBoxNone.CheckedChanged += new System.EventHandler(this.checkBoxNone_CheckedChanged);
            // 
            // checkBoxAMZ
            // 
            this.checkBoxAMZ.AutoSize = true;
            this.checkBoxAMZ.Location = new System.Drawing.Point(83, 10);
            this.checkBoxAMZ.Name = "checkBoxAMZ";
            this.checkBoxAMZ.Size = new System.Drawing.Size(64, 17);
            this.checkBoxAMZ.TabIndex = 1;
            this.checkBoxAMZ.Text = "Amazon";
            this.checkBoxAMZ.UseVisualStyleBackColor = true;
            this.checkBoxAMZ.CheckedChanged += new System.EventHandler(this.checkBoxAMZ_CheckedChanged);
            // 
            // checkBoxGoogleAds
            // 
            this.checkBoxGoogleAds.AutoSize = true;
            this.checkBoxGoogleAds.Location = new System.Drawing.Point(173, 10);
            this.checkBoxGoogleAds.Name = "checkBoxGoogleAds";
            this.checkBoxGoogleAds.Size = new System.Drawing.Size(81, 17);
            this.checkBoxGoogleAds.TabIndex = 2;
            this.checkBoxGoogleAds.Text = "Google Ads";
            this.checkBoxGoogleAds.UseVisualStyleBackColor = true;
            this.checkBoxGoogleAds.CheckedChanged += new System.EventHandler(this.checkBoxGoogleAds_CheckedChanged);
            // 
            // SettingPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 260);
            this.Controls.Add(this.panelGroupSaleChanel);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxH);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxW);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.buttonBrandTagBrowse);
            this.Controls.Add(this.textBoxBrandTag);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.productTimeTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.shopifyStoreName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.UPCBrowseBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.brandPictureBrowserBtn);
            this.Controls.Add(this.brandPictureFolderPath);
            this.Controls.Add(this.UPCExcelPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.brandNameTextBox);
            this.Controls.Add(this.label1);
            this.Name = "SettingPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tool settings";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingPanel_FormClosing);
            this.Load += new System.EventHandler(this.SettingPanel_Load);
            this.panelGroupSaleChanel.ResumeLayout(false);
            this.panelGroupSaleChanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox brandNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox UPCExcelPath;
        private System.Windows.Forms.Button brandPictureBrowserBtn;
        private System.Windows.Forms.TextBox brandPictureFolderPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button UPCBrowseBtn;
        private System.Windows.Forms.TextBox shopifyStoreName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox productTimeTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonBrandTagBrowse;
        private System.Windows.Forms.TextBox textBoxBrandTag;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxW;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxH;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panelGroupSaleChanel;
        private System.Windows.Forms.CheckBox checkBoxGoogleAds;
        private System.Windows.Forms.CheckBox checkBoxAMZ;
        private System.Windows.Forms.CheckBox checkBoxNone;
    }
}