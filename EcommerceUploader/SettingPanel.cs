﻿using Main.classes;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class SettingPanel : Form
    {
        private string storeName;
        private bool isFirstTimeSetW = false;
        private bool isFirstTimeSetH = false;

        public SettingPanel(string storeName)
        {
            this.storeName = storeName;
            InitializeComponent();
            shopifyStoreName.Enabled = true;
        }

        private void brandPictureBrowserBtn_Click(object sender, EventArgs e)
        {
            var dlg1 = new classes.FolderBrowserDialogEx
            {
                Description = "Select a folder for brand pictures:",
                ShowNewFolderButton = true,
                ShowEditBox = true,
                SelectedPath = brandPictureFolderPath.Text,
                ShowFullPathInEditBox = true,
                RootFolder = System.Environment.SpecialFolder.MyComputer
            };

            DialogResult result = dlg1.ShowDialog();
            if (result == DialogResult.OK)
            {
                brandPictureFolderPath.Text = dlg1.SelectedPath;
                ToolUtils.appConfig.BrandPictureFolderPath = brandPictureFolderPath.Text;
            }
        }

        private void buttonBrandTagBrowse_Click(object sender, EventArgs e)
        {
            var dlg1 = new classes.FolderBrowserDialogEx
            {
                Description = "Select a folder for brand tag pictures:",
                ShowNewFolderButton = true,
                ShowEditBox = true,
                SelectedPath = textBoxBrandTag.Text,
                ShowFullPathInEditBox = true,
                RootFolder = System.Environment.SpecialFolder.MyComputer
            };

            DialogResult result = dlg1.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBoxBrandTag.Text = dlg1.SelectedPath;
                ToolUtils.appConfig.TagPath = textBoxBrandTag.Text;
            }
        }

        private void UPCBrowseBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "Excel File|*.xlsx;*.xls";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Console.WriteLine(openFileDialog.FileName);
                    UPCExcelPath.Text = openFileDialog.FileName;
                    ToolUtils.appConfig.UpcCodeFilePath = UPCExcelPath.Text;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void brandNameTextBox_TextChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.BrandName = brandNameTextBox.Text;
        }

        private void shopifyStoreName_TextChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.ShopifyStoreName = shopifyStoreName.Text;
        }

        private void brandPictureFolderPath_TextChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.BrandPictureFolderPath = brandPictureFolderPath.Text;
        }

        private void UPCExcelPath_TextChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.UpcCodeFilePath = UPCExcelPath.Text;
        }

        private void textBoxBrandTag_TextChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.TagPath = textBoxBrandTag.Text;
        }

        private void SettingPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (shopifyStoreName.Text == null || shopifyStoreName.Text == "")
            {
                MessageBox.Show("The shopify store name is empty", "Error");
                e.Cancel = true;
                return;
            }
            if (brandNameTextBox.Text == null || brandNameTextBox.Text == "")
            {
                MessageBox.Show("The brand name is empty", "Error");
                e.Cancel = true;
                return;
            }
            if (UPCExcelPath.Text == null || UPCExcelPath.Text == "")
            {
                MessageBox.Show("The excel file path for UPC code is empty", "Error");
                e.Cancel = true;
                return;
            }
        }

        private void SettingPanel_Load(object sender, EventArgs e)
        {
            try
            {
                //ToolUtils.loadStoreConfig("stre2");
                brandNameTextBox.Text = ToolUtils.appConfig.BrandName;
                brandPictureFolderPath.Text = ToolUtils.appConfig.BrandPictureFolderPath;
                UPCExcelPath.Text = ToolUtils.appConfig.UpcCodeFilePath;
                productTimeTxt.Text = String.IsNullOrWhiteSpace(ToolUtils.appConfig.ProductTime) ? "4" : ToolUtils.appConfig.ProductTime;
                shopifyStoreName.Text = storeName;
                textBoxBrandTag.Text = ToolUtils.appConfig.TagPath;
                textBoxW.Text = ToolUtils.appConfig.ExportWidth != null ? ToolUtils.appConfig.ExportWidth.ToString() : "1000";
                textBoxH.Text = ToolUtils.appConfig.ExportHeight != null ? ToolUtils.appConfig.ExportHeight.ToString() : "0";
                ToolUtils.appConfig.ProductTime = productTimeTxt.Text;
                checkBoxNone.Checked = ToolUtils.appConfig.SkipSaleChanel;
                checkBoxAMZ.Checked = ToolUtils.appConfig.AmzSaleChanel;
                checkBoxGoogleAds.Checked = ToolUtils.appConfig.GoogleAdsSaleChanel;

                this.isFirstTimeSetH = true;
                this.isFirstTimeSetW = true;
            }
            catch (Exception ex)
            {
                ToolUtils.appConfig = new AppConfig();
                Console.WriteLine("Load loadAppConfig is fail:" + ex.Message, "Error");
            }
        }

        private void productTimeTxt_TextChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.ProductTime = productTimeTxt.Text;
        }


        private void textBoxW_TextChanged(object sender, EventArgs e)
        {
            if (this.isFirstTimeSetW)
            {
                this.isFirstTimeSetW = false;
                return;
            }
            try
            {
                ToolUtils.appConfig.ExportWidth = int.Parse(textBoxW.Text);
            }
            catch
            {
                textBoxW.SelectAll();
                MessageBox.Show("The value should be a number");
            }
        }

        private void textBoxH_TextChanged(object sender, EventArgs e)
        {
            if (this.isFirstTimeSetH)
            {
                this.isFirstTimeSetH = false;
                return;
            }
            try
            {
                ToolUtils.appConfig.ExportHeight = int.Parse(textBoxH.Text);
            }
            catch
            {
                MessageBox.Show("The value should be a number");
            }
        }

        private void checkBoxNone_CheckedChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.SkipSaleChanel = checkBoxNone.Checked;
            if (checkBoxNone.Checked)
            {
                ToolUtils.appConfig.AmzSaleChanel = checkBoxAMZ.Checked = false;
                ToolUtils.appConfig.GoogleAdsSaleChanel = checkBoxGoogleAds.Checked = false;
            }
        }

        private void checkBoxAMZ_CheckedChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.AmzSaleChanel = checkBoxAMZ.Checked;
            if (checkBoxAMZ.Checked)
            {
                ToolUtils.appConfig.SkipSaleChanel = checkBoxNone.Checked = false;
                ToolUtils.appConfig.GoogleAdsSaleChanel = checkBoxGoogleAds.Checked = false;
            }
        }

        private void checkBoxGoogleAds_CheckedChanged(object sender, EventArgs e)
        {
            ToolUtils.appConfig.GoogleAdsSaleChanel = checkBoxGoogleAds.Checked;
            if (checkBoxGoogleAds.Checked)
            {
                ToolUtils.appConfig.SkipSaleChanel = checkBoxNone.Checked = false;
                ToolUtils.appConfig.AmzSaleChanel = checkBoxAMZ.Checked = false;
            }
        }
    }
}
