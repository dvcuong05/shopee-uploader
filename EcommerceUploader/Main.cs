﻿namespace Main
{
    using CefSharp;
    using global::Main.classes;
    using global::Main.classes.data;
    using global::Main.Controls;
    using NCCLibs;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class Main : Form
    {
        private Chrome chrome;
        private LoginControl loginControl;
        private ShirtUploader ShirtUploader = null;
        internal Stopwatch watch;// for count upload time
        private ShopeeItemSetting activeItem;
        private int index = 0;
        private String configFileName = "shopeeSetup.txt";
        private AppConfig appConfig = new AppConfig();
        private int totalUploaded = 0;
        private bool isAutoShutdown = false;
        private IDictionary<string, string[]> keywordsMap = new Dictionary<string, string[]>();
        private string baseUrl = "";
        private string loggedMode = "";
        internal List<AppConfig> allStoreConfiged = new List<AppConfig>();
        private List<NCCLibs.ImageItem> currentSelectedMockups = new List<NCCLibs.ImageItem>();

        public Main()
        {
            try
            {
                InitializeComponent();
                ToolTip clearBtnTooltip = new ToolTip();
                clearBtnTooltip.SetToolTip(browseFile, "Chọn folder chứa áo cần upload");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, " Lỗi ");
            }
        }

        private async void Main_Load(object sender, EventArgs e)
        {
            //debug wihout login
            //this.folderPath.Text = @"C:\Users\Cuong\Downloads\Test data\MOCKUP LAYER\CuongTestMockup\Test";
            //this.loggedMode = "logged";
            //StateChanged("logged");
            // END DEBUg

            listItemFlow.MouseWheel += new MouseEventHandler(listItemFlow_MouseWheel);
            Logger.Text = " * **Ready***\n";
            // init chrome
            chrome = new Chrome("", "")
            {
                Dock = DockStyle.Fill
            };
            chrome.WirteLog += WriteLog;
            // add to view
            splitContainer.Panel2.Controls.Add(chrome);
            // init login form
            loginControl = new LoginControl(chrome);
            loginControl.FinishLogin += LoginSucessfully;
            loginControl.modeChanged += GoSetupView;
            // add to view
            splitContainer.Panel1.Controls.Add(loginControl);
            // hide the controls
            ExportButton.Visible = false;
            PanelLog.Visible = false;

            MessageBoxManager.Yes = "Lưu & Đóng";
            MessageBoxManager.No = "Đóng";
            MessageBoxManager.Cancel = "Hủy";
            MessageBoxManager.Register();
            ToolUtils.PARENT_SHOPEE_SETTING = ToolUtils.getParentShopeeSetting();
            ToolUtils.COLOR_SETTING = ToolUtils.getColorSetting();
            ToolUtils.loadTagImages();
            ToolUtils.loadMockupImages();
            loadAppConfig();
            
            try
            {
                String pathStr = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/assets/configs/debuger.txt";
                String isEnableStr = File.ReadAllText(pathStr);
                if (isEnableStr == "true")
                {
                    ToolUtils.DEBUG_MODE = true;
                }
            }
            catch (Exception) { }
        }

        private void LoginSucessfully(string username, string password, string store, string baseUrl)
        {
            loggedMode = "logged";
            this.Text = "..::Shopee - AutoUploader::..   Logged Account: " + ToolUtils.currentAuthentication.ShopeeUser;
            splitContainer.Panel1.Controls.Remove(loginControl);
            StateChanged("logged");
        }

        private void GoSetupView(string loginMode)
        {
            // no login
            loggedMode = loginMode;
            if (loggedMode == "login")
            {
                StateChanged("login");
            }
            else
            {
                splitContainer.Panel1.Controls.Remove(loginControl);
                StateChanged("setup");
            }
        }

        private void UpdateProcessBarValue(int val)
        {
            try
            {
                TeescapeItemPanel panel = (TeescapeItemPanel)listItemFlow.Controls[index - 1];
                panel.updateProgressBar(val);
            }
            catch (Exception ex)
            {
                WriteLog("updateProcessBarValue has error:" + ex.Message);
            }
        }

        private async void ButtonStart_ClickAsync(object sender, EventArgs e)
        {
            ToolUtils.CURRENT_TAG_INDEX = -1;
            ToolUtils.CURRENT_BG_INDEX = -1;
            ShirtUploader = new ShirtUploader(baseUrl, chrome, timer);
            ShirtUploader.FinishUploadAStyle += StartUploadAnItem;
            ShirtUploader.WriteLog += WriteLog;
            ShirtUploader.StartCheckingLoginForm("user", "pass");
            string url = "https://google.com";

            var q = "(function(){var aa=document.querySelector('.buttons');aa.querySelector('.button').click();})()";
            await chrome.browser.EvaluateScriptAsync(q);
            await Task.Delay(2000);
            chrome.browser.Reload();
            //chrome.browser.Load(url);
            await Task.Delay(2000);
            chrome.browser.Reload();
            await Task.Delay(2000);
            chrome.browser.Reload();
            await Task.Delay(5000);
            SelectTheUpLoadMethod();
            SaveConfig(false);
        }

        private void SelectTheUpLoadMethod()
        {
            try
            {
                StateChanged("start");
                WriteLog("****Starting*******");
                StartUploadAFolder();
            }
            catch (Exception ex)
            {
                WriteLog("startUpload has an exception:" + ex.Message);
            }
        }

        private void StartUploadAFolder()
        {
            foreach (TeescapeItemPanel panel in listItemFlow.Controls)
            {
                panel.disable();
            }
            StartUploadAnItem(0);
        }

        private async void StartUploadAnItem(int index)
        {
            try
            {
                timer.Stop();
                if (index > 0)
                {
                    TeescapeItemPanel previousPanel = (TeescapeItemPanel)listItemFlow.Controls[index - 1];
                    if (!previousPanel.isFinished())
                    {
                        previousPanel.uploadSuccessful();
                        UploadSuccess();
                        watch.Stop();
                        long elapsedMs = watch.ElapsedMilliseconds / 1000;
                        WriteLog("Time: " + elapsedMs + "(s)---Uploaded " + activeItem.Name + " successfully!*******************************************\n");
                    }
                }
                if (index == listItemFlow.Controls.Count)
                {
                    FinishAll();
                }
                else
                {
                    TeescapeItemPanel nextPanel = (TeescapeItemPanel)listItemFlow.Controls[index];
                    nextPanel.Visible = true;
                    listItemFlow.ScrollControlIntoView(nextPanel);
                    this.index = index + 1;
                    if (nextPanel.isFinished())
                    {
                        StartUploadAnItem(this.index);
                    }
                    else
                    {
                        if (index > 0)
                        {
                            try
                            {
                                // int delay = Int32.Parse(this.delay.Text) * 1000;
                                int delay = 0;
                                WriteLog("Delay : " + delay);
                                await Task.Delay(delay);
                            }
                            catch (Exception ex)
                            {
                                WriteLog("uploadAnItem() set Delay has error:" + ex.Message);
                            }
                        }
                        watch = Stopwatch.StartNew();
                        activeItem = nextPanel.getObjWithMergedDataFromParent();
                        if(ToolUtils.isEmpty(activeItem.Name) || activeItem.Name.Length < 10)
                        {
                            MessageBox.Show("Tên áo không đảm bảo trong khoảng lớn hơn 10 và bé hơn 120");
                            this.stopApplication();
                            return;
                        }
                        if (ToolUtils.isEmpty(activeItem.Sku))
                        {
                            MessageBox.Show("Áo đang úp chưa set mã SKU");
                            this.stopApplication();
                            return;
                        }
                        if (ToolUtils.isEmpty(activeItem.Price))
                        {
                            MessageBox.Show("Áo đang úp chưa set giá.");
                            this.stopApplication();
                            return;
                        }
                        if (activeItem.ShopeeSetting == null)
                        {
                            MessageBox.Show("Chưa setup thông tin đăng áo. Example: Description, giá, Thương Hiệu,...");
                            this.stopApplication();
                            return;
                        }
                        if (ToolUtils.isEmpty(activeItem.ShopeeSetting.Description) || activeItem.ShopeeSetting.Description.Length <=100 || activeItem.ShopeeSetting.Description.Length > 3000)
                        {
                            MessageBox.Show("Áo đang úp chưa set mô tả hoặc không đảm bảo trong khoảng lớn hơn 100 và bé hơn 3000");
                            this.stopApplication();
                            return;
                        }
                        if (ToolUtils.isEmpty(activeItem.ShopeeSetting.Category))
                        {
                            MessageBox.Show("Áo đang úp chưa category");
                            this.stopApplication();
                            return;
                        }
                        nextPanel.isUploading();
                        {
                            ShirtUploader.Upload(activeItem, index, ToolUtils.PARENT_SHOPEE_SETTING);
                        };
                        WriteLog("Uploading: " + activeItem.Name);
                        if (this.index <= 2)
                        {
                            await Task.Delay(5000);// ngu 5s cho ao dau tien, make sure setup.txt is uploaded
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("uploadAnItem has an exception: " + ex.Message);
            }
        }

        private void stopApplication()
        {
            StopBtn_Click(null, null);
        }

        private void WriteLog(string message)
        {
            try
            {

                string result = "";
                string[] lines = Logger.Text.Split(new[] { '\r', '\n' });
                if (lines.Count() > 200)
                {
                    for (int i = 0; i <= 200; i++)
                    {
                        result = result + lines[i] + "\n";
                    }
                }
                else
                {
                    result = Logger.Text;
                }
                Logger.Text = result.Insert(0, message + "\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine("writeLog method has error:" + ex.Message);
            }
        }

        private void FinishAll()
        {
            try
            {
                StateChanged("stop");
                if (ShirtUploader != null)
                {
                    ShirtUploader.StopApp();
                }
                foreach (TeescapeItemPanel panel in listItemFlow.Controls)
                {
                    if (!panel.isFinished())
                    {
                        panel.enable();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("finish() has error:" + ex.Message);
            }
        }

        private void BrowseFile_Click(object sender, EventArgs e)
        {
            var dlg1 = new FolderBrowserDialogEx
            {
                Description = "Select a folder for working:",
                ShowNewFolderButton = true,
                ShowEditBox = true,
                SelectedPath = folderPath.Text,
                ShowFullPathInEditBox = true,
                RootFolder = System.Environment.SpecialFolder.MyComputer
            };

            DialogResult result = dlg1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderPath.Text = dlg1.SelectedPath;
            }
        }

        private void LinkFolder_TextChanged(object sender, EventArgs e)
        {
            totalUploaded = 0;
            totalItemLabel.Text = "0";
            listItemFlow.Controls.Clear();
            ProcessDirectory(folderPath.Text);
        }

        public void ProcessDirectory(string targetDirectory)
        {
            try
            {
                int count = 1;
                string[] fileEntries = Directory.GetFiles(targetDirectory);
                Boolean hasAnyPicture = false;
                foreach (string filePath in fileEntries)
                {
                    String extension = Path.GetExtension(filePath).ToLower();
                    if (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".eps" || extension == "gif")
                    {
                        string name = Path.GetFileName(filePath);
                        if (!name.Contains("#"))
                        {
                            ProcessFile(filePath, count++);
                            hasAnyPicture = true;
                        }

                    }
                }
                totalItemLabel.Text = (count - 1).ToString();
                if (!hasAnyPicture)
                {
                    folderPath.Text = "";
                    MessageBox.Show("There have no image to upload in your selected folder!.", "..::Ố ồo:..", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    ReloadLastConfig(targetDirectory);
                    listItemFlow.BackgroundImage = null;
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Has error with your folder path." + ee.Message);
            }
        }

        private void ReloadLastConfig(string folderPath)
        {
            //Reload last config
            WriteLog("Load config..");
            try
            {
                String filePath = folderPath + "/" + configFileName;
                String fileContent = File.ReadAllText(filePath);
                ShopeeSetup shopeeSetupData = new ShopeeSetup();
                shopeeSetupData = JsonConvert.DeserializeObject<ShopeeSetup>(fileContent);
                ToolUtils.parentDescription = shopeeSetupData.Description;
                textBoxPrice.Text = shopeeSetupData.ParentPrice;
                foreach (ShopeeItemSetting objectItem in shopeeSetupData.ShopeeItemSettings)
                {
                    Control[] foundControls = listItemFlow.Controls.Find(objectItem.ImagePath, false);
                    if (foundControls.Length > 0)
                    {
                        TeescapeItemPanel foundPanel = (TeescapeItemPanel)foundControls[0];
                        foundPanel.setShopeeItemSettingData(objectItem);
                    }
                    //Count finished item
                    if (objectItem.IsFinished)
                    {
                        //chrome.browser.Invoke((MethodInvoker)delegate
                        // {
                        try
                        {
                            UploadSuccess();
                        }
                        catch (Exception ex)
                        {
                            WriteLog("Coudl not update total count of files." + ex.Message);
                        }
                        // });
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog("getIOKeywords() has error:" + ex.Message);
            }
        }

        public void ProcessFile(string path, int counter)
        {
            TeescapeItemPanel us1 = new TeescapeItemPanel(path, counter, chrome);
            us1.RemoveClicked += RemovePanel;
            us1.Visible = counter <= 10 ? true : false;
            listItemFlow.Controls.Add(us1);
            WriteLog("Adding - " + path);
        }

        private void ResetBtn_Click(object sender, EventArgs e)
        {
            totalUploaded = 0;
            totalUploadedLabel.Text = "0";
            totalRemainLabel.Text = "0";
            if (String.IsNullOrEmpty(folderPath.Text))
            {
                MessageBox.Show("Bạn chưa chọn folder chứa design");
                return;
            }
            MessageBoxManager.Yes = "Tất cả";
            MessageBoxManager.No = "Chỉ Status";
            bool isResetEverything = true;
            DialogResult val = MessageBox.Show("Bạn muốn reset theo tùy chọn nào ?", "Xác nhận", MessageBoxButtons.YesNoCancel);
            if (val == DialogResult.No)
            {
                isResetEverything = false;
            }
            else if (val == DialogResult.Cancel)
            {
                Console.WriteLine("vncud cancel button is clicked");
                return;
            }
            foreach (TeescapeItemPanel panel in listItemFlow.Controls)
            {
                //this.listItemFlow.ScrollControlIntoView(panel);
                panel.reset(isResetEverything);
            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if (folderPath.Text == "" || listItemFlow.Controls.Count == 0)
            {
                MessageBox.Show("Không có gì để lưu! Vui lòng chọn folder chứa áo để làm việc trước khi lưu.", "..::Information:..", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SaveConfig(true);
        }

        private void SaveConfig(bool showMessage)
        {
            try
            {
                string json = GetConfigData();
                String filePath = folderPath.Text + "/" + configFileName;
                File.WriteAllText(filePath, json);
                if (showMessage)
                {
                    MessageBox.Show("Lưu thành công!", "..::Information:..", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteLog("Save setup is failed:" + ex.Message);
            }
        }

        private string GetConfigData()
        {
            ShopeeSetup teescapeConfigObj = new ShopeeSetup();
            teescapeConfigObj.FolderPath = folderPath.Text;
            teescapeConfigObj.Description = ToolUtils.parentDescription;
            teescapeConfigObj.ParentPrice = textBoxPrice.Text;
            List<ShopeeItemSetting> listOfTeescapeObj = new List<ShopeeItemSetting>();
            foreach (TeescapeItemPanel panel in listItemFlow.Controls)
            {
                listOfTeescapeObj.Add(panel.getShopeeItemSetting());
            }
            teescapeConfigObj.ShopeeItemSettings = listOfTeescapeObj;

            return JsonConvert.SerializeObject(teescapeConfigObj);
        }

        public void RemovePanel(TeescapeItemPanel item)
        {

            listItemFlow.Controls.Remove(item);
            item.Dispose();

            int count = 0;
            foreach (TeescapeItemPanel panel in listItemFlow.Controls)
            {
                count++;
                panel.updateIndex(count);
            }
            totalItemLabel.Text = listItemFlow.Controls.Count.ToString();

            if (listItemFlow.Controls.Count == 0)
            {
                folderPath.Text = "";
            }
        }

        public void UploadSuccess()
        {
            totalUploaded++;
            totalUploadedLabel.Text = totalUploaded.ToString();
            totalRemainLabel.Text = (int.Parse(totalItemLabel.Text) - totalUploaded).ToString();
            SaveConfig(false);
        }

        private void StopBtn_Click(object sender, EventArgs e)
        {
            try
            {
                chrome.browser.Stop();
                if (ShirtUploader != null)
                {
                    ShirtUploader.StopApp();
                }
                StateChanged("stop");
                foreach (TeescapeItemPanel panel in listItemFlow.Controls)
                {
                    if (!panel.isFinished())
                    {
                        panel.stop();
                    }
                }
            }
            catch (Exception ee)
            {
                WriteLog("Stop application have an exception:" + ee.Message);
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (folderPath.Text == "" || folderPath.Text == null || isAutoShutdown)
            {
                return;
            }

            MessageBoxManager.Yes = "Lưu & Đóng";
            MessageBoxManager.No = "Đóng";
            DialogResult val = MessageBox.Show("Nhớ lưu các thay đổi trước khi thoát. Không thì cứ thoát thôi!", "Thoát ứng dụng", MessageBoxButtons.YesNoCancel);
            if (val == DialogResult.Yes)
            {
                SaveConfig(false);
            }
            else if (val == DialogResult.No)
            {

            }
            else
            {
                e.Cancel = true;
                Activate();
            }
        }

        private void StylesBtn_Click(object sender, EventArgs e)
        {

            if (!String.IsNullOrEmpty(folderPath.Text))
            {
                TeescapeItemPanel item = (TeescapeItemPanel)listItemFlow.Controls[0];
                //TeescapeSelection selection = new TeescapeSelection(null, parentStyles, true, item.getShopeeItemSetting().TmpFullPath);
                //selection.saveClicked += SaveParentStyles;
                //selection.ShowDialog();
            }
            else
            {
                MessageBox.Show("Vui lòng chọn folder chứa áo để làm việc trước khi dùng mục này.");
            }
        }

        private void SaveParentStyles(List<TeescapeStyle> list, bool isSingleUpload)
        {
            //parentStyles = list;
           // ToolUtils.stylesOfParent = list;
        }

        private void LockBrowserChecbox_CheckedChanged(object sender, EventArgs e)
        {
            splitContainer.Panel2.Enabled = !lockBrowserChecbox.Checked;
        }

        private void CopyLogBtn_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(Logger.Text);
        }

        private async void StartNewUpdaload()
        {
            await Task.Delay(10000);
            SelectTheUpLoadMethod();
        }

        private void StateChanged(string stateName)
        {
            switch (stateName)
            {
                case "logout":
                    PanelLog.Visible = false;
                    ExportButton.Visible = false;
                    listItemFlow.Visible = false;
                    splitContainer.Panel2Collapsed = true;
                    break;
                case "login":
                    splitContainer.Panel2Collapsed = false;
                    break;
                case "logged":
                    ExportButton.Visible = true;
                    PanelLog.Visible = true;
                    listItemFlow.Visible = true;
                    //ButtonLogout.Text = "Logout";
                    ButtonStart.Enabled = true;
                    ButtonStop.Enabled = false;
                    splitContainer.Panel2Collapsed = false;
                    break;
                case "setup":
                    ExportButton.Visible = true;
                    listItemFlow.Visible = true;
                    PanelLog.Visible = false;
                    //ButtonLogout.Text = "Back Login";
                    ButtonStart.Enabled = false;
                    ButtonStop.Enabled = false;
                    splitContainer.Panel2Collapsed = false;
                    break;
                case "stop":
                    GroupWorking.Enabled = true;
                    GroupAction.Enabled = true;
                    ButtonStop.Enabled = true;
                    break;
                case "start":
                    GroupWorking.Enabled = false;
                    //GroupAction.Enabled = false;
                    ButtonStop.Enabled = true;
                    break;
                default: break;
            }
        }

        private async Task loadAppConfig()
        {
            await Task.Run(() =>
            {
                try
                {
                    string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\assets\\configs\\AppConfig.txt";
                    string fileContent = File.ReadAllText(filePath);
                    appConfig = JsonConvert.DeserializeObject<AppConfig>(fileContent);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Load loadAppConfig is fail:", ex.Message);
                }
            });
        }

        private void saveAppConfig()
        {
            try
            {
                String filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\assets\\configs\\AppConfig.txt";
                String newConfigString = JsonConvert.SerializeObject(appConfig);
                File.WriteAllText(filePath, newConfigString);
            }
            catch (Exception ex)
            {
                Console.WriteLine("saveAppConfig is fail:", ex.Message);
            }
        }

        private void listItemFlow_MouseWheel(object sender, MouseEventArgs e)
        {

            if ((listItemFlow.VerticalScroll.Maximum - listItemFlow.VerticalScroll.LargeChange + 1) == listItemFlow.VerticalScroll.Value)
            {
                displayNextPageItems();
            }
        }

        private int displayNextPageItems()
        {
            int rs = 0;
            for (var index = 0; index < listItemFlow.Controls.Count; index++)
            {
                if (listItemFlow.Controls[index].Visible)
                {
                    rs++;
                }
            }
            rs = rs - 1;
            int totalNextPage = 2;
            while (rs < listItemFlow.Controls.Count && totalNextPage > 0)
            {
                listItemFlow.Controls[rs].Visible = true;
                rs++;
                totalNextPage--;
            }
            return rs;
        }

        private void listItemFlow_SizeChanged(object sender, EventArgs e)
        {
            int totalCanDisplay = (listItemFlow.Size.Height / 200) + 1;
            for (var index = 0; index < listItemFlow.Controls.Count; index++)
            {
                listItemFlow.Controls[index].Visible = index <= totalCanDisplay;
            }
        }

        private void listItemFlow_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.NewValue == listItemFlow.VerticalScroll.Maximum - listItemFlow.VerticalScroll.LargeChange + 1)
            {
                if (e.NewValue != e.OldValue)
                {
                    displayNextPageItems();
                }
            }
        }

        private void saveParentShopeeSetting(string settingData)
        {
            if(settingData == null)
            {
                return;
            }
            ShopeeSetting savedData = JsonConvert.DeserializeObject<ShopeeSetting>(settingData);
            if (savedData != null)
            {
                ToolUtils.PARENT_SHOPEE_SETTING = savedData;
                ToolUtils.saveParentShopeeSetting(savedData);
            }
        }

        private void ButtonSetting_Click(object sender, EventArgs e)
        {
            CustomSelectionEditorForm editForm = new CustomSelectionEditorForm(chrome, "tayngan");
            editForm.closeFormHandler += DrawSelectedMockup;
            editForm.ShowDialog();
        }

        private void DrawSelectedMockup(List<ImageItem> selectedMockups, string styleId)
        {
            //
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            if (loggedMode == "setup")
            {
                StateChanged("logout");
                this.chrome.browser.EvaluateScriptAsync("(function(){var accountDom=document.querySelectorAll('.user-info-box');if(accountDom&&accountDom.length>0){var spans=accountDom[0].querySelectorAll('span');spans[spans.length-1].click();}})()");
                splitContainer.Panel1.Controls.Add(loginControl);
            }
            else
            {
                MessageBoxManager.Yes = "Logout";
                MessageBoxManager.No = "No";
                DialogResult result = MessageBox.Show("Do you want to logout?", "Confirm", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    chrome.LogoutHandler += ProcessLogout;
                    chrome.LogoutOnBrowser();
                }
            }
        }
        private void ProcessLogout()
        {
            StateChanged("logout");
            splitContainer.Panel1.Controls.Add(loginControl);
        }

        private void buttonSetupShopee_Click(object sender, EventArgs e)
        {
            ChromeItem chromeItem = new ChromeItem(chrome, ToolUtils.PARENT_SHOPEE_SETTING, ToolUtils.COLOR_SETTING, "settingupload");
            chromeItem.formCloseHandler += saveParentShopeeSetting;
            chromeItem.ShowDialog();
        }

        private void textBoxPrice_TextChanged(object sender, EventArgs e)
        {
            ToolUtils.PARENT_PRICE = textBoxPrice.Text;
        }
    }
}
