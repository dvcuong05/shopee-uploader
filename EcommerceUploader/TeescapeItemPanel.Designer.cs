﻿using System.Windows.Forms;
namespace Main
{
    partial class TeescapeItemPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TeescapeItemPanel));
            this.teeLink = new System.Windows.Forms.Label();
            this.teeName = new System.Windows.Forms.Label();
            this.removeBtn = new System.Windows.Forms.Button();
            this.imagePathTxt = new System.Windows.Forms.TextBox();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.progressBarMain = new System.Windows.Forms.ProgressBar();
            this.indexLabel = new System.Windows.Forms.Label();
            this.resetBtn = new System.Windows.Forms.Button();
            this.stylesBtn = new System.Windows.Forms.Button();
            this.descriptionlabel = new System.Windows.Forms.Label();
            this.textBoxSKU = new System.Windows.Forms.TextBox();
            this.linkLabelShowImg = new System.Windows.Forms.LinkLabel();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // teeLink
            // 
            this.teeLink.AutoSize = true;
            this.teeLink.Location = new System.Drawing.Point(5, 18);
            this.teeLink.Name = "teeLink";
            this.teeLink.Size = new System.Drawing.Size(63, 13);
            this.teeLink.TabIndex = 20;
            this.teeLink.Text = "Image path:";
            // 
            // teeName
            // 
            this.teeName.AutoSize = true;
            this.teeName.Location = new System.Drawing.Point(6, 41);
            this.teeName.Name = "teeName";
            this.teeName.Size = new System.Drawing.Size(43, 13);
            this.teeName.TabIndex = 21;
            this.teeName.Text = "Tên SP";
            // 
            // removeBtn
            // 
            this.removeBtn.BackColor = System.Drawing.Color.Transparent;
            this.removeBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.removeBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.removeBtn.FlatAppearance.BorderSize = 0;
            this.removeBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.removeBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.removeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeBtn.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeBtn.ForeColor = System.Drawing.Color.DarkRed;
            this.removeBtn.Image = ((System.Drawing.Image)(resources.GetObject("removeBtn.Image")));
            this.removeBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.removeBtn.Location = new System.Drawing.Point(322, 37);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(27, 20);
            this.removeBtn.TabIndex = 4;
            this.removeBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.removeBtn.UseVisualStyleBackColor = false;
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // imagePathTxt
            // 
            this.imagePathTxt.Location = new System.Drawing.Point(71, 14);
            this.imagePathTxt.Name = "imagePathTxt";
            this.imagePathTxt.ReadOnly = true;
            this.imagePathTxt.Size = new System.Drawing.Size(245, 20);
            this.imagePathTxt.TabIndex = 0;
            // 
            // nameTxt
            // 
            this.nameTxt.Location = new System.Drawing.Point(71, 37);
            this.nameTxt.MaxLength = 150;
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(245, 20);
            this.nameTxt.TabIndex = 1;
            // 
            // progressBarMain
            // 
            this.progressBarMain.Location = new System.Drawing.Point(6, 94);
            this.progressBarMain.MarqueeAnimationSpeed = 60;
            this.progressBarMain.Name = "progressBarMain";
            this.progressBarMain.Size = new System.Drawing.Size(413, 18);
            this.progressBarMain.TabIndex = 26;
            // 
            // indexLabel
            // 
            this.indexLabel.AutoSize = true;
            this.indexLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.indexLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.indexLabel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.indexLabel.ForeColor = System.Drawing.Color.DeepPink;
            this.indexLabel.Location = new System.Drawing.Point(0, 0);
            this.indexLabel.Name = "indexLabel";
            this.indexLabel.Size = new System.Drawing.Size(16, 16);
            this.indexLabel.TabIndex = 32;
            this.indexLabel.Text = "0";
            // 
            // resetBtn
            // 
            this.resetBtn.BackColor = System.Drawing.Color.Transparent;
            this.resetBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.resetBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.resetBtn.FlatAppearance.BorderSize = 0;
            this.resetBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.resetBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.resetBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetBtn.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetBtn.ForeColor = System.Drawing.Color.DarkGreen;
            this.resetBtn.Image = ((System.Drawing.Image)(resources.GetObject("resetBtn.Image")));
            this.resetBtn.Location = new System.Drawing.Point(322, 14);
            this.resetBtn.Name = "resetBtn";
            this.resetBtn.Size = new System.Drawing.Size(27, 20);
            this.resetBtn.TabIndex = 3;
            this.resetBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.resetBtn.UseVisualStyleBackColor = false;
            this.resetBtn.Click += new System.EventHandler(this.resetBtn_Click);
            // 
            // stylesBtn
            // 
            this.stylesBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(7)))));
            this.stylesBtn.BackgroundImage = global::Main.Properties.Resources.Gear_icon;
            this.stylesBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.stylesBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.stylesBtn.FlatAppearance.BorderSize = 0;
            this.stylesBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.stylesBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.stylesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.stylesBtn.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stylesBtn.ForeColor = System.Drawing.Color.DarkGreen;
            this.stylesBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.stylesBtn.Location = new System.Drawing.Point(322, 63);
            this.stylesBtn.Name = "stylesBtn";
            this.stylesBtn.Size = new System.Drawing.Size(27, 20);
            this.stylesBtn.TabIndex = 6;
            this.stylesBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.stylesBtn.UseVisualStyleBackColor = false;
            this.stylesBtn.Click += new System.EventHandler(this.stylesBtn_Click);
            // 
            // descriptionlabel
            // 
            this.descriptionlabel.AutoSize = true;
            this.descriptionlabel.Location = new System.Drawing.Point(6, 66);
            this.descriptionlabel.Name = "descriptionlabel";
            this.descriptionlabel.Size = new System.Drawing.Size(29, 13);
            this.descriptionlabel.TabIndex = 22;
            this.descriptionlabel.Text = "SKU";
            // 
            // textBoxSKU
            // 
            this.textBoxSKU.Location = new System.Drawing.Point(71, 62);
            this.textBoxSKU.MaxLength = 150;
            this.textBoxSKU.Name = "textBoxSKU";
            this.textBoxSKU.Size = new System.Drawing.Size(73, 20);
            this.textBoxSKU.TabIndex = 71;
            // 
            // linkLabelShowImg
            // 
            this.linkLabelShowImg.AutoSize = true;
            this.linkLabelShowImg.Location = new System.Drawing.Point(355, 20);
            this.linkLabelShowImg.Name = "linkLabelShowImg";
            this.linkLabelShowImg.Size = new System.Drawing.Size(55, 13);
            this.linkLabelShowImg.TabIndex = 72;
            this.linkLabelShowImg.TabStop = true;
            this.linkLabelShowImg.Text = "Xem trước";
            this.linkLabelShowImg.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelShowImg_LinkClicked);
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(228, 62);
            this.textBoxPrice.MaxLength = 150;
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(88, 20);
            this.textBoxPrice.TabIndex = 74;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(183, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 73;
            this.label1.Text = "Giá bán";
            // 
            // TeescapeItemPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.textBoxPrice);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.linkLabelShowImg);
            this.Controls.Add(this.textBoxSKU);
            this.Controls.Add(this.nameTxt);
            this.Controls.Add(this.stylesBtn);
            this.Controls.Add(this.resetBtn);
            this.Controls.Add(this.indexLabel);
            this.Controls.Add(this.progressBarMain);
            this.Controls.Add(this.imagePathTxt);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.descriptionlabel);
            this.Controls.Add(this.teeName);
            this.Controls.Add(this.teeLink);
            this.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.Name = "TeescapeItemPanel";
            this.Size = new System.Drawing.Size(423, 117);
            this.Load += new System.EventHandler(this.TeespringItemPanel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Label teeLink;
        private System.Windows.Forms.Label teeName;
        private System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.TextBox imagePathTxt;
        public System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.ProgressBar progressBarMain;
        private System.Windows.Forms.Label indexLabel;
        private System.Windows.Forms.Button resetBtn;
        private System.Windows.Forms.Button stylesBtn;
        private Label descriptionlabel;
        public TextBox textBoxSKU;
        private LinkLabel linkLabelShowImg;
        public TextBox textBoxPrice;
        private Label label1;
    }
}
