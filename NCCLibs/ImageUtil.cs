﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace NCCLibs
{
    public class ImageUtil
    {
        public static Bitmap Resize(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                //graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static Point convertToPx(Image img, Int32 w, Int32 h, Int32 x, Int32 y)
        {
            float stretch_X = img.Width / (float)w;
            float stretch_Y = img.Height / (float)h;

            int pX = (int)(x * stretch_X);
            int pY = (int)(y * stretch_Y);

            return new Point(pX, pY);
        }

        public static Bitmap merged(int resizedW, int resizedH, int previewW, string htmlBgCode, ImageItem background, ImageItem effect,
            ImageItem design, ImageItem tag)
        {
            FileStream fsMain = new FileStream(background.FullPath, FileMode.Open, FileAccess.Read);
            Image baseImg = Image.FromStream(fsMain);
            Bitmap finalImg = CreateFinalImage(baseImg, resizedW, resizedH);
            finalImg.SetResolution(baseImg.HorizontalResolution, baseImg.VerticalResolution);
            var graphics = Graphics.FromImage(finalImg);
            graphics.CompositingMode = CompositingMode.SourceOver;
            //graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            
            if (!string.IsNullOrEmpty(htmlBgCode))
            {
                Color c = System.Drawing.ColorTranslator.FromHtml(htmlBgCode);
                graphics.Clear(c);
            }

            
            if (design != null)
            {
                AddSticker(graphics, design, null, finalImg, previewW, false);
            }
            if (effect != null)
            {
                DrawImage(graphics, effect, resizedW, resizedH);
            }
            if (background != null)
            {
                DrawImage(graphics, background, resizedW, resizedH);
            }
            if (tag != null)
            {
                AddSticker(graphics, tag, null, finalImg, previewW, true);
            }

            graphics.Dispose();
            graphics = null;
            return finalImg;
        }

        public static void DrawImage(Graphics g, ImageItem srcImg, int w, int h)
        {
            if (!string.IsNullOrEmpty(srcImg.FullPath))
            {
                FileStream fs = new FileStream(@srcImg.FullPath, FileMode.Open, FileAccess.Read);
                Image img = Image.FromStream(fs);
                int[] s = correctSize(img, w, h);
                if (s[0] != 0 && s[1] != 0)
                {
                    Image cloned = (Image)img.Clone();
                    img.Dispose();
                    img = Resize(cloned, s[0], s[1]);
                    cloned.Dispose();
                    cloned = null;
                }
                if(srcImg.Angle != 0)
                {
                    Image cloned = (Image)img.Clone();
                    img.Dispose();
                    img = RotateBitmap((Bitmap)cloned, srcImg.Angle);
                    cloned.Dispose();
                    cloned = null;
                }
                if(srcImg.Opacity < 100)
                {
                    Image cloned = (Image)img.Clone();
                    img.Dispose();
                    float opacityVal = (float)srcImg.Opacity / 100;
                    img = ChangeOpacity(cloned, opacityVal);
                    cloned.Dispose();
                    cloned = null;
                }
                g.DrawImage(img, 0, 0);
                fs.Dispose();
                fs = null;
                img.Dispose();
                img = null;
            }
        }

        public static void AddSticker(Graphics g, ImageItem sticker, Image sourceImg, Image finalImg, int previewW, bool isXYScale)
        {
            if ((sticker != null && !string.IsNullOrEmpty(sticker.FullPath) || sourceImg != null) 
                && sticker.Rect != null && sticker.Rect.X != 0 && sticker.Rect.Y != 0)
            {
                FileStream fs = null;
                Image img;
                if (sourceImg == null)
                {
                    fs = new FileStream(@sticker.FullPath, FileMode.Open, FileAccess.Read);
                    img = Image.FromStream(fs);
                    fs.Dispose();
                    fs = null;
                }
                else
                {
                    img = (Image)sourceImg.Clone();
                }
                // int[] s = correctSize(img, previewW, previewW);
                // int containerW = s[0], containerH = s[1];
                Rectangle rect = sticker.Rect;
                Point rectStartPx = convertToPx(finalImg, previewW, previewW, rect.X, rect.Y);
                Point rectEndPx = convertToPx(finalImg, previewW, previewW, rect.X + rect.Width, rect.Y + rect.Height);

                float newStickerW = rectEndPx.X - rectStartPx.X;
                float newStickerH = rectEndPx.Y - rectStartPx.Y;
                if (isXYScale)
                {
                    newStickerH = newStickerW / img.Width * img.Height;
                }
                
                Image resizedImg = Resize(img, (int)newStickerW, (int)newStickerH);
                if (sticker != null)
                {
                    if (sticker.Angle != 0)
                    {
                        Image cloned = (Image)resizedImg.Clone();
                        resizedImg.Dispose();
                        resizedImg = RotateBitmap((Bitmap)cloned, sticker.Angle);
                        cloned.Dispose();
                        cloned = null;
                    }
                    if (sticker.Opacity < 100)
                    {
                        Image cloned = (Image)resizedImg.Clone();
                        resizedImg.Dispose();
                        float opacityVal = (float)sticker.Opacity / 100;
                        resizedImg = ChangeOpacity(cloned, opacityVal);
                        cloned.Dispose();
                        cloned = null;
                    }
                }
                g.DrawImage(resizedImg, rectStartPx.X, rectStartPx.Y, newStickerW, newStickerH);
                img.Dispose();
                img = null;
                resizedImg.Dispose();
                resizedImg = null;
            }
        }

        public static Bitmap CreateFinalImage(Image baseImg, int w, int h)
        {
            int[] s = correctSize(baseImg, w, h);
            if (s[0] != 0 && s[1] != 0)
            {
                return new Bitmap(s[0], s[1], PixelFormat.Format32bppArgb);
            }
            else
            {
                return new Bitmap(baseImg.Width, baseImg.Height, PixelFormat.Format32bppArgb);
            }
        }

        private static int[] correctSize(Image baseImg, int w, int h)
        {
            float resizedW = 0, resizeH = 0;
            if (w != 0 && h != 0)
            {
                resizedW = w;
                resizeH = h;
            }
            else if (w != 0)
            {
                resizedW = w;
                resizeH = resizedW / (float)baseImg.Width * baseImg.Height;
            }
            else if (h != 0)
            {
                resizeH = h;
                resizedW = resizeH / (float)baseImg.Height * baseImg.Width;
            }
            return new int[] { (int)resizedW, (int)resizeH };
        }

        public static Bitmap ChangeOpacity(Image img, float opacityvalue)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb); // Determining Width and Height of Source Image
            bmp.SetResolution(img.HorizontalResolution, img.VerticalResolution);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CompositingMode = CompositingMode.SourceOver;

            ColorMatrix colormatrix = new ColorMatrix();
            colormatrix.Matrix33 = opacityvalue;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            graphics.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose();   // Releasing all resource used by graphics 
            return bmp;
        }

        // Return a bitmap rotated around its center.
        public static Bitmap RotateBitmap(Bitmap bm, float angle)
        {
            // Make a bitmap to hold the rotated result.
            Bitmap result = new Bitmap(bm.Width, bm.Height);
            result.SetResolution(bm.HorizontalResolution, bm.VerticalResolution);
            // Create the real rotation transformation.
            Matrix rotate_at_center = new Matrix();
            rotate_at_center.RotateAt(angle, new PointF(bm.Width / 2f, bm.Height / 2f));

            // Draw the image onto the new bitmap rotated.
            using (Graphics gr = Graphics.FromImage(result))
            {
                // Use smooth image interpolation.
                // gr.InterpolationMode = InterpolationMode.High;
                //// For debugging. (Easier to see the background.)
                //gr.Clear(Color.LightBlue);

                // Set up the transformation to rotate.
                gr.Transform = rotate_at_center;

                // Draw the image centered on the bitmap.
                gr.DrawImage(bm, 0, 0);
                gr.Dispose();

            }

            // Return the result bitmap.
            return result;
        }
    }
}
