﻿using System;
using System.Collections.Generic;
using System.IO;

namespace NCCLibs
{
    public class DirectoryUtil
    {
        public static string[] GetDirectories(string path)
        {
            string[] directories = null;
            try
            {
                directories = Directory.GetDirectories(path);
            }
            catch
            {

            }
            return directories;
        }
        public static List<ImageItem> GetImages(string path, List<string> acceptTypes, bool isLoadMockup)
        {
            List<ImageItem> result = new List<ImageItem>();
            try
            {
                string[] files = Directory.GetFiles(path);
                result = ExtractImages(files, acceptTypes, isLoadMockup);
            }
            catch
            {

            }
            return result;
        }

        public static List<ImageItem> ExtractImages(string[] files, List<string> acceptTypes, bool isLoadMockup)
        {
            List<ImageItem> result = new List<ImageItem>();
            foreach (string file in files)
            {
                string extendsion = Path.GetExtension(file);
                if (acceptTypes == null || acceptTypes.IndexOf(extendsion.ToUpper()) != -1)
                {
                    ImageItem item = new ImageItem();
                    item.FullPath = file;
                    item.DirectoryPath = Path.GetDirectoryName(file);
                    item.Extendsion = extendsion;
                    item.Name = Path.GetFileNameWithoutExtension(item.FullPath);
                    item.Thumbnail = item.DirectoryPath + "/" + item.Name + "#THUMB.PNG";
                    if (isLoadMockup)
                    {
                        if (!item.Name.Contains("#"))
                        {
                            string additionalImagePath = findAdditionalImage(item.Name, files);
                            if (additionalImagePath != null)
                            {
                                item.AdditionalImageFullPath = additionalImagePath;
                                result.Add(item);
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                    else if (!item.Name.Contains("#THUMB"))
                    {
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public static string GetDirectoryName(string path)
        {
            return new DirectoryInfo(path).Name;
        }

        public static bool IsExistedFile(string path)
        {
            return File.Exists(path);
        }
        public static void CopyFile(String oldPath, String newPath)
        {
            //File.Copy(from, to);
            FileStream input = null;
            FileStream output = null;
            try
            {
                input = new FileStream(oldPath, FileMode.Open);
                output = new FileStream(newPath, FileMode.Create, FileAccess.ReadWrite);

                byte[] buffer = new byte[32768];
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, read);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                input.Close();
                input.Dispose();
                output.Close();
                output.Dispose();
            }
        }
        public static void DeleteFile(string file)
        {
            try
            {
                File.Delete(file);
            }
            catch
            {
            }
        }

        public static void createFolder(string fullPath)
        {
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
        }

        private static string findAdditionalImage(string mainFileNameWithoutExtension, string[] files)
        {
            foreach (string file in files)
            {
                string currentFileName = Path.GetFileNameWithoutExtension(file);
                if (currentFileName != mainFileNameWithoutExtension && currentFileName.StartsWith(mainFileNameWithoutExtension + "#BG"))
                {
                    return file;
                }
            }

            return null;
        }

    }
}
