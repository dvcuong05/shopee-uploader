﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main.classes
{
    public class ColorOpacity
    {
        string value;
        string colorCode;
        string name;
        int opacity;

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public string ColorCode
        {
            get { return colorCode; }
            set { colorCode = value; }
        }

        public string Name { get => name; set => name = value; }
        public int Opacity { get => opacity; set => opacity = value; }
    }
}
