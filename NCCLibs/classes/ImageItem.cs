﻿using Main.classes;
using System.Collections.Generic;
using System.Drawing;

namespace NCCLibs
{
    public class ImageItem
    {
        private string id;
        private string name;
        private string fullPath;
        private string directoryPath;
        private string extendsion;
        private string thumbnail;
        private bool isChecked = true;
        private string cloudUrl;
        private float angle = 0;
        private int opacity = 100;
        private Rectangle rect;
        // add the moment for tag
        private Rectangle additionalRect;
        // for export
        private string exportedName;
        private string exportedFullPath;
        private string additionalImageFullPath;
        private bool displayMode;
        private List<ColorOpacity> colors;

        public string Name { get => name; set => name = value; }
        public string FullPath { get => fullPath; set => fullPath = value; }
        public string Extendsion { get => extendsion; set => extendsion = value; }
        public string Thumbnail { get => thumbnail; set => thumbnail = value; }
        public string DirectoryPath { get => directoryPath; set => directoryPath = value; }
        public bool IsChecked { get => isChecked; set => isChecked = value; }
        public string CloudUrl { get => cloudUrl; set => cloudUrl = value; }
        public string ExportedName { get => exportedName; set => exportedName = value; }
        public string ExportedFullPath { get => exportedFullPath; set => exportedFullPath = value; }
        public string AdditionalImageFullPath { get => additionalImageFullPath; set => additionalImageFullPath = value; }
        public bool DisplayMode { get => displayMode; set => displayMode = value; }
        public int Opacity { get => opacity; set => opacity = value; }
        public float Angle { get => angle; set => angle = value; }
        public Rectangle Rect { get => rect; set => rect = value; }
        public Rectangle AdditionalRect { get => additionalRect; set => additionalRect = value; }
        public string Id { get => id; set => id = value; }
        public List<ColorOpacity> Colors { get => colors; set => colors = value; }
    }
}
